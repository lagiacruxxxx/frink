        
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/menueditor.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/besc_crud/css/besc_crud.css"); ?>">
        
        <script>var clientId = <?= $client->id?>;</script>
        
        <script type="text/javascript" src="<?=site_url("items/backend/js/client_content.js"); ?>"></script>
        
        <div id="menu_menu">
            <div class="menu_menu_item nofloat">Client: <strong><?= $client->name?></strong></div>
            <div class="menu_menu_item">
                <div class="menu_menu_item_button" id="menu_save">Save changes</div>
                <div class="menu_menu_item_button" id="menu_discard">Discard changes</div>
            </div>
            <br clear="both">
            <div class="menu_menu_item">
                <div class="menu_menu_item_button" id="select_all">Select all</div>
                <div class="menu_menu_item_button" id="deselect_all">Cancel</div>
            </div>
        </div>
        
        <table id="client_content_table" class="bc_table">
            <thead>
                <th>Show</th>
                <th>Content</th>
                <th>Type</th>
            </thead>
            <tbody>
                <?php $i = 0; foreach($content as $contentItem):?>
                    <tr<?php if($i % 2 == 1):?> class="bc_erow"<?php endif;?>>
                        <td><input type="checkbox" content_id="<?= $contentItem['id']?>" <?php if($contentItem['active']):?>checked<?php endif;?>></td>
                        <td><?= $contentItem['name']?></td>
                        <td>
                        <?php switch($contentItem['type'])
                        {
                            case CONTENT_TYPE_ARTICLE: $type = "Article";
                                break;
                            case CONTENT_TYPE_ASSESSMENT: $type = "Assessment";
                                break;
                            case CONTENT_TYPE_CHEATSHEET: $type = "Cheatsheet";
                                break;
                            case CONTENT_TYPE_LESSON: $type = "Lesson";
                                break;
                            case CONTENT_TYPE_QUIZ: $type = "Quiz";
                                break;
                            default: $type = "";
                                break;
                        }
                        echo $type;
                        ?></td>
                    </tr>
                <?php $i++; endforeach;?>
            </tbody>
        </table>
