
            
            <!-- CSS -->
            <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/jquery-ui.css"); ?>">
            <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/jquery-ui.structure.css"); ?>">
            <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/jquery-ui.theme.css"); ?>">
            <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/imgareaselect-default.css"); ?>">
            <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/contenteditor.css"); ?>">
        	<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/content.css"); ?>">
        
        	<!-- JS -->
        	<script type="text/javascript" src="<?=site_url("items/general/js/jquery-ui.min.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/jquery.imgareaselect.min.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/imagesloaded.pkgd.min.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/ckeditor/ckeditor.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/ckeditor/adapters/jquery.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/content.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_text.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_image.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_multiplechoice4.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_video.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_accordion.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_sequence.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_assessment.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_gallery.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_vimeo.js"); ?>"></script>

        	<!-- moduledata -->
        	<script>
                var contentId = <?= $id?>;
                var contentLang = <?= $currentLang?>;
                var contentType = <?= $contentType?>;
                var quizId = <?= $quiz?>;
                var assessmentId = <?= $assessment ?>;
        	</script>

        
        	<div id="content_menu_container">
                <div id="content_menu">
                    <div class="content_menu_item nofloat">Currently editing: <strong><?= $name ?></strong></div>
                    <div class="content_menu_item">
                        <span>Language:</span> 
                        <select id="content_lang_select">    
                            <?php foreach($languages->result() as $lang):?>
                                <option value=<?= $lang->id?> <?php if($lang->id == $currentLang):?>SELECTED<?php endif;?> ><?= $lang->name?></option>
                            <?php endforeach;?>
                        </select>
                        <div class="content_menu_item_button" id="content_lang_switch">Switch</div>
                    </div>
                    <div class="content_menu_separator">|</div>
                    <div class="content_menu_item">
                        <select id="content_module_select">
                            <option value="text">Text</option>
                            <option value="image">Image</option>
                            <option value="video">Video (Youtube)</option>
                            <option value="vimeo">Video (Vimeo)</option>
                            <option value="accordion_start">Accordion start</option>
                            <option value="accordion_end">Accordion end</option>
                            <?php if($contentType == CONTENT_TYPE_QUESTION):?><option value="multiplechoice4">Multiple Choice (4)</option><?php endif;?>
                            <?php if($contentType == CONTENT_TYPE_ASSESSMENT_QUESTION):?><option value="assessment" SELECTED>Assessment question</option><?php endif;?>
                            <option value="sequence" >Sequence</option>
                            <option value="gallery" >Gallery</option>
                        </select>
                        <div class="content_menu_item_button" id="content_module_add">Add</div>
                    </div>
                    <div class="content_menu_separator">|</div>
                    <div class="content_menu_item">
                        <div class="content_menu_item_button" id="content_save">Save changes</div>
                        <div class="content_menu_item_button" id="content_discard">Go back</div>
                    </div>
                </div>
            </div>
            
          
            <div id="content_container" class="has_placeholder" data-text="EMPTY!!!"><?php
            $i = 0;
            foreach($modules as $module)
            {
                switch($module['mod'])
                {
                    case 'text':
                        echo '<div module_id=' . $i++ . ' class="module module_text has_placeholder" data-text="Enter text here">' . $module['content'] . '</div>';
                        break;
                    
                    case 'image':
                        $stretch = $module['stretch'] == 1 ? 'width: 100%;' : ' ';
                        echo '<div module_id=' . $i++ . ' class="module module_image has_placeholder" data-text="Enter text here" style="text-align: ' . $module['align'] . ';">';
                        echo '<img style="' . $stretch . '" src="' . $module['fname'] .'" />';
                        echo '<div class="module_image_caption">' . $module['caption'] . '</div>';
        	            echo '</div>';
                        break;  

                    case 'multiplechoice4':
                        echo '<div module_id=' . $i++ . ' class="module module_multiplechoice4" correct_answer=' . $module['correct_answer'] . '>';
                        echo '<div class="module_question">' . $module['question'] . '</div>';
                        echo '<div class="module_answer" answer=1>' . $module['answer1'] . '</div>';
                        echo '<div class="module_answer" answer=2>' . $module['answer2'] . '</div>';
                        echo '<div class="module_answer" answer=3>' . $module['answer3'] . '</div>';
                        echo '<div class="module_answer" answer=4>' . $module['answer4'] . '</div>';
                        echo '</div>';
                        break; 
                    
                    case 'video':
                        echo '<div module_id=' . $i++ . ' class="module module_video" start="' . $module['start'] . '" code="' . $module['code'] . '">';
                        echo '<div class="module_video_overlay"></div><iframe class="module_video_iframe" src="https://www.youtube.com/embed/' . $module['code'] . '?start=' . $module['start'] . '&wmode=transparent" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe>';
                        echo '</div>';
                        break;
                    
                    case 'accordion_start':
                        echo '<div module_id=' . $i++ . ' class="module module_accordion_start">' . $module['headline'] . '</div>';
                        break;
                    
                    case 'accordion_end':
                        echo '<div module_id=' . $i++ . ' class="module module_accordion_end"></div>';
                        break;

                    case 'sequence':
                        echo '<div module_id=' . $i++ . ' class="module ' . ($module['orientation'] == SEQUENCE_ORIENTATION_TOPBOTTOM ? '' : 'leftright') . ' module_sequence">';
                        foreach($this->em->getModulesSequenceItem($module['id'])->result() as $seq)
                        {
                            echo '<div class="module_sequence_item">';
                            echo '<div class="module_sequence_img">';
                            echo '<img src="' . $seq->fname . '" /></div>';
                            echo '<div class="module_sequence_desc">';
                            echo '<div class="module_sequence_enlarge">Click to enlarge</div>';
                            echo '<div class="module_sequence_ordering">?</div><div class="module_sequence_headline">' . $seq->headline . '</div>';
                            echo '<div class="module_sequence_text">' . $seq->description . '</div>';
                            echo '</div></div>';
                        }
                        echo '</div>';
                        break;
                        
                    case 'assessment':
                            echo '<div module_id=' . $i++ . ' class="module module_assessment" >';
                            echo '<div class="module_question">' . $module['question'] . '</div>';
                            echo '<div class="module_answer" answer=1 role=' . $module['answer1_role'] . ' >' . $module['answer1'] . '</div>';
                            echo '<div class="module_answer" answer=2 role=' . $module['answer2_role'] . ' >' . $module['answer2'] . '</div>';
                            echo '<div class="module_answer" answer=3 role=' . $module['answer3_role'] . ' >' . $module['answer3'] . '</div>';
                            echo '<div class="module_answer" answer=4 role=' . $module['answer4_role'] . ' >' . $module['answer4'] . '</div>';
                            echo '</div>';
                            break;
                
                    case 'gallery':
                        echo '<div module_id=' . $i++ . ' class="module module_gallery">';
                        foreach($this->em->getModulesGalleryItem($module['id'])->result() as $seq)
                        {
                            echo '<div class="module_gallery_item" fname="' . $seq->fname . '"> ';
                            echo '<img src="' . $seq->fname . '"  />';
                            echo '</div>';
                        }
                        echo '</div>';
                        break;       

                    case 'vimeo':
                        echo '<div module_id=' . $i++ . ' class="module module_vimeo" code="' . $module['code'] . '" >';
                        echo '<div class="module_vimeo_overlay"></div><style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="https://player.vimeo.com/video/' . $module['code'] . '" frameborder=0 webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
                        echo '</div>';
                        break;                
                }
            }
            ?></div>
                        
            
            <div id="popup_module_text" class="popup_edit">
                <div class="header">Edit text module</div>
                <div class="popup_edit_container">
                    <textarea id="module_text_editor"></textarea>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_module_multiplechoice4" class="popup_edit">
                <div class="header">Edit text module</div>
                <div class="popup_edit_container">
                    <table>
                        <tr>
                            <td>Question</td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_multiplechoice4_question"></textarea></td>
                        </tr>
                        <tr>
                            <td>Answer 1 <?php if($correct == 1):?>(CORRECT)<?php endif;?></td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_multiplechoice4_answer1"></textarea></td>
                        </tr>
                        <tr>
                            <td>Answer 2 <?php if($correct == 2):?>(CORRECT)<?php endif;?></td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_multiplechoice4_answer2"></textarea></td>
                        </tr>                        
                        <tr>
                            <td>Answer 3 <?php if($correct == 3):?>(CORRECT)<?php endif;?></td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_multiplechoice4_answer3"></textarea></td>
                        </tr>                            
                        <tr>
                            <td>Answer 4 <?php if($correct == 4):?>(CORRECT)<?php endif;?></td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_multiplechoice4_answer4"></textarea></td>
                        </tr>    
                                                
                    </table>

                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_module_image" class="popup_edit">
                <div class="header">Edit image module</div>
                <div class="popup_edit_container">
                    <table>
                        <tr>
                            <td>
                                <div id="module_image_upload_button">Upload new image</div>
                                <input id="module_image_upload_input" type="file" accept=".png,.jpg,.jpeg"/>
                            </td>
                            <td>
                                <div id="module_image_filemanager">Image manager</div>
                            </td>
                        </tr>
                        <tr><td colspan=2>
                            <img id="module_image_upload_preview" src="" />
                        </td></tr>
                        <tr>
                            <td>Stretch image</td>
                            <td><input id="module_image_upload_stretch" type="checkbox"></td></tr>
                        <tr>
                            <td>Align</td>
                            <td>
                                <select id="module_image_upload_align">
                                    <option value="left">Left</option>
                                    <option value="center">Center</option>
                                    <option value="right">Right</option>
                                </select>
                            </td>
                        </tr> 
                        <tr>
                            <td>Caption</td>
                            <td><input type="text" id="module_image_upload_caption" /></td>
                        </tr>                                               
                    </table>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            
            <div id="popup_module_video" class="popup_edit">
                <div class="header">Edit video (Youtube)</div>
                <div class="popup_edit_container">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Video code</td>
                            <td>
                                <input type="text" id="video_code_input" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Starting position<br/>(in seconds)</td>
                            <td>
                                <input type="text" id="video_start_input" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            
            <div id="popup_module_accordionstart" class="popup_edit">
                <div class="header">Edit accordion start module</div>
                <div class="popup_edit_container">
                    <table>
                        <tr>
                            <td>Headline</td>
                            <td><input type="text" id="module_accordion_headline" /></td>
                        </tr>                                               
                    </table>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_module_accordionend" class="popup_edit">
                <div class="header">Edit accordion end module</div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_module_sequence" class="popup_edit">
                <div class="header">Edit sequence module</div>
                <div class="popup_edit_container">
                    <table>
                    
                        <tr>
                            <td style="vertical-align: middle;">Orientation:</td>
                            <td>
                                <select id="module_sequence_orientation">
                                    <option value="<?= SEQUENCE_ORIENTATION_TOPBOTTOM?>">Top-Bottom</option>
                                    <option value="<?= SEQUENCE_ORIENTATION_LEFTRIGHT?>">Left-Right</option>
                                </select>
                            </td>
                        </tr>
                    
                        <tr><td colspan=2><hr></td></tr>
                    
                        <tr>
                            <td style="vertical-align: middle;">
                                <div id="module_sequence_upload_button">Upload new image</div>
                                <input id="module_sequence_upload_input" type="file" accept=".png,.jpg,.jpeg"/>
                                <div id="module_sequence_filemanager">Filemanager</div>
                            </td>
                            <td>
                                <img id="module_sequence_upload_preview" src="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle;">Headline:</td>
                            <td><input style="margin-top: 5px; margin-bottom: 5px;" type="text" id="module_sequence_headline"/></td>
                        </tr>
                        
                        <tr>
                            <td style="vertical-align: middle;">Description:</td>
                            <td>
                                <textarea id="module_sequence_text"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2 style="border-bottom: solid 2px #000000">
                                <div id="module_sequence_add_button" mode="add" style="margin-top: 5px;">Save sequence item</div>
                            </td>
                        </tr>
                    </table>
                    
                    <div id="sequence_list">
                        
                    </div>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <?php if($contentType == CONTENT_TYPE_ASSESSMENT_QUESTION):?>
            <div id="popup_module_assessment" class="popup_edit">
                <div class="header">Edit assessment module</div>
                <div class="popup_edit_container">
                    <table>
                        <tr>
                            <td>Question</td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_assessment_question"></textarea></td>
                        </tr>
                        <tr>
                            <td>Answer 1</td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_assessment_answer1"></textarea></td>
                        </tr>
                        <tr>
                            <td> <select id="module_assessment_answer1_role"><?php foreach($roles->result() as $role):?><option value=<?= $role->id?>><?= $role->name?></option><?php endforeach;?></select></td>
                        </tr>
                        <tr>
                            <td>Answer 2</td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_assessment_answer2"></textarea></td>
                        </tr>                        
                        <tr>
                            <td> <select id="module_assessment_answer2_role"><?php foreach($roles->result() as $role):?><option value=<?= $role->id?>><?= $role->name?></option><?php endforeach;?></select></td>
                        </tr>
                        <tr>
                            <td>Answer 3</td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_assessment_answer3"></textarea></td>
                        </tr>
                        <tr>
                            <td> <select id="module_assessment_answer3_role"><?php foreach($roles->result() as $role):?><option value=<?= $role->id?>><?= $role->name?></option><?php endforeach;?></select></td>
                        </tr>
                        <tr>
                            <td>Answer 4</td>
                        </tr>
                        <tr>
                            <td> <textarea id="module_assessment_answer4"></textarea></td>
                        </tr>    
                        <tr>
                            <td> <select id="module_assessment_answer4_role"><?php foreach($roles->result() as $role):?><option value=<?= $role->id?>><?= $role->name?></option><?php endforeach;?></select></td>
                        </tr>
                    </table>

                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            <?php endif;?>
            
            
            <div id="popup_module_gallery" class="popup_edit">
                <div class="header">Edit gallery module</div>
                <div class="popup_edit_container">
                
                    <div id="module_gallery_upload_button">Upload new image</div>
                    <input id="module_gallery_upload_input" type="file" accept=".png,.jpg,.jpeg"/>
                    <div id="module_gallery_filemanager">Filemanager</div>
                    
                    <div id="module_gallery_items">
                        
                    </div>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            
            <div id="popup_module_vimeo" class="popup_edit">
                <div class="header">Edit video (Vimeo)</div>
                <div class="popup_edit_container">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Video code</td>
                            <td>
                                <input type="text" id="vimeo_code_input" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_delete_button">Delete module</div>
                    <div class="popup_edit_button popup_save_button">Save module</div>
                    <div class="popup_edit_button popup_moveup_button">Move up</div>
                    <div class="popup_edit_button popup_movedown_button">Move down</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            
            
            <div id="roxyCustomPanel" style="display: none;">
                <iframe src="" style="width:100%;height:100%" frameborder="0"></iframe>
            </div>
             
           
            