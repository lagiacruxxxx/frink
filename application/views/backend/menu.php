
<div id="messagecontainer"></div>

<div id="menu">
	<?php if($username != null):?>
	<ul>
		<li><a href="<?= site_url('authentication/logout')?>">Logout</a></li>
		<li><a href="<?= site_url('authentication/adminsettings')?>"><img src="<?= site_url('items/backend/img/settings.png')?>" /></a></li>
		<li>Logged in as <b><?= $username?></b></li>
	</ul>
	<?php endif;?>
</div>

<div id="sidebar">
    <div class="sidebar_logo">
        <img src="<?= site_url('items/backend/img/logo.png')?>" />
    </div>

	<ul>
		<li class="sidebar_headline">USER MANAGEMENT</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/User/client')?>">Clients</a>
        </li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/User/user')?>">Users</a>
        </li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/User/admin')?>">Administrators</a>
        </li>
		
        
        <li><div class="separator"></div></li>

        
        <li class="sidebar_headline">CONTENT</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Content/article')?>">Articles</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Content/gallery')?>">Galleries</a>
        </li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Content/quiz')?>">Quizzes</a>
        </li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Content/quiz_stats')?>">Quiz statistics</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Content/assessment')?>">Assessments</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Content/assessment_role')?>">Assessment roles</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Content/assessment_stats')?>">Assessment statistics</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Content/lesson')?>">Training lessons</a>
        </li>
        
        <li><div class="separator"></div></li>

        
        <li class="sidebar_headline">SETTINGS</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Settings/language')?>">Languages</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Menu/metatag')?>">Metatags</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Menu/techs')?>">Technologies</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Settings/search_keywords')?>">Search keywords</a>
        </li>
	</ul>
</div>

