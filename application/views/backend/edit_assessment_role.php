
            
            <!-- CSS -->
            <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/menueditor.css"); ?>">
        
        	<!-- JS -->
        	<script type="text/javascript" src="<?=site_url("items/general/js/jquery-ui.min.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/menu.js"); ?>"></script>

        	
        	<script>
                var roleId = <?= $role->id?>
        	</script>
        	
        	<div id="menu_menu">
                <div class="menu_menu_item nofloat">Currently editing: <strong><?= $role->name ?></strong></div>
                <div class="menu_menu_item">
                    <div class="menu_menu_item_button" id="assessment_role_save">Save changes</div>
                    <div class="menu_menu_item_button" id="assessment_role_discard">Cancel</div>
                </div>
            </div>
        	
            <div id="langlist">
                <?php foreach($languages as $key => $lang):?>
                    <div class="langlist_item" lang_id=<?=$key?>>
                        <div class="langlist_item_header"><?= $lang['lang_name']?></div>
                        <div class="langlist_item_desc">Role name</div>
                        <div class="langlist_item_text"><input type="text"/ value="<?= $lang['role_name'] != null ? $lang['role_name']->name : ''?>"></div>
                    </div>
                <?php endforeach;?>
            </div>          

            
            