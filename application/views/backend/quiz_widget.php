
    <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/chosen.css"); ?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url("items/besc_crud/css/jquery-ui.css"); ?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/jquery-ui.theme.css"); ?>">
    
    
    <script type="text/javascript" src="<?=site_url("items/general/js/jquery-ui.min.js"); ?>"></script>
    <script type="text/javascript" src="<?=site_url("items/backend/js/chosen.jquery.min.js"); ?>"></script>
    <script type="text/javascript" src="<?=site_url("items/backend/js/widget.js"); ?>"></script>
    
    <div id="widgetmenu">
        <div class="widgetmenu_item client">
            <select multiple>
                <?php foreach($clients->result() as $client):?>
                <option value="<?= $client->id?>"><?= $client->name?></option>
                <?php endforeach;?>
            </select>
        </div>
        
        <div class="widgetmenu_item language">
            <select multiple>
                <?php foreach($language->result() as $lang):?>
                <option value="<?= $lang->id?>"><?= $lang->short?></option>
                <?php endforeach;?>
            </select>
        </div>
        
        <div class="widgetmenu_item startdate">
            <input type="text" placeholder="Enter start date"/>
        </div>
        
        <div class="widgetmenu_item enddate">
            <input type="text" placeholder="Enter end date"/>
        </div>
        
        <div class="widgetmenu_item query quiz">
            <div id="widget_query">Run query</div>
        </div>
    </div>
    <br clear="both"/>
    <div id="widgetresult">
    
    </div>
