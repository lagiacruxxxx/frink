
<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/gallery_lightbox.css"); ?>">
<script type="text/javascript" src="<?=site_url("items/frontend/js/gallery.js"); ?>"></script>

<div id="content">
    <div id="gallery">
        <div id="galleryHeader">
            <div id="galleryHeadline"><?= $gallery->name?></div>
            <div id="galleryItemcount"><?= $galleryItems->num_rows()?> <?= MyLang::langString('gallery_photos')?></div>
        </div>
        <div id="galleryContainer">
            <?php foreach($galleryItems->result() as $galleryItem):?>
                <div class="galleryItem">
                    <a href="<?= site_url('items/uploads/gallery/' . $galleryItem->fname)?>" data-lightbox="gallery" data-title="<?= nl2br($galleryItem->description)?>">
                        <img src="<?= site_url('items/uploads/gallery/' . $galleryItem->fname)?>" />
                    </a>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>        