        
<?php if($ga != null):?>                
<script>
ga('send', 'event', '<?=$ga['type']?>', '<?= $ga['action']?>', '<?=$ga['name']?>');
</script>             
<?php endif;?>   
                
                
<div id="content">
    <div id="content_container" class="<?php if($contentType == CONTENT_TYPE_QUESTION || $contentType == CONTENT_TYPE_ASSESSMENT_QUESTION):?>question_background<?php endif;?>">
        
        
        <?php if($contentType == CONTENT_TYPE_QUESTION || $contentType == CONTENT_TYPE_ASSESSMENT_QUESTION):?>
            <div id="question_block">
                <div class="question_block_top left"><?= MyLang::langString('question')?></div>
                <!-- <div class="question_block_top right"><?= MyLang::langString('question_next')?></div> -->
                <br clear="both"/>
                <div class="question_block_bottom left"><?= $questionOrder?></div>
                <!--<?php if($contentType == CONTENT_TYPE_QUESTION):?>
                    <div class="question_block_bottom right" type="quiz"><a href="<?= site_url('quiz_next')?>"><img src="<?= site_url('items/frontend/img/question_next.png')?>" /></a></div>
                <?php else:?>
                    <div class="question_block_bottom right" type="assessment"><a href="<?= site_url('assessment_next')?>"><img src="<?= site_url('items/frontend/img/question_next.png')?>" /></a></div>
                <?php endif;?>-->
            </div>
        <?php endif;?>
    
        <?php foreach($modules as $module):?>
            <?php 
                switch($module['mod'])
                {
                    case 'text':
                        echo '<div module_id=' . $module['id'] . ' class="module module_text">' . $module['content'] . '</div>';
                        break;
                        
                    case 'image':
                        $stretch = $module['stretch'] == 1 ? 'width: 100%;' : ' ';
                        echo '<div module_id=' . $module['id'] . ' class="module module_image" style="text-align: ' . $module['align'] . ';">';
                        echo '<img style="' . $stretch . '" src="' . $module['fname'] .'" />';
                        echo '<div class="module_image_caption">' . $module['caption'] . '</div>';
        	            echo '</div>';
                        break;
                        
                    case 'multiplechoice4':
                        echo '<div module_id=' . $module['id'] . ' class="module module_multiplechoice4">';
            			echo '<div class="module_question">' . $module['question'] . '</div>';
            			if($module['answer1'] != '')
                            echo '<div class="module_answer animate" answer=1>' . $module['answer1'] . '</div>';
            			if($module['answer2'] != '')
                            echo '<div class="module_answer animate" answer=2>' . $module['answer2'] . '</div>';
            			if($module['answer3'] != '')
                            echo '<div class="module_answer animate" answer=3>' . $module['answer3'] . '</div>';
            			if($module['answer4'] != '')
                            echo '<div class="module_answer animate" answer=4>' . $module['answer4'] . '</div>';
            			echo '</div>';
                        break;
                        
                    case 'video':
                        echo '<div module_id=' . $module['id'] . ' class="module module_video">';
                        echo '<iframe class="module_video_iframe" src="https://www.youtube.com/embed/' . $module['code'] . '?start=' . $module['start'] . '&wmode=transparent" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe>';
                        echo '</div>';
                        break;
                        
                    case 'accordion_start':
                        echo '<div module_id=' . $module['id'] . ' class="module module_accordion_start"><div class="module_accordion_arrow"></div><div class="module_accordion_headline">' . $module['headline'] . '</div></div>';
                        echo '<div module_id=' . $module['id'] . ' class="module_accordion_content">';
                        break;
                        
                    case 'accordion_end':
                        echo '</div>';
                        break; 
                        
                    case 'sequence':
                        //$orientation = 
                        echo '<div module_id=' . $module['id'] . ' class="module ' . ($module['orientation'] == SEQUENCE_ORIENTATION_TOPBOTTOM ? '' : 'leftright') . ' module_sequence">';
                        foreach($this->cm->getModulesSequenceItem($module['id'])->result() as $seq)
                        {
                            echo '<div class="module_sequence_item">';
                            //echo '<a href="/frink' . $seq->fname . '" data-lightbox="module_' . $module['id'] . '"><div class="module_sequence_img">'; 
                            echo '<div class="module_sequence_img">';
                            //echo '<img src="/frink' . $seq->fname . '" /></div></a>';
                            echo '<img src="' . $seq->fname . '" /></div>';
                            echo '<div class="module_sequence_desc">';
                            echo '<div class="module_sequence_enlarge">' . MyLang::langString('sequence_enlarge') . '</div>';
                            echo '<div class="module_sequence_ordering">' . ($seq->ordering + 1) . '</div><div class="module_sequence_headline">' . $seq->headline . '</div>';
                            echo '<div class="module_sequence_text">' . $seq->description . '</div>';
                            echo '</div></div>';
                        }
                        echo '</div>';
                        break;
                    
                    case 'assessment':
                        echo '<div module_id=' . $module['id'] . ' class="module module_assessment">';
                        echo '<div class="module_question">' . $module['question'] . '</div>';
                        if($module['answer1'] != '')
                            echo '<div class="module_answer animate" answer=1>' . $module['answer1'] . '</div>';
                        if($module['answer2'] != '')
                            echo '<div class="module_answer animate" answer=2>' . $module['answer2'] . '</div>';
                        if($module['answer3'] != '')
                            echo '<div class="module_answer animate" answer=3>' . $module['answer3'] . '</div>';
                        if($module['answer4'] != '')
                            echo '<div class="module_answer animate" answer=4>' . $module['answer4'] . '</div>';
                        echo '</div>';
                        break;     

                    case 'gallery':
                        echo '<div module_id=' . $module['id'] . ' class="module module_gallery">';
                        foreach($this->cm->getModulesGalleryItem($module['id'])->result() as $seq)
                        {
                            echo '<div class="module_gallery_item">';
                            echo '<a href="' . $seq->fname . '" data-lightbox="module_' . $module['id'] . '">';
                            echo '<img src="' . $seq->fname . '" /></a>';
                            echo '</div>';
                        }
                        echo '</div>';
                        break; 
                        
                    case 'vimeo':
                        echo '<div module_id=' .  $module['id'] . ' class="module module_vimeo">';
                        echo '<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="https://player.vimeo.com/video/' . $module['code'] . '" frameborder=0 webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
                        echo '</div>';
                        break;                                               
                }
            ?>
        <?php endforeach;?>
        
        <?php if($contentType == CONTENT_TYPE_QUIZ):?>
            <br clear="both"/>
            <a href="<?= site_url('quiz_start/' . $contentId)?>"><div id="start_quiz"><?= MyLang::langString('quiz_start_btn')?></div></a>
        <?php endif;?>
        
        <?php if($contentType == CONTENT_TYPE_QUESTION):?>
            <script type="text/javascript" src="<?=site_url("items/frontend/js/quiz.js"); ?>"></script>
            <br clear="both"/>
            <!-- <div id="progressbar" limit=<?= $question->timelimit?>></div> -->
        <?php endif;?>
        
        <?php if($contentType == CONTENT_TYPE_ASSESSMENT):?>
            <br clear="both"/>
            <a href="<?= site_url('assessment_start/' . $contentId)?>"><div id="start_assessment"><?= MyLang::langString('assessment_start_btn')?></div></a>
        <?php endif;?>
        
        <?php if($contentType == CONTENT_TYPE_ASSESSMENT_QUESTION):?>
            <script type="text/javascript" src="<?=site_url("items/frontend/js/quiz.js"); ?>"></script>
        <?php endif;?>
        
        <?php if($contentType == CONTENT_TYPE_ASSESSMENT_RESULT):?>
            <?= $assessment_results?>
        <?php endif;?>
        
        <?php if($contentType == CONTENT_TYPE_QUIZ_RESULT):?>
            <?= $quiz_results?>
        <?php endif;?>        
        
        <?php if($contentType == CONTENT_TYPE_LESSON && false):?>
            
            <br clear="both"/>
            <?php if(!MyLesson::alreadyRegistered($contentId, $user->id)):?>
                <div id="lesson_register" lesson=<?= $contentId?> class="animate"><?= MyLang::langString('lesson_register_btn')?></div>
                <div id="lesson_register_loading"><img src="<?= site_url('items/frontend/img/ajax-loader.gif')?>" /></div>
                <div id="lesson_register_success"><?= MyLang::langString('lesson_register_success')?></div>
            <?php else:?>
                <div id="lesson_register_success" style="display: block;"><?= MyLang::langString('lesson_register_already')?></div>
            <?php endif;?>
        <?php endif;?>
    </div>
    
    
</div>    
    
             

        
                
