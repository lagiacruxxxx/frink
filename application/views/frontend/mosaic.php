

<div id="content">
    <div id="mosaic">
        <?php foreach($contentitems as $item):?>
            <?php if($item->teaser_type == TEASER_TYPE_NORMAL):?>
            <div class="content" metatags="<?= $item->metatags?>" techs="<?= $item->techs?>">
                <a href="<?= $item->url?>" >
                    <img src="<?= '/items/uploads/content/' . $item->teaser_fname?>" />
                    <div class="content_text">
                        <div class="content_headliner" ><?= $item->teasers != null ? $item->teasers->teaser_text : ''?></div>
                        <div class="content_subheader"><?= $item->teasers != null ? nl2br($item->teasers->teaser_subtext) : ''?></div>
                    </div>
                </a>
            </div>
            <?php endif;?>
            
            <?php if($item->teaser_type == TEASER_TYPE_CITATION):?>
            <div class="content citation" metatags="<?= $item->metatags?>" techs="<?= $item->techs?>">
                <a href="<?= $item->url?>" >
                    <img src="<?= '/items/uploads/content/' . $item->teaser_fname_big?>" />
                </a>
            </div>
            <?php endif;?>
            
            <?php if($item->teaser_type == TEASER_TYPE_3):?>
            <div class="content content_3" metatags="<?= $item->metatags?>" techs="<?= $item->techs?>">
                <a href="<?= $item->url?>" >
                    <img src="<?= '/items/uploads/content/' . $item->teaser_fname_3?>" />
                    <div class="content_text_3">
                        <div class="content_headliner" ><?= $item->teasers != null ? nl2br($item->teasers->teaser_text) : ''?></div>
                        <div class="content_subheader"><?= $item->teasers != null ? nl2br($item->teasers->teaser_subtext) : ''?></div>
                    </div>
                </a>
            </div>
            <?php endif;?>

        <?php endforeach;?>
    </div>
</div>
        
        
        

            