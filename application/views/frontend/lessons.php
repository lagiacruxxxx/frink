

<script>
    var year = <?= $year?>;
    var month = <?= $month?>;
</script>

<div id="content">
    <div id="content_container">
    
        <div id="calendar_header">
            <div id="calendar_headline"><?= MyLang::langString('calendar_headline')?></div>
            <div id="calendar_subline"><?= MyLang::langString('calendar_subline')?></div>
        </div>
    
        <div id="calendar_container">
            <div class="calendar_control">
                <a href="<?= site_url('Frontend/lessons?month=' . $prevmonth . '&year=' . $prevyear)?>">
                    <p><?= MyLang::langString('calendar_prev_month')?></p>
                    <img src="<?= site_url('items/frontend/img/prev_month.png')?>" />
                </a>
            </div>
            <table id="calendar_prev" class="calendar">
                <thead>
                    <tr>
                        <th colspan=7 class="calendar_month"><?= $prevData['monthname'] . ' ' . $prevData['year']?></th>
                    </tr>
                    <tr>
                        <th><?= MyLang::langString('calendar_mon')?></td>
                        <th><?= MyLang::langString('calendar_tue')?></td>
                        <th><?= MyLang::langString('calendar_wed')?></td>
                        <th><?= MyLang::langString('calendar_thu')?></td>
                        <th><?= MyLang::langString('calendar_fri')?></td>
                        <th><?= MyLang::langString('calendar_sat')?></td>
                        <th><?= MyLang::langString('calendar_sun')?></td>
                    </tr>
                </thead>
                <tbody>
                    <?= $prevData['calendar'] ?>
                </tbody>
            </table>  
            <table id="calendar_cur" class="calendar">
                <thead>
                    <tr>
                        <th colspan=7 class="calendar_month"><?= $curData['monthname'] . ' ' . $curData['year']?></th>
                    </tr>
                    <tr>
                        <th><?= MyLang::langString('calendar_mon')?></td>
                        <th><?= MyLang::langString('calendar_tue')?></td>
                        <th><?= MyLang::langString('calendar_wed')?></td>
                        <th><?= MyLang::langString('calendar_thu')?></td>
                        <th><?= MyLang::langString('calendar_fri')?></td>
                        <th><?= MyLang::langString('calendar_sat')?></td>
                        <th><?= MyLang::langString('calendar_sun')?></td>
                    </tr>
                </thead>
                <tbody>
                    <?= $curData['calendar'] ?>
                </tbody>
            </table>
            <table id="calendar_next" class="calendar">
                <thead>
                    <tr>
                        <th colspan=7 class="calendar_month"><?= $nextData['monthname'] . ' ' . $nextData['year']?></th>
                    </tr>
                    <tr>
                        <th><?= MyLang::langString('calendar_mon')?></td>
                        <th><?= MyLang::langString('calendar_tue')?></td>
                        <th><?= MyLang::langString('calendar_wed')?></td>
                        <th><?= MyLang::langString('calendar_thu')?></td>
                        <th><?= MyLang::langString('calendar_fri')?></td>
                        <th><?= MyLang::langString('calendar_sat')?></td>
                        <th><?= MyLang::langString('calendar_sun')?></td>
                    </tr>
                </thead>
                <tbody>
                    <?= $nextData['calendar'] ?>
                </tbody>
            </table>  
            <div class="calendar_control">
                <a href="<?= site_url('lessons?month=' . $nextmonth . '&year=' . $nextyear)?>">
                    <p><?= MyLang::langString('calendar_next_month')?></p>
                    <img src="<?= site_url('items/frontend/img/next_month.png')?>" />
                </a>
            </div>
        </div>
        
        <?php foreach($lessongroups as $key => $lessongroup):?>
            <div class="lessongroup">
                <a name="<?= $key?>"><div class="lessongroup_header"><?= $lessongroup['dayname']?></div></a>
                <div class="lessongroup_toggle"><span><?= MyLang::langString('accordion_show_hide')?></span></div>
                <?php $i = 0 ; foreach($lessongroup['lessons'] as $lesson):?>
                    <div class="lesson <?php if($i++ == 0):?>first_lesson<?php endif;?>">
                        <div class="lesson_header">
                            <div class="lesson_header_toggle"></div>
                            <div class="lesson_header_name"><?= $lesson->name?></div>
                            <div class="lesson_header_time"><?= $lesson->starttime?></div>
                        </div>
                        <div class="lesson_content">
                            <div class="lesson_content_desc"><?= nl2br($lesson->desc)?></div>
                            <div class="lesson_content_info">
                                <div class="lesson_content_info_desc"><?= MyLang::langString('lesson_date') ?>:</div>
                                <div class="lesson_content_info_data"><?= date('d.m.Y', strtotime($lesson->startdate))?></div>
                                <div class="lesson_content_info_desc"><?= MyLang::langString('lesson_time') ?>:</div>
                                <div class="lesson_content_info_data"><?= $lesson->starttime?></div>
                                <div class="lesson_content_info_desc"><?= MyLang::langString('lesson_location') ?>:</div>
                                <div class="lesson_content_info_data"><?= nl2br($lesson->location)?></div>
                                <div class="lesson_content_info_desc"><?= MyLang::langString('lesson_duration') ?>:</div>
                                <div class="lesson_content_info_data"><?= nl2br($lesson->duration)?> min.</div>
                                <div class="lesson_content_info_desc"><?= MyLang::langString('lesson_focus') ?>:</div>
                                <div class="lesson_content_info_data"><?= nl2br($lesson->focus)?></div>
                                <div class="lesson_content_info_desc"><?= MyLang::langString('lesson_teacher') ?>:</div>
                                <div class="lesson_content_info_data"><?= nl2br($lesson->teacher)?></div>
                                <br clear="both" />
                                <?php if(!MyLesson::alreadyRegistered($lesson->id, $user->id)):?>
                                    <div class="lesson_register" lesson=<?= $lesson->id?>><?= MyLang::langString('lesson_register_btn')?></div>
                                    <div class="lesson_register_loading"><img src="<?= site_url('items/frontend/img/ajax-loader.gif')?>" /></div>
                                    <div class="lesson_register_success"><?= MyLang::langString('lesson_register_success')?></div>
                                <?php else:?>
                                    <div class="lesson_register_success" style="display: block;"><?= MyLang::langString('lesson_register_already')?></div>
                                <?php endif;?>
                            </div>
                            
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endforeach;?>
    
    </div>
</div>

<script type="text/javascript" src="<?=site_url("items/frontend/js/lesson.js"); ?>"></script>