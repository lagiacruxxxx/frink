


    <br clear="both"/>
    <div class="results">
        <?= MyLang::langString('assessment_result_header')?> <strong><?= $role->name?></strong>
        <?php if($lessons->num_rows() > 0):?>
        <br/>
        <?= MyLang::langString('assessment_result_suggestions')?>
        
        <div class="profile_item">
            <table id="quiztable">
                <thead>
                    <tr>
                        <th>Lesson</th>
                        <th>Date</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                <?php  foreach($lessons->result() as $lesson):?>
                    <tr>
                        <td><a href="<?= site_url('lessons/#' . date('d-m-Y', strtotime($lesson->startdate)))?>" target="_blank"><?= $lesson->name?></a></td>
                        <td class="centered"><?= date('d.m.Y', strtotime($lesson->startdate))?></td>
                        <td class="centered"><?= $lesson->starttime?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <?php endif;?>
    </div>

    