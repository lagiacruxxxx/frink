

    <body>
    
        <div id="header">
            <?php if($client != null):?>
                <img id="clientlogo" src="<?= site_url('items/uploads/client/' . $client->fname)?>"/>
            <?php endif;?>
            
            
            <?php if($user != null):?>
                <a href="<?= site_url('content/' . $client->help_content_id)?>"><div id="help_icon">?</div></a>
                <div class="header_disruptor"></div>
                <div id="searchbar">
                    <input value="" placeholder="<?= MyLang::langString('search_defaulttext')?>"/>
                    <?php foreach($keywords->result() as $keyword):?>
                        <div class="searchbar_keywords"><?= $keyword->keyword?></div>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
            
            
            <?php if($languages != null && $languages->num_rows() > 1):?>
                <div class="header_disruptor"></div>
                <div id="langswitch">
                    <?php foreach($languages->result() as $lang):?>
                        <span lang_id="<?= $lang->id?>" <?php if($lang->id == $user->active_lang) :?>class="activelang"<?php endif;?>><?= $lang->short?></span>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
        </div>
        
        <div id="techheader">
            <?php if($techs != null):?>
                <div id="techlist">
                    <div class="tech" tech_id=0>
                        <div class="techname" >All</div>
                    </div>
                    <?php foreach($techs->result() as $tech):?>
                        <div class="tech <?php if($tech->id == $filters['tech']):?>techmenu_item_active<?php endif;?>" tech_id="<?= $tech->id?>">
                            <img class="techlogo" src="<?= site_url('items/uploads/technology/' . $tech->fname)?>" />
                            <div class="techname"><?= $tech->name ?></div>
                            <div class="arrow_placeholder_tech" style="border-right-color: <?= $client->color2?>;"></div>
                        </div>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
        </div>
        
        
        
        <div id="sidemenu" >
            <?php if($menuitems != null):?>
                <div class="sidemenu_item sidemenu_reset" metatag_id=0>All</div>
                <?php foreach($menuitems->result() as $menuitem):?>
                    <?php if($menuitem->metatag_id == NULL):?>
                        <div class="sidemenu_headline"><?= $menuitem->name?></div>
                    <?php else:?>
                        <div class="sidemenu_item <?php if($menuitem->metatag_id == $filters['tag']):?>sidemenu_item_active<?php endif;?>" metatag_id=<?= $menuitem->metatag_id?>><span><?= $menuitem->name?></span></div>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>
            
            
            <?php if($user != null && $client->has_lessons == 1):?>
                <div class="sidemenu_headline"><?= MyLang::langString('menu_lesson')?></div>
                <a href="<?= site_url('lessons')?>"><div class="sidemenu_item" metatag_id=0><?= MyLang::langString('menu_lesson_available')?></div></a>
            <?php endif;?>
            <div class="sidemenu_headline"><?= MyLang::langString('sidebar_account')?></div>
            <?php if($user != null):?>
                <!-- <a href="<?= site_url('profile')?>"><div class="sidemenu_item" metatag_id=0><?= MyLang::langString('sidebar_profile')?></div></a>  -->
                <a href="<?= site_url('Authentication/logout')?>"><div class="sidemenu_item" metatag_id=0><?= MyLang::langString('sidebar_logout')?></div></a>
            <?php else:?>
                <a href="<?= site_url('Authentication/login')?>"><div class="sidemenu_item" metatag_id=0><?= MyLang::langString('sidebar_login')?></div></a>
            <?php endif;?>
        </div>