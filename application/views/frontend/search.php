

        <script type="text/javascript" src="<?=site_url("items/frontend/js/elasticsearch.jquery.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/elasticsearch.js"); ?>"></script>
        
        <script>
            var es_query = "<?= $query?>";
            var es_client_id = <?= $clientId?>;
            var es_activeLang = "<?= $activeLang?>";
            var es_techs = [];
            <?php foreach($techs->result() as $tech):?>
                es_techs[<?= $tech->id?>] = ["<?= $tech->name?>", "<?= $tech->fname?>"];
            <?php endforeach;?>
        </script>
        
        <div id="content">
            
            <div id="searchheader">
                <div id="searchheader_input">
                    <div id="searchheader_searchterm"><?= MyLang::langString('search_term')?></div>
                    <input value="<?= $query?>" />
                </div>
                
                <div id="searchheader_searchtotal"></div>
                <div id="searchheader_searchresult"><?= MyLang::langString('search_result')?></div>
                
            </div>
            
            <div id="searchresults">
                
            </div>
            
            
            <div class="searchresult is_dummy">
                <div class="searchresult_image">
                    <img src="" />
                </div>
                <div class="searchresult_description">
                    <div class="searchresult_title"><a href=""><span></span></a></div>
                    <div class="searchresult_highlight"></div>
                    <div class="searchresult_techs"></div>
                </div>
            </div>
        
            
        </div>