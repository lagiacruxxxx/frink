<html>
    <head>
    	        
        <link rel="apple-touch-icon" sizes="57x57" href="<?= site_url('items/general/img/favicons/apple-icon-57x57.png')?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= site_url('items/general/img/favicons/apple-icon-60x60.png')?>/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= site_url('items/general/img/favicons/apple-icon-72x72.png')?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= site_url('items/general/img/favicons/apple-icon-76x76.png')?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= site_url('items/general/img/favicons/apple-icon-114x114.png')?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= site_url('items/general/img/favicons/apple-icon-120x120.png')?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= site_url('items/general/img/favicons/apple-icon-144x144.png')?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= site_url('items/general/img/favicons/apple-icon-152x152.png')?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= site_url('items/general/img/favicons/apple-icon-180x180.png')?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= site_url('items/general/img/favicons/android-icon-192x192.png')?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= site_url('items/general/img/favicons/favicon-32x32.png')?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= site_url('items/general/img/favicons/favicon-96x96.png')?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= site_url('items/general/img/favicons/favicon-16x16.png')?>">
        <link rel="manifest" href="<?= site_url('items/general/img/favicons/apple-icon-57x57.png')?>/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= site_url('items/general/img/favicons/ms-icon-144x144.png')?>">
        <meta name="theme-color" content="#ffffff">
        
        <meta charset="UTF-8">
        
        <title>FRINK Academy</title>
    
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/reset.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/jquery-ui.css"); ?>">
        <!-- <link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/lightbox.css"); ?>"> -->
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/slimbox2.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/content.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/desktop.css"); ?>">
        

        
        <script type="text/javascript" src="<?=site_url("items/general/js/jquery-1.11.2.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/general/js/jquery-ui.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/isotope.pkgd.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/cookie.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/responsive.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/desktop.js"); ?>"></script>
        
        <script>
            var rootUrl = "<?= site_url()?>";
            var user_id = <?= $user != null ? $user->id : null?>;
        </script>
    	
    	<?php if($client != null && $client->ga_code != ''):?>
    	<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
          ga('create', '<?=$client->ga_code?>', 'auto');
          ga('send', 'pageview');

        </script>
        <?php endif;?>
    </head>
