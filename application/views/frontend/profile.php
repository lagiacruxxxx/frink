

<div id="content">

    <div class="profile_item">
        <div class="profile_item_header">Your quiz results</div>
        <table id="quiztable">
            <thead>
                <th>Quiz</th>
                <th>Date</th>
                <th>Result</th>
                <th>Points</th>
                <th></th>
            </thead>
            <tbody>
            <?php foreach($quizzes as $quiz):?>
                <tr>
                    <td><?= $quiz->quiz->quizname?></td>
                    <td class="centered"><?= date('d.m.Y', strtotime($quiz->quiz->created_date))?></td>
                    <td class="centered"><?= $quiz->percent . '%'?></td>
                    <td class="centered"><?= $quiz->points?></td>
                    <td><?php if(intval($quiz->percent) >= 60) echo '<img src="' . site_url('items/frontend/img/thumbsup.png') . '" />'; else echo '<img src="' . site_url('items/frontend/img/thumbsdown.png') . '" />';?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
    
    <div class="profile_item">
        <div class="profile_item_header">Your assessment results</div>
        <table id="quiztable">
            <thead>
                <th>Assessment</th>
                <th>Date</th>
                <th>Result</th>
            </thead>
            <tbody>
            <?php  foreach($assessments as $assessment):?>
                <tr>
                    <td><?= $assessment->name?></td>
                    <td class="centered"><?= date('d.m.Y', strtotime($assessment->assessment->created_date))?></td>
                    <td class="centered"><?= $assessment->rolename?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>        