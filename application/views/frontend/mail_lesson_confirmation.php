
    
    
    <div ></div>

    <table style="width: 100%; font-size: 14px;">
        <tr>
            <td colspan=3><span style="font-size: 16px; margin-bottom: 5px;"><?= "Dear $user->firstname $user->lastname,"?></span></td>
            <td><img src="<?= site_url('items/frontend/img/rsz_frink_lab-logo-small.png')?>" /></td>
        </tr>
        <tr>
            <td colspan=4 style="font-size: 16px; margin-bottom: 20px;">You have successfully registered for the following lesson:</td>
        </tr>		<tr>
		    <td style="font-weight: bold; width: 25%; vertical-align: top;">Lesson:</td>			<td style="width: 25%;"><?= $lesson->name?></td>
			<td></td>			<td></td>
		</tr>
		
		<tr>
		    <td style="font-weight: bold; width: 25%; vertical-align: top;">Date:</td>
			<td style="width: 25%;"><?= date('d.m.Y', strtotime($lesson->startdate))?></td>
			<td></td>
			<td></td>
		</tr>
		
		<tr>
			<td style="font-weight: bold; width: 25%; vertical-align: top;">Time:</td>
			<td style="width: 25%;"><?= $lesson->starttime?></td>
			<td></td>
			<td></td>
		</tr>
				<tr>
			<td style="font-weight: bold; width: 25%; vertical-align: top;">Duraton:</td>
			<td style="width: 25%;"><?= $lesson->duration?></td>
			<td></td>
			<td></td>
		</tr>
	
	    <tr>
			<td style="font-weight: bold; width: 25%; vertical-align: top;">Location:</td>
			<td style="width: 25%;"><?= nl2br($lesson->location)?></td>
			<td></td>
			<td></td>
		</tr>
		
		<tr>
            <td colspan=4 style="margin-top: 20px; font-size: 16px;">If you have any further question please direct them to <?= mailto('registration@frink.com', 'registration@frink.com')?>.</td>
		</tr>
	
	</table>
	
	