<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['std_pw'] = "start";

$config['module_image_placeholder'] = '';

define('CONTENT_TYPE_ARTICLE', 0);
define('CONTENT_TYPE_QUIZ', 1);
define('CONTENT_TYPE_QUIZ_RESULT', 7);
define('CONTENT_TYPE_QUESTION', 2);
define('CONTENT_TYPE_CHEATSHEET', 3);
define('CONTENT_TYPE_ASSESSMENT', 4);
define('CONTENT_TYPE_ASSESSMENT_QUESTION', 5);
define('CONTENT_TYPE_ASSESSMENT_RESULT', 6);
define('CONTENT_TYPE_LESSON', 8);
define('CONTENT_TYPE_GALLERY', 9);

define('PAGE_TYPE_ARTICLE', 0);
define('PAGE_TYPE_QUIZQUESTION', 1);

define('TEASER_TYPE_NORMAL', 0);
define('TEASER_TYPE_CITATION', 1);
define('TEASER_TYPE_3', 2);

define('SEQUENCE_ORIENTATION_TOPBOTTOM', 0);
define('SEQUENCE_ORIENTATION_LEFTRIGHT', 1);
