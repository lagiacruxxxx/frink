<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyLesson extends MyRender
{
	protected $lesson = null;
	protected $user = null;
	protected $lm = null;
	
	function __construct($lessonId, $user)
	{
	    parent::__construct();
	    
	    $this->user = $user;
	    $this->ci->load->model('Lesson_model', 'lm');
	    $this->lm = $this->ci->lm;
	    
	    if($lessonId != null)
	        $this->lesson = $this->lm->getLessonById($lessonId)->row();
	}	
    
    public function showPage()
    {
        $this->content($this->lesson->id, CONTENT_TYPE_LESSON);
    }
    
    
    public function register()
    {
        $registrationId = $this->lm->insertLessonRegistration(array(
            'lesson_id' => $this->lesson->id,
            'user_id' => $this->user->id,
        ));
        
        $this->sendConfirmation($registrationId);
        
        echo json_encode(array(
            'success' => true,
        ));
    }
    
    
    public function sendConfirmation($registrationId, $email_resend = '')
    {
        $this->ci->load->model('Frontend_model', 'fm');
        
        require_once(APPPATH . "libraries/phpmailer/PHPMailerAutoload.php");
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->IsSendmail();
        $mail->Host       = "smtp-relay.gmail.com";      // setting SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to
        //$mail->Username   = "greenlight@tba21.org";  // user email address
        //$mail->Password   = "grLig2602$";            // password
        //$mail->SMTPAuth   = true; // enabled SMTP authentication
        //$mail->IsSMTP();
        $mail->SetFrom('academy@frink.at');  //Who is sending the email
        $mail->AddReplyTo('academy@frink.at ');  //email address that receives the response
        
        $data['registration'] = $this->lm->getLessonRegistrationById($registrationId)->row();
        $data['lesson'] = $this->lm->getLessonById($data['registration']->lesson_id)->row();
        $data['user'] = $this->ci->fm->getUserById($data['registration']->user_id)->row();
    
        $mail->Subject = $this->ci->fm->getClientById($data['lesson']->client_id)->row()->name .  ' Registration Confirmation: ';
        $mail->Body = $this->ci->load->view('frontend/mail_lesson_confirmation', $data, true);
        $mail->isHTML(true);
    
        if($email_resend == '')
        {
            //echo $this->user->email;
            $mail->AddAddress($this->user->email, '');
        }
        else
        {
            //echo $email_resend;
            $mail->AddAddress($email_resend, '');
            
        }
    
        if(!$mail->Send())
            echo "Error sending: " . $mail->ErrorInfo;
    }
    
    public static function alreadyRegistered($lessonId, $userId)
    {
        $ci = & get_instance();
        $ci->load->model('Lesson_model', 'lm');
        
        return $ci->lm->alreadyRegistered($lessonId, $userId)->num_rows() >= 1;
    }
    
    public static function getLessonSuggestions($user, $roleId)
    {
        $ci = & get_instance();
        $ci->load->model('Lesson_model', 'lm');
        
        return $ci->lm->getLessonSuggestions2($user->client_id, $roleId);
    }
    
    public static function calendar($month, $year)
    {
        $ci = & get_instance();
        $ci->load->model('Lesson_model', 'lm');
        
        $number_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $days_of_month = array();
        for($i = 1 ; $i <= $number_days ; $i++)
        {
            $days_of_month[] = array(
                'day' => $i,
                'month' => $month,
                'year' => $year,
                'weekday' => MyLesson::getDayOfWeek(strtotime("$i-$month-$year")),
                'hasLesson' => $ci->lm->getLessonsByDate(date('Y-m-d 00:00:00', strtotime("$year-$month-$i 00:00:00")), MyAuth::getUser()->client_id)->num_rows() > 0 && strtotime("$year-$month-$i 00:00:00") >= strtotime(date('Y-m-d 00:00:00')),     
            );
            
        }
        
        $i = 0; 
        $html = '<tr>';
        foreach($days_of_month as $day)
        {
            while($i != $day['weekday'])
            {
                $html .= '<td></td>';
                $i++;
                if($i > 6)
                    $i = 0;
            }
        
            $html .= '<td' . ($day['hasLesson'] ? ' class="hasLesson"' : '') . '>' . ($day['hasLesson'] ? '<a href="#' . date('d-m-Y', strtotime($day['year'].'-'.$day['month'].'-'.$day['day'] . '00:00:00')) . '">' : '') . $day['day'] . ($day['hasLesson'] ? '</a>' : '') . '</td>';
        
            if($i == 6)
            {
                $i = 0;
                $html .= '</tr><tr>';
            }
            else
                $i++;
        }
        $html .= '</tr>';
        
        return array(
            'calendar' => $html,
            'monthname' => MyLang::langString('calendar_month_' . $month),
            'year' => $year,
        );
    }
    
    public static function getLessons()
    {
        $ci = & get_instance();
        $ci->load->model('Lesson_model', 'lm');
        
        $lessongroups = array();
        foreach($ci->lm->getLessonsByClient(MyAuth::getUser()->client_id)->result() as $lesson)
        {
            if(strtotime(date('d-m-Y', strtotime($lesson->startdate))) >= strtotime(date('Y-m-d 00:00:00')))
            {
                $key = date('d-m-Y', strtotime($lesson->startdate));
                if(!isset($lessongroups[$key]))
                {
                    $lessongroups[$key] = array(
                        'dayname' => MyLang::langString('calendar_weekday_' . MyLesson::getDayOfWeek(strtotime($key))) . ', <strong>' . date('d.m.Y', strtotime($lesson->startdate)) . '</strong>',
                        'lessons' => array($lesson),    
                    );
                }
                else 
                {
                    $lessongroups[$key]['lessons'][] = $lesson;
                }
            }
        }
        
        return $lessongroups;
    }
    
    public static function getDayOfWeek($date)
    {
        $wd = date('w', $date);
        $wd--;
        if($wd < 0)
            $wd = 6;
        
        return $wd;
    }
}


