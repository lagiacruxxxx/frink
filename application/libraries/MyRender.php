<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyRender
{
	protected $ci = null;
	protected $fm = null;
	
	function __construct()
	{
	    $this->ci = & get_instance();
	    $this->ci->load->model('Frontend_model', 'fm');
	    $this->fm = $this->ci->fm;
    }	
    
    public function mosaic()
    {
        $data = array();
        $client = $this->fm->getClientById(MyAuth::getUser()->client_id)->row();
        $clientId = $client->id;
        
        $techs = '';
        foreach($this->ci->fm->getTechsByClientId($clientId)->result() as $tech)
        {
            $techs .= ($techs != '' ? ',' : '') . $tech->id;
        }
        $contentitems = array();
        $raw_content = $this->fm->getContentByTech($techs)->result_array();
        
        $raw_content = unique_multidim_array($raw_content, 'id');
        
        foreach($raw_content as $contentitem)
        {
            $contentitem['metatags'] = '';
            $dummy = $contentitem['type'] != CONTENT_TYPE_LESSON ? $this->fm->getMetatagsByContent($contentitem['id']) : $this->fm->getMetatagsByLesson($contentitem['id']);
            foreach($dummy->result() as $tag)
            {
                $contentitem['metatags'] .= ($contentitem['metatags'] != '' ? ',' : '') . $tag->metatag_id;
            }
        
            $contentitem['techs'] = '';
            $dummy = $contentitem['type'] != CONTENT_TYPE_LESSON ? $this->fm->getTechsByContent($contentitem['id']) : $this->fm->getTechsByLesson($contentitem['id']);
            foreach($this->fm->getTechsByContent($contentitem['id'], $contentitem['type'] == CONTENT_TYPE_LESSON ? 'lesson_technology' : 'content_technology')->result() as $tech)
            {
                $contentitem['techs'] .= ($contentitem['techs'] != '' ? ',' : '') . $tech->technology_id;
            }
            $contentitem['teasers'] = $this->fm->getTeaserByContent($contentitem['id'], MyAuth::getUser()->active_lang)->row();
            
            switch($contentitem['type'])
            {
                case CONTENT_TYPE_ARTICLE:
                    $contentitem['url'] =  site_url('content/' . $contentitem['id']);
                    break;
                case CONTENT_TYPE_QUIZ:
                    $contentitem['url'] =  site_url('quiz/' . $contentitem['id']);
                    break;
                case CONTENT_TYPE_ASSESSMENT:
                    $contentitem['url'] =  site_url('assessment/' . $contentitem['id']);
                    break;
                case CONTENT_TYPE_LESSON:
                    $contentitem['url'] =  site_url('lesson/' . $contentitem['id']);
                    break;
                case CONTENT_TYPE_GALLERY:
                    $contentitem['url'] =  site_url('gallery/' . $contentitem['id']);
                    break;
            }
        
            if($this->fm->isContentAvailableForClient($clientId, $contentitem['id'])->num_rows() > 0)
                $contentitems[] = (object)$contentitem;
        }
        
        $data['contentitems'] = $contentitems;
        
        $this->__render('frontend/mosaic', $data);
    }
    
    public function profile()
    {
        $data = array();
        $user = MyAuth::getUser();
        
        $this->ci->load->model('Quiz_model', 'qm');
        $this->ci->load->model('Assessment_model', 'asm');
        
        $data['quizzes'] = array();
        foreach($this->ci->qm->getQuizResultsByUserId($user->id)->result() as $quiz)
        {
            $summary = MyQuiz::getResultSummary($quiz);
            
            $data['quizzes'][] = (object)array(
                'totalQuestions' => $summary['totalQuestions'],
                'correctQuestions' => $summary['correctQuestions'],
                'percent' => $summary['percent'],
                'points' => $summary['points'],
                'quiz' => $quiz,
            );
        }
        
        $data['assessments'] = array();
        foreach($this->ci->asm->getAssessmentResultByUser($user->id)->result() as $assessment)
        {
            $rolename = $this->ci->asm->getAssessmentRoleById($assessment->role_result);
            $data['assessments'][] = (object)array(
                'name' => $this->ci->asm->getAssessmentById($assessment->content_id)->row()->name,
                'assessment' => $assessment,
                'rolename' => $rolename->num_rows() == 1 ? $rolename->row()->name : '',
            );
        }
        
        $this->__render('frontend/profile', $data);
    }
    
    
    public function lessons($year, $month)
    {
        $data = array();
        
        if($year == null && $month == null)
        {
            $currentDate = new DateTime('first day of this month');
            $prevDate = (new DateTime('first day of this month'))->modify('-1 month');
            $nextDate = (new DateTime('first day of this month'))->modify('+1 month');
        }
        else
        {
            $currentDate = new DateTime("01-$month-$year");
            $prevDate = (new DateTime("01-$month-$year"))->modify('-1 month');
            $nextDate = (new DateTime("01-$month-$year"))->modify('+1 month');
        }
        
        $data['curData'] = MyLesson::calendar($currentDate->format('n'), $currentDate->format('Y'));
        $data['prevData'] = MyLesson::calendar($prevDate->format('n'), $prevDate->format('Y'));
        $data['nextData'] = MyLesson::calendar($nextDate->format('n'), $nextDate->format('Y'));
        $data['month'] = $currentDate->format('n');
        $data['year'] = $currentDate->format('Y');
        $data['nextmonth'] = $nextDate->format('n');
        $data['nextyear'] = $nextDate->format('Y');
        $data['prevmonth'] = $prevDate->format('n');
        $data['prevyear'] = $prevDate->format('Y');
        
        $data['lessongroups'] = MyLesson::getLessons();
        
        $this->__render('frontend/lessons', $data);
    }
    
    
    protected function __render($view , $content_data)
    {
        $data = array();
        $data['client'] = MyAuth::getUser() != null ? $this->fm->getClientById(MyAuth::getUser()->client_id)->row() : null;
        $data['user'] = MyAuth::getUser();
        $data['languages'] = MyAuth::getUser() != null ? $this->fm->getLanguagesByClientId($data['client']->id) : null;
        $data['menuitems'] = MyAuth::getUser() != null ? $this->fm->getMenuitemsByClientId($data['client']->id, MyAuth::getUser()->active_lang) : null;
        $data['rgb'] = MyAuth::getUser() != null ? array('color1' => hex2rgb($data['client']->color1), 'color2' => hex2rgb($data['client']->color2)): null;
        $data['techs'] = MyAuth::getUser() != null ? $this->fm->getTechsByClientId($data['client']->id) : null;
        $data['footertext'] = str_replace('::client::', $data['client'] != null ? $data['client']->name : null, MyLang::langString('footer_text'));
        $data['keywords'] = $this->fm->getSearchKeywords();
        
        $this->ci->load->helper('cookie');
        $data['filters']['tech'] = get_cookie('frink_academy_techfilter');
        $data['filters']['tag'] = get_cookie('frink_academy_tagfilter');
        
        
        $this->ci->load->view('frontend/head', $data);
        $this->ci->load->view('frontend/header', $data);
        $this->ci->load->view($view, $content_data);
        $this->ci->load->view('frontend/footer', $data);
    }
    
    public function __renderBackend($view, $content_data)
    {
        $data = array();
        $data['username'] = MyAuth::getUser() != null ? MyAuth::getUser()->username : null;
        $data['additional_css'] = isset($content_data['additional_css']) ? $content_data['additional_css'] : array();
        $data['additional_js'] = isset($content_data['additional_js']) ? $content_data['additional_js'] : array();
        
        $this->ci->load->view('backend/head', $data);
        $this->ci->load->view('backend/menu', $data);
        $this->ci->load->view($view, $content_data);
        $this->ci->load->view('backend/footer', $data);        
    }
    
    public function content($contentId, $pageType, $data = array(), $parent = null)
    {
        $user = MyAuth::getUser();
        $this->ci->load->model('entities/Content_model', 'cm');
        
        $data['client'] = $this->fm->getClientById($user->client_id)->row();
        $data['user'] = $user;
        $data['contentId'] = ($parent == null ? $contentId : $parent);
        $data['contentType'] = $pageType;
        $data['ga'] = $this->__getTrackerinfo($contentId, $pageType);
        if($this->__isAllowed(($parent == null ? $contentId : $parent), $user->client_id, $pageType))
        {
            $data['modules'] = array_merge(array(), $this->ci->cm->getModulesText($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesImage($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesMultipleChoice4($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesVideo($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesAccordionStart($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesAccordionEnd($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesSequence($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesAssessment($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesGallery($contentId, $user->active_lang, $pageType)->result_array());
            $data['modules'] = array_merge($data['modules'], $this->ci->cm->getModulesVimeo($contentId, $user->active_lang, $pageType)->result_array());
            
            usort($data['modules'], 'module_cmp');
        
            $this->__render('frontend/content', $data);
        }
        else
            redirect(site_url());
    }
    
    public function gallery($contentId)
    {
        $user = MyAuth::getUser();
        if($this->__isAllowed($contentId, $user->client_id, CONTENT_TYPE_GALLERY))
        {
            $data['galleryItems'] = $this->fm->getGalleryItemsByContentId($contentId);
            $data['gallery'] = $this->fm->getContentById($contentId)->row();
            $this->__render('frontend/gallery', $data);
        }
    }
    
    public function __isAllowed($contentId, $clientId, $pageType)
    {
        $allow = false;
        
        if($pageType != CONTENT_TYPE_LESSON)
        {
            if($this->fm->isContentAvailableForClient($clientId, $contentId)->num_rows() > 0)
            {
                foreach($this->fm->getTechsByClientId($clientId)->result() as $clientTech)
                {
                    foreach($this->fm->getTechsByContent($contentId)->result() as $contentTech)
                    {
                        if($clientTech->id == $contentTech->technology_id)
                            $allow = true;
                    }
                }
            }
        }
        else
        {
            foreach($this->fm->getTechsByClientId($clientId)->result() as $clientTech)
            {
                foreach($this->fm->getTechsByLesson($contentId)->result() as $contentTech)
                {
                    if($clientTech->id == $contentTech->technology_id)
                        $allow = true;
                }
            }            
        }
        return $allow;
    }
    
    private function __getTrackerinfo($contentId, $pageType)
    {
        
        switch($pageType)
        {
            case CONTENT_TYPE_QUIZ:
                $ga = array(
                    'type' => 'QUIZ',
                    'action' => 'START',
                    'name' => $this->fm->getContentById($contentId)->row()->name,
                );
                break;
            case CONTENT_TYPE_ARTICLE:
                $ga = array(
                    'type' => 'CONTENT',
                    'action' => 'VIEW',
                    'name' => $this->fm->getContentById($contentId)->row()->name,
                );
                break;
            case CONTENT_TYPE_ASSESSMENT:
                $ga = array(
                    'type' => 'ASSESSMENT',
                    'action' => 'START',
                    'name' => $this->fm->getContentById($contentId)->row()->name,
                );
                break;
                
            default:
                $ga = null;
                break;
        }
        
        return $ga;
    }
    
    public function search($query)
    {
        $data = array();
        $data['query'] = $query;
        $data['activeLang'] = $this->fm->getLanguageById(MyAuth::getUser()->active_lang)->row()->short;
        $data['clientId'] = MyAuth::getUser()->client_id;
        $data['techs'] =  $this->fm->getTechs();
        $this->__render('frontend/search', $data);
    }
}


