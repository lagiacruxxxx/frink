<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyQuiz extends MyRender
{
	protected $quiz = null;
	protected $user = null;
	protected $qm = null;
	
	function __construct($quizId, $user)
	{
	    parent::__construct();
	    
	    if(!$this->__isAllowed($quizId, $user->client_id, CONTENT_TYPE_QUIZ))
	        redirect(site_url());
	 
	    $this->user = $user;
	    $this->ci->load->model('Quiz_model', 'qm');
	    $this->qm = $this->ci->qm;
	    
	    if($quizId != null)
	        $this->quiz = $this->fm->getContentById($quizId)->row();
	}	
    
    public function startPage()
    {
        $this->content($this->quiz->id, CONTENT_TYPE_QUIZ);
    }
    
    public function start()
    {
        $result_id = $this->qm->createQuizResult(array(
            'content_id' => $this->quiz->id,
            'user_id' => $this->user->id,
        ));
        
        $this->ci->session->set_userdata('current_quiz', $this->quiz->id);
        $this->ci->session->set_userdata('current_question', -1);
        $this->ci->session->set_userdata('current_resultset', $result_id);
        
        redirect(site_url('quiz_next'));
    }
    
    public function next()
    {
        $q = $this->ci->session->userdata('current_question');
        
        
        if($q+1 < $this->qm->getQuestionsByQuizId($this->quiz->id)->num_rows() && (string)$q != 'finished')
        {
            $q++;
            $this->ci->session->set_userdata('current_question', $q);
            $this->__showQuestion($q);
        }      
        else
        {
            $this->ci->session->set_userdata('current_question', 'finished');
            $this->__showResults();
        }  
    }
    
    private function __showQuestion($questionOrder)
    {
        $q = $this->qm->getQuestionsByQuizId($this->quiz->id);
        
        $data = array();
        $data['question'] = $q->row($questionOrder); 
        $data['questionOrder'] = ($questionOrder + 1) . '/' . $q->num_rows();
        $this->content( $data['question']->id, CONTENT_TYPE_QUESTION, $data, $this->quiz->id);
    }
    
    public function answer($answer)
    {
        $questionOrder = $this->ci->session->userdata('current_question');
        
        if(in_array(intval($answer), array(1,2,3,4,5)))
        {
            $q = $this->qm->getQuestionsByQuizId($this->quiz->id)->row($questionOrder);
            $result_id = $this->ci->session->userdata('current_resultset');
            
            $correct = $this->__isCorrect($answer, $q->id);
            
            $this->qm->insertQuizResultQuestion(array(
                'quiz_result_id' => $result_id,
                'question_id' => $q->id,
                'answer' => $answer,
                'correct' => $correct,
                'time_left' => 0,
            ));
            
            echo json_encode(array(
                'success' => true,
                'correct' => $correct,
                'correctAnswer' => $this->__getCorrectAnswer($q->id)
            ));
        }
        else
            echo json_encode(array(
                'success' => false,
            ));
        
    }
    
    private function __isCorrect($answer, $question_id)
    {
        return $this->qm->getQuestionById($question_id)->row()->correct == $answer;
    }
    
    private function __getCorrectAnswer($question_id)
    {
        return $this->qm->getQuestionById($question_id)->row()->correct;
    }
    
    private function __showResults()
    {
        $result_id = $this->ci->session->userdata('current_resultset');
        
        $result = $this->qm->getQuizResultById($result_id)->row();
        if($this->user->id == $result->user_id && $this->quiz->id == $result->content_id)
        {
            $data = MyQuiz::getResultSummary($result);
            $data['quizId'] = $this->quiz->id;
            $data['user'] = $this->user;
            
            $data['quiz_results'] = $this->ci->load->view('frontend/quiz_result', $data, true);
            $this->content($this->quiz->id, CONTENT_TYPE_QUIZ_RESULT, $data);
        }
        else
            $this->mosaic();
        
    }
    
    public static function getResultSummary($result)
    {
        $ci = & get_instance();
        $ci->load->model('Quiz_model', 'qm');
        
        $totalQuestions = 0;
        $correctQuestions = 0;
        $points = 0;
        $totalPoints = 0;
    
        foreach($ci->qm->getQuizResultQuestions($result->id)->result() as $q)
        {
            $p = $ci->qm->getQuestionById($q->question_id)->row()->points;
            $totalQuestions++;
            $totalPoints += $p;
            if($q->correct)
            {
                $correctQuestions++;
                $points += $p;
            }
        }
        
        $questionsText = str_replace('::correct::', $correctQuestions, MyLang::langString('quiz_result_questions'));
        $questionsText = str_replace('::total::', $totalQuestions, $questionsText);
        $questionsText = str_replace('::points::', $points, $questionsText);
        $questionsText = str_replace('::maxpoints::', $totalPoints, $questionsText);
        
        $linkText = str_replace('::restart::', '<a href="' . site_url('quiz/' . $result->content_id) . '">' . MyLang::langString('quiz_result_link_clicktext') . '</a>', MyLang::langString('quiz_result_links'));
        $linkText = str_replace('::mosaic::', '<a href="' . site_url() . '">' . MyLang::langString('quiz_result_link_clicktext2') . '</a>', $linkText);
        
        return array(
            'totalQuestions' => $totalQuestions,
            'correctQuestions' => $correctQuestions,
            'questionsText' => $questionsText,
            'linkText' => $linkText,
            'percent' => $totalQuestions != 0 ? round(($correctQuestions / $totalQuestions) * 100) : 0,
            'points' => $points,
        );
    }
    
    public static function getWidgetResults()
    {
        $ci = & get_instance();
        $ci->load->model('Quiz_model', 'qm');
        
        $startdate = $ci->input->post('startdate') != '' ? $ci->input->post('startdate') : '01.01.2010';
        $enddate = $ci->input->post('enddate') != '' ? $ci->input->post('enddate') : date('d.m.Y');
        
        $results = $ci->qm->getQuizQueryResults($ci->input->post('clients'), $ci->input->post('lang'), $ci->input->post('startdate'), $ci->input->post('enddate'));
        
        return '<div class="widget_result">There were <strong>' . $results->num_rows() . '</strong> quizzes taken in that period.</div>';
    }
}


