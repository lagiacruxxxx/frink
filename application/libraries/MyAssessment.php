<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyAssessment extends MyRender
{
	protected $assessment = null;
	protected $user = null;
	protected $am = null;
	
	function __construct($assessmentId, $user)
	{
	    parent::__construct();
	    
	    if(!$this->__isAllowed($assessmentId, $user->client_id, CONTENT_TYPE_ASSESSMENT))
	        redirect(site_url());
	 
	    $this->user = $user;
	    $this->ci->load->model('Assessment_model', 'asm');
	    $this->asm = $this->ci->asm;
	    
	    if($assessmentId != null)
	        $this->assessment = $this->fm->getContentById($assessmentId)->row();
	}	
    
    public function startPage()
    {
        $this->content($this->assessment->id, CONTENT_TYPE_ASSESSMENT);
    }
    
    public function start()
    {
        $result_id = $this->asm->createAssessmentResult(array(
            'content_id' => $this->assessment->id,
            'user_id' => $this->user->id,
            'language_id' => $this->user->active_lang,
        ));
        
        $this->ci->session->set_userdata('current_assessment', $this->assessment->id);
        $this->ci->session->set_userdata('current_assessment_question', -1);
        $this->ci->session->set_userdata('current_resultset', $result_id);
        
        foreach($this->asm->getAssessmentRoles()->result() as $role)
        {
            $this->ci->session->set_userdata('assessment_role_' . $role->id, 0);
        }
        
        redirect(site_url('assessment_next'));
    }
    
    public function next()
    {
        $q = $this->ci->session->userdata('current_assessment_question');
        
        if($q+1 < $this->asm->getQuestionsByAssessmentId($this->assessment->id)->num_rows() && (string)$q != 'finished')
        {
            $q++;
            $this->ci->session->set_userdata('current_assessment_question', $q);
            $this->__showQuestion($q);
        }      
        else
        {
            $this->ci->session->set_userdata('current_assessment_question', 'finished');
            $this->__saveResults();
            $this->__showResults();
        }  
    }
    
    private function __showQuestion($questionOrder)
    {
        $q = $this->asm->getQuestionsByAssessmentId($this->assessment->id);
        
        $data = array();
        $data['question'] = $q->row($questionOrder); 
        $data['questionOrder'] = ($questionOrder + 1) . '/' . $q->num_rows();
        $this->content($data['question']->id, CONTENT_TYPE_ASSESSMENT_QUESTION, $data, $this->assessment->id);
    }
    
    public function answer($answer)
    {
        $questionOrder = $this->ci->session->userdata('current_assessment_question');
        
        
        if(in_array(intval($answer), array(1,2,3,4,5)))
        {
            $q = $this->asm->getQuestionsByAssessmentId($this->assessment->id)->row($questionOrder);
            $mod = $this->asm->getAssessmentModule($q->id, $this->user->active_lang, CONTENT_TYPE_ASSESSMENT_QUESTION)->row();
            switch(intval($answer))
            {
                case 1:
                    $role = $mod->answer1_role;
                    break;
                case 2:
                    $role = $mod->answer2_role;
                    break;
                case 3:
                    $role = $mod->answer3_role;
                    break;
                case 4:
                    $role = $mod->answer4_role;
                    break;
            }
            
            $count = $this->ci->session->userdata('assessment_role_' . $role);
            $count++;
            $this->ci->session->set_userdata('assessment_role_' . $role, $count);
            
            echo json_encode(array(
                'success' => true,
            ));
        }
        else
            echo json_encode(array(
                'success' => false,
            ));
        
    }
    
    private function __isCorrect($answer, $question_id)
    {
        return $this->qm->getQuestionById($question_id)->row()->correct == $answer;
    }
    
    private function __showResults()
    {
        $result_id = $this->ci->session->userdata('current_resultset');
        
        $result = $this->asm->getAssessmentResultById($result_id)->row();
        if($this->user->id == $result->user_id && $this->assessment->id == $result->content_id)
        {
            $data['result'] = $result;
            $data['role'] = $this->asm->getAssessmentRoleLangByRoleId($result->role_result, $this->user->active_lang)->row();
            $data['lessons'] = MyLesson::getLessonSuggestions($this->user, $result->role_result);
            $data['assessment_results'] = $this->ci->load->view('frontend/assessment_result', $data, true);
            $this->content($this->assessment->id, CONTENT_TYPE_ASSESSMENT_RESULT, $data);
        }
        else
            $this->mosaic();
    }
    
    private function __saveResults()
    {
        $role_id = null;
        $role_count = -1;
        $role_prio = -1;
        foreach($this->asm->getAssessmentRoles()->result() as $role)
        {
            if($this->ci->session->userdata('assessment_role_' . $role->id) > $role_count || ($this->ci->session->userdata('assessment_role_' . $role->id) == $role_count && $role->priority > $role_prio))
            {
                $role_id = $role->id;
                $role_count = $this->ci->session->userdata('assessment_role_' . $role->id);
                $role_prio = $role->priority;
            }
        }
        
        $this->asm->saveAssessmentResult($this->ci->session->userdata('current_resultset'), array('role_result' => $role_id));
    }
    
    public static function getWidgetResults()
    {
        $ci = & get_instance();
        $ci->load->model('Assessment_model', 'asm');
    
        $startdate = $ci->input->post('startdate') != '' ? $ci->input->post('startdate') : '01.01.2010';
        $enddate = $ci->input->post('enddate') != '' ? $ci->input->post('enddate') : date('d.m.Y');
        
        $results = $ci->asm->getAssessmentQueryResults($ci->input->post('clients'), $ci->input->post('lang'), $startdate, $enddate);
    
        return '<div class="widget_result">There were <strong>' . $results->num_rows() . '</strong> assessments taken in that period.</div>';
    }    
}


