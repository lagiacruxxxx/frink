<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Content extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Content_model', 'em');
    }  

	public function article()
	{
		$bc = new besc_crud();
		$bc->table('content');
		$bc->primary_key('id');
		$bc->title('Article');
		
		$bc->custom_buttons(array(
		    array(
		        'name' => 'Edit item',
                'icon' => site_url('items/backend/img/icon_editcontent.png'),
                'add_pk' => true,
                'url' => 'edit_content'),
		    array(
		        'name' => 'Edit teaser text',
		        'icon' => site_url('items/backend/img/icon_edit_teaser.png'),
		        'add_pk' => true,
		        'url' => 'edit_teasertext'),
		    array(
		        'name' => 'Clone article',
		        'icon' => site_url('items/backend/img/icon_clone.png'),
		        'add_pk' => true,
		        'url' => 'clone_article'),		    
		));
		
		$bc->where('type = ' . CONTENT_TYPE_ARTICLE);
		
		$bc->list_columns(array('name', 'content_technology_relation', 'content_client_relation', 'teaser_fname', 'visible'));
		$bc->filter_columns(array('name', 'content_technology_relation','content_client_relation'));
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Name',
	            'validation' => 'required',
	        ),
	        
	        'content_language_relation' => array
	        (
	            'relation_id' => 'content_language_relation',
	            'type' => 'm_n_relation',
	            'table_mn' => 'content_language',
	            'table_mn_pk' => 'id',
	            'table_mn_col_m' => 'content_id',
	            'table_mn_col_n' => 'language_id',
	            'table_m' => 'content',
	            'table_n' => 'language',
	            'table_n_pk' => 'id',
	            'table_n_value' => 'name',
	            'display_as' => 'Languages',
	            'box_width' => 150,
	            'box_height' => 150,
	        ),
	        
	        'content_metatag_relation' => array
	        (
	            'relation_id' => 'content_metatag_relation',
	            'type' => 'm_n_relation',
	            'table_mn' => 'content_metatag',
	            'table_mn_pk' => 'id',
	            'table_mn_col_m' => 'content_id',
	            'table_mn_col_n' => 'metatag_id',
	            'table_m' => 'content',
	            'table_n' => 'metatag',
	            'table_n_pk' => 'id',
	            'table_n_value' => 'name',
	            'display_as' => 'Metatags',
	            'box_width' => 150,
	            'box_height' => 150,
	            'filter' => false,
	        ),
	        
	        'content_technology_relation' => array
	        (
	            'relation_id' => 'content_technology_relation',
	            'type' => 'm_n_relation',
	            'table_mn' => 'content_technology',
	            'table_mn_pk' => 'id',
	            'table_mn_col_m' => 'content_id',
	            'table_mn_col_n' => 'technology_id',
	            'table_m' => 'content',
	            'table_n' => 'technology',
	            'table_n_pk' => 'id',
	            'table_n_value' => 'name',
	            'display_as' => 'Technologies',
	            'box_width' => 150,
	            'box_height' => 150,
	            'filter' => false,
	        ),
	        
	        'content_client_relation' => array
	        (
	            'relation_id' => 'content_client_relation',
	            'type' => 'm_n_relation',
	            'table_mn' => 'client_content',
	            'table_mn_pk' => 'id',
	            'table_mn_col_m' => 'content_id',
	            'table_mn_col_n' => 'client_id',
	            'table_m' => 'content',
	            'table_n' => 'client',
	            'table_n_pk' => 'id',
	            'table_n_value' => 'name',
	            'display_as' => 'Clients',
	            'box_width' => 150,
	            'box_height' => 150,
	            'filter' => false,
	        ),
	        
	        'visible' => array
	        (
	            'db_name' => 'visible',
	            'type' => 'select',
	            'display_as' => 'Visibility status',
	            'options' => array(
	                array(
                        'key' => 1,
	                    'value' => 'Visible'   
	                ),
	                array(
	                    'key' => 0,
	                    'value' => 'Hidden',
	                )
	            ),
	        ),

	        
	        'teaser_fname' => array(
	            'db_name' => 'teaser_fname',
	            'type' => 'image',
	            'display_as' => 'Teaser image (normal)',
	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>200x200 px',
	            'accept' => '.png,.jpg,.jpeg',
	            'uploadpath' => 'items/uploads/content',
                'crop' => array(
                    'ratio' => '200:200',
                    'minWidth' => 200,
                    'minHeight' => 200,
                    'maxWidth' => 400,
                    'maxHeight' => 400,
                ),
	        ),
	        
	        'teaser_fname_big' => array(
	            'db_name' => 'teaser_fname_big',
	            'type' => 'image',
	            'display_as' => 'Teaser image (citation)',
	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>214x263 px',
	            'accept' => '.png,.jpg,.jpeg',
	            'uploadpath' => 'items/uploads/content',
	            'crop' => array(
	                'ratio' => '214:263',
	                'minWidth' => 214,
	                'minHeight' => 263,
	                'maxWidth' => 428,
	                'maxHeight' => 526,
	            ),
	        ),
	        
	        'teaser_fname_3' => array(
	            'db_name' => 'teaser_fname_3',
	            'type' => 'image',
	            'display_as' => 'Teaser image (3 part)',
	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>662x263 px',
	            'accept' => '.png,.jpg,.jpeg',
	            'uploadpath' => 'items/uploads/content',
	            'crop' => array(
	                'ratio' => '662:263',
	                'minWidth' => 662,
	                'minHeight' => 263,
	                'maxWidth' => 1324,
	                'maxHeight' => 526,
	            ),
	        ),
	        
	        'teaser_type' => array
	        (
	            'db_name' => 'teaser_type',
	            'type' => 'select',
	            'display_as' => 'Active Teaser Type',
	            'options' => array(
	                array('key' => TEASER_TYPE_NORMAL, 'value' => 'Teaser image (normal)',),
	                array('key' => TEASER_TYPE_CITATION, 'value' => 'Teaser image (citation)'),
	                array('key' => TEASER_TYPE_3, 'value' => 'Teaser image (3 part)'),
	            ),
	        ),
	        
	        'priority' => array
	        (
	            'db_name' => 'priority',
	            'type' => 'text',
	            'display_as' => 'Priority',
	            'col_info' => 'High value = first item',
	            'validation' => 'numeric',
	        ),
	        	        
	        'type' => array(
	            'db_name' => 'type',
	            'type' => 'hidden',
	            'value' => CONTENT_TYPE_ARTICLE,
	        ),
	        


	        
	    ));
		
		$data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	
	
	public function gallery()
	{
	    $bc = new besc_crud();
	    $bc->table('content');
	    $bc->primary_key('id');
	    $bc->title('Gallery');
	
	    $bc->custom_buttons(array(
	        array(
	            'name' => 'Edit teaser text',
	            'icon' => site_url('items/backend/img/icon_edit_teaser.png'),
	            'add_pk' => true,
	            'url' => 'edit_teasertext'),
	        array(
	            'name' => 'Edit gallery images',
	            'icon' => site_url('items/backend/img/icon_gallery.png'),
	            'add_pk' => true,
	            'url' => 'edit_gallery'),	        
	    ));
	
	    $bc->where('type = ' . CONTENT_TYPE_GALLERY);
	
	    $bc->list_columns(array('name', 'content_technology_relation', 'content_client_relation', 'teaser_fname', 'visible'));
	    $bc->filter_columns(array('name', 'content_technology_relation', 'content_client_relation'));
	
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	             
	            'content_language_relation' => array
	            (
	                'relation_id' => 'content_language_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_language',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'language_id',
	                'table_m' => 'content',
	                'table_n' => 'language',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Languages',
	                'box_width' => 150,
	                'box_height' => 150,
	            ),
	             
	            'content_metatag_relation' => array
	            (
	                'relation_id' => 'content_metatag_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_metatag',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'metatag_id',
	                'table_m' => 'content',
	                'table_n' => 'metatag',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Metatags',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'content_technology_relation' => array
	            (
	                'relation_id' => 'content_technology_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_technology',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'technology_id',
	                'table_m' => 'content',
	                'table_n' => 'technology',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Technologies',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'content_client_relation' => array
	            (
	                'relation_id' => 'content_client_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'client_content',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'client_id',
	                'table_m' => 'content',
	                'table_n' => 'client',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Clients',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'visible' => array
	            (
	                'db_name' => 'visible',
	                'type' => 'select',
	                'display_as' => 'Visibility status',
	                'options' => array(
	                    array(
	                        'key' => 1,
	                        'value' => 'Visible'
	                    ),
	                    array(
	                        'key' => 0,
	                        'value' => 'Hidden',
	                    )
	                ),
	            ),
	
	             
	            'teaser_fname' => array(
	                'db_name' => 'teaser_fname',
	                'type' => 'image',
	                'display_as' => 'Teaser image (normal)',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>200x200 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/content',
	                'crop' => array(
	                    'ratio' => '200:200',
	                    'minWidth' => 200,
	                    'minHeight' => 200,
	                    'maxWidth' => 400,
	                    'maxHeight' => 400,
	                ),
	            ),
	             
	            'teaser_fname_big' => array(
	                'db_name' => 'teaser_fname_big',
	                'type' => 'image',
	                'display_as' => 'Teaser image (citation)',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>214x263 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/content',
	                'crop' => array(
	                    'ratio' => '214:263',
	                    'minWidth' => 214,
	                    'minHeight' => 263,
	                    'maxWidth' => 428,
	                    'maxHeight' => 526,
	                ),
	            ),
	             
	            'teaser_fname_3' => array(
	                'db_name' => 'teaser_fname_3',
	                'type' => 'image',
	                'display_as' => 'Teaser image (3 part)',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>662x263 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/content',
	                'crop' => array(
	                    'ratio' => '662:263',
	                    'minWidth' => 662,
	                    'minHeight' => 263,
	                    'maxWidth' => 1324,
	                    'maxHeight' => 526,
	                ),
	            ),
	             
	            'teaser_type' => array
	            (
	                'db_name' => 'teaser_type',
	                'type' => 'select',
	                'display_as' => 'Active Teaser Type',
	                'options' => array(
	                    array('key' => TEASER_TYPE_NORMAL, 'value' => 'Teaser image (normal)',),
	                    array('key' => TEASER_TYPE_CITATION, 'value' => 'Teaser image (citation)'),
	                    array('key' => TEASER_TYPE_3, 'value' => 'Teaser image (3 part)'),
	                ),
	            ),
	             
	            'priority' => array
	            (
	                'db_name' => 'priority',
	                'type' => 'text',
	                'display_as' => 'Priority',
	                'col_info' => 'High value = first item',
	                'validation' => 'numeric',
	            ),
	
	            'type' => array(
	                'db_name' => 'type',
	                'type' => 'hidden',
	                'value' => CONTENT_TYPE_GALLERY,
	            ),
	             
	
	
	             
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('backend/crud', $data);
	}
	
	
	public function edit_gallery($gallery_id)
	{
	    $bc = new besc_crud();
	    $bc->table('gallery_item');
	    $bc->primary_key('id');
	    $bc->title('Gallery image');
	    
	    $bc->where('content_id = ' . $gallery_id);
	    
	    $bc->list_columns(array('fname', 'description'));
	    $bc->ordering(array(
	        'ordering' => 'ordering',
	        'value' => 'description',
	    ));
	    
	    $bc->columns(array
	        (
	            'fname' => array(
	                'db_name' => 'fname',
	                'type' => 'image',
	                'display_as' => 'Gallery image',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/gallery',
	            ),
	            
	            'description' => array
	            (
	                'db_name' => 'description',
	                'type' => 'multiline',
	                'display_as' => 'Description text',
	                'validation' => 'required',
	            ),
	            
	            'content_id' => array(
	                'db_name' => 'content_id',
	                'type' => 'hidden',
	                'value' => $gallery_id,
	            ),
	        ));
	    
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('backend/crud', $data);	    
	}
	
	
	
	public function lesson()
	{
	    $bc = new besc_crud();
	    $bc->table('lesson');
	    $bc->primary_key('id');
	    $bc->title('Training lesson');
	
	    $bc->custom_buttons(array(
	        array(
	            'name' => 'Registrations',
	            'icon' => site_url('items/backend/img/icon_registration.png'),
	            'add_pk' => true,
	            'url' => 'lesson_registration'),	        
	    ));
	
	    $bc->list_columns(array('name', 'client_id', 'lesson_technology_relation', 'lesson_assessment_role_relation', 'teacher', 'focus', 'startdate', 'visible'));
	    $bc->filter_columns(array('name', 'client_id', 'lesson_technology_relation', 'lesson_assessment_role_relation', 'teacher'));
	
	    $teachers = array();
	    foreach($this->em->getUsers()->result() as $user)
	    {
	        $teachers[] = array(
                'key' => $user->id,
	            'value' => $user->firstname . ' ' . $user->lastname,   
	        );
	    }
	    
	    $clients = array();
	    foreach($this->em->getClients()->result() as $client)
	    {
	        $clients[] = array(
	            'key' => $client->id,
	            'value' => $client->name,
	        );
	    }
	    
	    $bc->columns(array
	        (
	             
	             
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	            
	            'client_id' => array
	            (
	                'db_name' => 'client_id',
	                'type' => 'combobox',
	                'display_as' => 'Client',
	                'options' => $clients, 
	            ),
	            
	            'lessone_language_relation' => array
	            (
	                'relation_id' => 'lesson_language_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'lesson_language',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'lesson_id',
	                'table_mn_col_n' => 'language_id',
	                'table_m' => 'lesson',
	                'table_n' => 'language',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Languages',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'lesson_metatag_relation' => array
	            (
	                'relation_id' => 'lesson_metatag_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'lesson_metatag',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'lesson_id',
	                'table_mn_col_n' => 'metatag_id',
	                'table_m' => 'lesson',
	                'table_n' => 'metatag',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Metatags',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'lesson_technology_relation' => array
	            (
	                'relation_id' => 'lesson_technology_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'lesson_technology',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'lesson_id',
	                'table_mn_col_n' => 'technology_id',
	                'table_m' => 'content',
	                'table_n' => 'technology',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Technologies',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	            
	            'content_client_relation' => array
	            (
	                'relation_id' => 'content_client_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'client_content',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'client_id',
	                'table_m' => 'content',
	                'table_n' => 'client',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Clients',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'lesson_assessment_role_relation' => array
	            (
	                'relation_id' => 'lesson_assessment_role_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'lesson_assessment_role',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'lesson_id',
	                'table_mn_col_n' => 'assessment_role_id',
	                'table_m' => 'lesson',
	                'table_n' => 'assessment_role',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Assessment roles',
	                'box_width' => 150,
	                'box_height' => 150,
	            ),
	             
	            'visible' => array
	            (
	                'db_name' => 'visible',
	                'type' => 'select',
	                'display_as' => 'Visibility status',
	                'options' => array(
	                    array(
	                        'key' => 1,
	                        'value' => 'Visible'
	                    ),
	                    array(
	                        'key' => 0,
	                        'value' => 'Hidden',
	                    )
	                ),
	            ),
	
	            'teacher' => array
	            (
	                'db_name' => 'teacher',
	                'type' => 'select',
	                'display_as' => 'Teacher',
	                'options' => $teachers,
	            ),
	            
	            'focus' => array
	            (
	                'db_name' => 'focus',
	                'type' => 'text',
	                'display_as' => 'Focus',
	            ),
	            
	            'desc' => array(
	                'db_name' => 'desc',
	                'type' => 'multiline',
	                'display_as' => 'Description',
	                'height' => 130,
	            ),
	            
	            'startdate' => array
	            (
	                'db_name' => 'startdate',
	                'type' => 'date',
	                'display_as' => 'Date',
	                'edit_format' => 'dd.mm.yy',
	                'list_format' => 'd.m.Y',
	                'defaultvalue' => date('d.m.Y'),
	            ),
	            
	            'starttime' => array
	            (
	                'db_name' => 'starttime',
	                'type' => 'text',
	                'display_as' => 'Time',
	                'col_info' => 'e.g.: "19:00 CEST"',
	            ),
	            
	            'duration' => array
	            (
	                'db_name' => 'duration',
	                'type' => 'text',
	                'display_as' => 'Duration (min)',
	                'col_info' => 'Length of the lesson in minutes',
	            ),
	            
	            'location' => array(
	                'db_name' => 'location',
	                'type' => 'multiline',
	                'display_as' => 'Location',
	                'height' => 130,
	            ),
	            
	             
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('backend/crud', $data);
	}	
	
	
	public function lesson_registration($lessonId)
	{
	    $bc = new besc_crud();
	    $bc->table('lesson_registration_stats');
	    $bc->primary_key('id');
	    $bc->title('Training lesson registrations');
	
	    $bc->list_columns(array('firstname', 'lastname', 'username', 'email', 'clientname'));
	    $bc->filter_columns(array('username', 'clientname'));
	
	    $bc->where('lesson_id = ' . $lessonId);
	    
	    $bc->unset_add();
	    $bc->unset_delete();
	    $bc->unset_edit();
	    
	    $bc->custom_actions(array(
	        array(
	            'name' => 'Go back',
	            'url' => site_url('entities/Content/lesson'),
	            'add_pk' => false,
	            'icon' => site_url('items/backend/img/menu_go_back.png'),
	        ),

	        array(
	            'name' => 'Export lesson registration',
	            'url' => site_url('entities/Content/lesson_registration_export/' . $lessonId),
	            'add_pk' => false,
	            'icon' => site_url('items/backend/img/export.png'),
	        ),
	        

	    ));
	    
	    $bc->columns(array
        (
            'clientname' => array
            (
                'db_name' => 'clientname',
                'type' => 'text',
                'display_as' => 'Client',
            ),
            'firstname' => array
            (
                'db_name' => 'firstname',
                'type' => 'text',
                'display_as' => 'Firstname',
            ),
            'lastname' => array
            (
                'db_name' => 'lastname',
                'type' => 'text',
                'display_as' => 'Lastname',
            ),
            'username' => array
            (
                'db_name' => 'username',
                'type' => 'text',
                'display_as' => 'Username',
            ),
            'email' => array
            (
                'db_name' => 'email',
                'type' => 'text',
                'display_as' => 'E-mail',
            ),
        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('backend/crud', $data);
	}	
	
	public function lesson_registration_export($lessonId)
	{
	    $this->load->helper('file');
	    $this->load->dbutil();
	    $this->load->helper('download');
	
	    header('Content-type: text/csv');
	    header('Content-disposition: attachment;filename=FRINK_quiz_stats__' . date('Y-m-d') . '.csv');
	    echo "sep=,\n";
	    echo $this->dbutil->csv_from_result($this->em->getLessonRegistrationStats($lessonId));
	}
	
	
	public function quiz()
	{
	    $bc = new besc_crud();
	    $bc->table('content');
	    $bc->primary_key('id');
	    $bc->title('Quiz');
	
	    $bc->custom_buttons(array(
	        array(
	            'name' => 'Edit item',
	            'icon' => site_url('items/backend/img/icon_quiz.png'),
	            'add_pk' => true,
	            'url' => 'quiz_question'),
	        array(
	            'name' => 'Edit landing page',
	            'icon' => site_url('items/backend/img/icon_startpage.png'),
	            'add_pk' => true,
	            'url' => 'edit_content'),
	        array(
	            'name' => 'Edit result page',
	            'icon' => site_url('items/backend/img/icon_resultpage.png'),
	            'add_pk' => true,
	            'url' => 'edit_result_content'),
	        array(
	            'name' => 'Edit teaser text',
	            'icon' => site_url('items/backend/img/icon_edit_teaser.png'),
	            'add_pk' => true,
	            'url' => 'edit_teasertext'),	        	        
	    ));
	
	    $bc->where('type = ' . CONTENT_TYPE_QUIZ);
	
	    $bc->list_columns(array('name', 'content_technology_relation', 'content_client_relation', 'teaser_fname', 'visible'));
	    $bc->filter_columns(array('name', 'content_technology_relation', 'content_client_relation'));
	
	    $bc->columns(array
	        (
	             
	             
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	             
	            'content_language_relation' => array
	            (
	                'relation_id' => 'content_language_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_language',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'language_id',
	                'table_m' => 'content',
	                'table_n' => 'language',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Languages',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'content_metatag_relation' => array
	            (
	                'relation_id' => 'content_metatag_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_metatag',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'metatag_id',
	                'table_m' => 'content',
	                'table_n' => 'metatag',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Metatags',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'content_technology_relation' => array
	            (
	                'relation_id' => 'content_technology_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_technology',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'technology_id',
	                'table_m' => 'content',
	                'table_n' => 'technology',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Technologies',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	            
	            'content_client_relation' => array
	            (
	                'relation_id' => 'content_client_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'client_content',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'client_id',
	                'table_m' => 'content',
	                'table_n' => 'client',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Clients',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'visible' => array
	            (
	                'db_name' => 'visible',
	                'type' => 'select',
	                'display_as' => 'Visibility status',
	                'options' => array(
	                    array(
	                        'key' => 1,
	                        'value' => 'Visible'
	                    ),
	                    array(
	                        'key' => 0,
	                        'value' => 'Hidden',
	                    )
	                ),
	            ),
             
	             
                'teaser_fname' => array(
    	            'db_name' => 'teaser_fname',
    	            'type' => 'image',
    	            'display_as' => 'Teaser image (normal)',
    	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>200x200 px',
    	            'accept' => '.png,.jpg,.jpeg',
    	            'uploadpath' => 'items/uploads/content',
                    'crop' => array(
                        'ratio' => '200:200',
                        'minWidth' => 200,
                        'minHeight' => 200,
                        'maxWidth' => 400,
                        'maxHeight' => 400,
                    ),
    	        ),
    	        
    	        'teaser_fname_big' => array(
    	            'db_name' => 'teaser_fname_big',
    	            'type' => 'image',
    	            'display_as' => 'Teaser image (citation)',
    	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>214x268 px',
    	            'accept' => '.png,.jpg,.jpeg',
    	            'uploadpath' => 'items/uploads/content',
    	            'crop' => array(
    	                'ratio' => '214:268',
    	                'minWidth' => 214,
    	                'minHeight' => 268,
    	                'maxWidth' => 428,
    	                'maxHeight' => 536,
    	            ),
    	        ),
    	        
    	        'teaser_fname_3' => array(
    	            'db_name' => 'teaser_fname_3',
    	            'type' => 'image',
    	            'display_as' => 'Teaser image (3 part)',
    	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>662x254 px',
    	            'accept' => '.png,.jpg,.jpeg',
    	            'uploadpath' => 'items/uploads/content',
    	            'crop' => array(
    	                'ratio' => '662:254',
    	                'minWidth' => 662,
    	                'minHeight' => 254,
    	                'maxWidth' => 1324,
    	                'maxHeight' => 508,
    	            ),
    	        ),
    	        
    	        'teaser_type' => array
    	        (
    	            'db_name' => 'teaser_type',
    	            'type' => 'select',
    	            'display_as' => 'Active Teaser Type',
    	            'options' => array(
    	                array('key' => TEASER_TYPE_NORMAL, 'value' => 'Teaser image (normal)',),
    	                array('key' => TEASER_TYPE_CITATION, 'value' => 'Teaser image (citation)'),
    	                array('key' => TEASER_TYPE_3, 'value' => 'Teaser image (3 part)'),
    	            ),
    	        ),
	             
	            'priority' => array
	            (
	                'db_name' => 'priority',
	                'type' => 'text',
	                'display_as' => 'Priority',
	                'col_info' => 'High value = first item',
	                'validation' => 'numeric',
	            ),
	            
	            'type' => array(
	                'db_name' => 'type',
	                'type' => 'hidden',
	                'value' => CONTENT_TYPE_QUIZ,
	            ),
	            
	        ));
	
	    $data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	
	public function quiz_question($contentId)
	{
	    $bc = new besc_crud();
	    $bc->table('quiz_question');
	    $bc->primary_key('id');
	    $bc->title('Quiz questions');
	    
	    $bc->custom_actions(array(
	        array(
	            'name' => 'Go back',
	            'url' => site_url('entities/Content/quiz'),
	            'add_pk' => false,
	            'icon' => site_url('items/backend/img/menu_go_back.png'),
	        ),
	    ));
	    
	    $bc->custom_buttons(array(
		    array(
		        'name' => 'Edit item',
                'icon' => site_url('items/backend/img/icon_editcontent.png'),
                'add_pk' => true,
                'url' => site_url('entities/content/edit_question')),
	    ));
	    
	    $bc->where(array('content_id' => $contentId));
	    
	    $bc->order_by_field('order');
		$bc->order_by_direction('asc');
	    
	    $bc->list_columns(array('name', 'timelimit', 'points', 'correct'));
	    $bc->ordering(array(
	        'ordering' => 'order',
	        'value' => 'name',
	    ));
	    
	    $bc->columns(array
        (
            'name' => array
            (
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
                'validation' => 'required',
            ),
            
            'timelimit' => array
            (
                'db_name' => 'timelimit',
                'type' => 'text',
                'display_as' => 'Timelimit',
                'col_info'=> 'in seconds',
                'validation' => 'required|is_natural',
            ),
            
            'points' => array
            (
                'db_name' => 'points',
                'type' => 'text',
                'display_as' => 'Points',
                'validation' => 'required|is_natural',
            ),
    
            'content_id' => array(
                'db_name' => 'content_id',
                'type' => 'hidden',
                'value' => $contentId,
            ),
            
            'correct' => array(
                'db_name' => 'correct',
                'type' => 'select',
                'display_as' => 'Correct',
                'options' => array(
                    array(
                        'key' => 1,
                        'value' => 'Answer 1',
                    ),
                    array(
                        'key' => 2,
                        'value' => 'Answer 2',
                    ),
                    array(
                        'key' => 3,
                        'value' => 'Answer 3',
                    ),
                    array(
                        'key' => 4,
                        'value' => 'Answer 4',
                    )
                ),
            ),
        ));
	    
	    $data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	
	public function quiz_stats()
	{
	    $bc = new besc_crud();
	    $bc->table('quiz_stats');
	    $bc->primary_key('id');
	    $bc->title('Quiz statistics');
	     
	    $bc->unset_add();
	    $bc->unset_delete();
	    $bc->unset_edit();
	    
	    $bc->custom_actions(array(
	        array(
	            'name' => 'Export quiz statistics',
	            'url' => site_url('entities/Content/quiz_stats_export'),
	            'add_pk' => false,
	            'icon' => site_url('items/backend/img/export.png'),
	        ),
	    ));
	    
	    $bc->list_columns(array('quizname', 'created_date', 'percent', 'points', 'firstname', 'lastname', 'username', 'client_id', 'lang_id'));

	    $bc->filter_columns(array('quizname', 'client_id', 'username', 'lang_id'));
	     
	    $langoptions = array();
	    foreach($this->em->getLanguage()->result() as $lang)
	    {
	        $langoptions[] = array(
	            'key' => $lang->id,
	            'value' => $lang->short,
	        );
	    }
	     
	    $clientoptions = array();
	    foreach($this->em->getClients()->result() as $client)
	    {
	        $clientoptions[] = array(
	            'key' => $client->id,
	            'value' => $client->name,
	        );
	    }
	    
	    $bc->columns(array
	        (
	            'quizname' => array
	            (
	                'db_name' => 'quizname',
	                'type' => 'text',
	                'display_as' => 'Quizname',
	            ),
	    
	            'created_date' => array
	            (
	                'db_name' => 'created_date',
	                'type' => 'text',
	                'display_as' => 'Timestamp',
	            ),
	    
	            'percent' => array
	            (
	                'db_name' => 'percent',
	                'type' => 'text',
	                'display_as' => 'percent',
	            ),
	    
	            'points' => array(
	                'db_name' => 'points',
	                'type' => 'text',
	                'display_as' => 'Points',
	            ),
	            
	            'client_id' => array
	            (
	                'db_name' => 'client_id',
	                'type' => 'select',
	                'display_as' => 'Client',
	                'options' => $clientoptions,
	            ),
	            
	            'lang_id' => array
	            (
	                'db_name' => 'lang_id',
	                'type' => 'select',
	                'display_as' => 'Language',
	                'options' => $langoptions,
	            ),
	            
	            'username' => array(
	                'db_name' => 'username',
	                'type' => 'text',
	                'display_as' => 'Username',
	            ),
	            
	            'lastname' => array
	            (
	                'db_name' => 'lastname',
	                'type' => 'text',
	                'display_as' => 'Lastname',
	            ),
	             
	            'firstname' => array
	            (
	                'db_name' => 'firstname',
	                'type' => 'text',
	                'display_as' => 'Firstname',
	            ),
	             
	            
	             
	        ));
	     
	    $data['crud_data'] = $bc->execute();
	    
	    $widget = array();
	    $widget['clients'] = $this->em->getClients();
	    $widget['techs'] =  $this->em->getTechnology();
	    $widget['language'] = $this->em->getLanguage();
	    $data['widget'] = $this->load->view('backend/quiz_widget', $widget, true);
	    
	    $this->render->__renderBackend('backend/crudplus', $data);
	}
	
	public function quiz_stats_export()
	{
	    $this->load->helper('file');
	    $this->load->dbutil();
	    $this->load->helper('download');
	     
	    header('Content-type: text/csv');
	    header('Content-disposition: attachment;filename=FRINK_quiz_stats__' . date('Y-m-d') . '.csv');
	    echo "sep=,\n";
	    echo $this->dbutil->csv_from_result($this->em->getQuizStats());
	}
	
	
	public function assessment_role()
	{
	    $bc = new besc_crud();
	    $bc->table('assessment_role');
	    $bc->primary_key('id');
	    $bc->title('Assessment role');
	   
	    $bc->filter_columns(array('name'));
	    
	    $bc->custom_buttons(array(
	        array(
	            'name' => 'Edit role text',
	            'icon' => site_url('items/backend/img/icon_edit_teaser.png'),
	            'add_pk' => true,
	            'url' => 'edit_assessment_role_language'),
	    ));
	     
	    $bc->columns(array
        (
            'name' => array
            (
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
                'validation' => 'required',
            ),
            
            'priority' => array
            (
                'db_name' => 'priority',
                'type' => 'text',
                'display_as' => 'Priority',
                'validation' => 'required',
            ),
             
        ));
	     
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('backend/crud', $data);
	}
	
	public function edit_assessment_role_language($roleId)
	{
	    $data['languages'] = array();
	    $data['role'] = $this->em->getAssessmentRoleById($roleId)->row();
	     
	    foreach($this->em->getLanguage()->result() as $lang)
	    {
	        $role_lang = $this->em->getAssessmentRoleLangByRoleId($roleId, $lang->id);
	        $data['languages'][$lang->id] = array(
	            'role_name' => $role_lang->num_rows() == 1 ? $role_lang->row() : null,
	            'lang_name' => $lang->name,
	        );
	    }
	     
	    $data['crud_data'] = $this->load->view('backend/edit_assessment_role', $data, true);
	    $this->render->__renderBackend('backend/crud', $data);
	}
	
	public function save_assessment_role_lang()
	{
	
	    if($this->input->post('roleNames') != null)
	    {
	        $batch = array();
	        foreach($this->input->post('roleNames') as $item)
	        {
	            $this->em->deleteAssessmentRoleLang($item['role_id'], $item['lang_id']);
	
	            $batch[] = array(
	                'role_id' => $item['role_id'],
	                'language_id' => $item['lang_id'],
	                'name' => $item['name'],
	            );
	        }
	
	        $this->em->insertAssessmentRoleLang($batch);
	    }
	
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );
	}
	
	public function assessment()
	{
	    $bc = new besc_crud();
	    $bc->table('content');
	    $bc->primary_key('id');
	    $bc->title('Assessment');
	    
	    $bc->custom_buttons(array(
	        array(
	            'name' => 'Edit item',
	            'icon' => site_url('items/backend/img/icon_quiz.png'),
	            'add_pk' => true,
	            'url' => 'assessment_question'),
	        array(
	            'name' => 'Edit landing page',
	            'icon' => site_url('items/backend/img/icon_startpage.png'),
	            'add_pk' => true,
	            'url' => 'edit_content'),
	        array(
	            'name' => 'Edit result page',
	            'icon' => site_url('items/backend/img/icon_resultpage.png'),
	            'add_pk' => true,
	            'url' => 'edit_result_content'),	     
	        array(
	            'name' => 'Edit teaser text',
	            'icon' => site_url('items/backend/img/icon_edit_teaser.png'),
	            'add_pk' => true,
	            'url' => 'edit_teasertext'),
	    ));
	    
	    $bc->where('type = ' . CONTENT_TYPE_ASSESSMENT);
	    
	    $bc->list_columns(array('name', 'content_technology_relation', 'content_client_relation', 'teaser_fname', 'visible'));
	    $bc->filter_columns(array('name', 'content_technology_relation', 'content_client_relation'));
	    
	    $bc->columns(array
	        (
	    
	    
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	    
	           'content_language_relation' => array
	            (
	                'relation_id' => 'content_language_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_language',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'language_id',
	                'table_m' => 'content',
	                'table_n' => 'language',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Languages',
	                'box_width' => 150,
	                'box_height' => 150,
	            ),
	             
	            'content_metatag_relation' => array
	            (
	                'relation_id' => 'content_metatag_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_metatag',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'metatag_id',
	                'table_m' => 'content',
	                'table_n' => 'metatag',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Metatags',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	             
	            'content_technology_relation' => array
	            (
	                'relation_id' => 'content_technology_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'content_technology',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'technology_id',
	                'table_m' => 'content',
	                'table_n' => 'technology',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Technologies',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	            
	            'content_client_relation' => array
	            (
	                'relation_id' => 'content_client_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'client_content',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'content_id',
	                'table_mn_col_n' => 'client_id',
	                'table_m' => 'content',
	                'table_n' => 'client',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'name',
	                'display_as' => 'Clients',
	                'box_width' => 150,
	                'box_height' => 150,
	                'filter' => false,
	            ),
	    
	            'visible' => array
	            (
	                'db_name' => 'visible',
	                'type' => 'select',
	                'display_as' => 'Visibility status',
	                'options' => array(
	                    array(
	                        'key' => 1,
	                        'value' => 'Visible'
	                    ),
	                    array(
	                        'key' => 0,
	                        'value' => 'Hidden',
	                    )
	                ),
	            ),
	    
	            'teaser_fname' => array(
    	            'db_name' => 'teaser_fname',
    	            'type' => 'image',
    	            'display_as' => 'Teaser image (normal)',
    	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>200x200 px',
    	            'accept' => '.png,.jpg,.jpeg',
    	            'uploadpath' => 'items/uploads/content',
                    'crop' => array(
                        'ratio' => '200:200',
                        'minWidth' => 200,
                        'minHeight' => 200,
                        'maxWidth' => 400,
                        'maxHeight' => 400,
                    ),
    	        ),
    	        
    	        'teaser_fname_big' => array(
    	            'db_name' => 'teaser_fname_big',
    	            'type' => 'image',
    	            'display_as' => 'Teaser image (citation)',
    	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>214x268 px',
    	            'accept' => '.png,.jpg,.jpeg',
    	            'uploadpath' => 'items/uploads/content',
    	            'crop' => array(
    	                'ratio' => '214:268',
    	                'minWidth' => 214,
    	                'minHeight' => 268,
    	                'maxWidth' => 428,
    	                'maxHeight' => 536,
    	            ),
    	        ),
    	        
    	        'teaser_fname_3' => array(
    	            'db_name' => 'teaser_fname_3',
    	            'type' => 'image',
    	            'display_as' => 'Teaser image (3 part)',
    	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>662x254 px',
    	            'accept' => '.png,.jpg,.jpeg',
    	            'uploadpath' => 'items/uploads/content',
    	            'crop' => array(
    	                'ratio' => '662:254',
    	                'minWidth' => 662,
    	                'minHeight' => 254,
    	                'maxWidth' => 1324,
    	                'maxHeight' => 508,
    	            ),
    	        ),
    	        
    	        'teaser_type' => array
    	        (
    	            'db_name' => 'teaser_type',
    	            'type' => 'select',
    	            'display_as' => 'Active Teaser Type',
    	            'options' => array(
    	                array('key' => TEASER_TYPE_NORMAL, 'value' => 'Teaser image (normal)',),
    	                array('key' => TEASER_TYPE_CITATION, 'value' => 'Teaser image (citation)'),
    	                array('key' => TEASER_TYPE_3, 'value' => 'Teaser image (3 part)'),
    	            ),
    	        ),
	            
	            'priority' => array
	            (
	                'db_name' => 'priority',
	                'type' => 'text',
	                'display_as' => 'Priority',
	                'col_info' => 'High value = first item',
	                'validation' => 'numeric',
	            ),
	    
                'type' => array
	            (
	                'db_name' => 'type',
	                'type' => 'hidden',
	                'value' => CONTENT_TYPE_ASSESSMENT,
	            ),
	    
	    
	        ));
	    
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('backend/crud', $data);
	}
	
	public function assessment_question($contentId)
	{
	    $bc = new besc_crud();
	    $bc->table('assessment_question');
	    $bc->primary_key('id');
	    $bc->title('Assessment questions');
	     
	    $bc->custom_actions(array(
	        array(
	            'name' => 'Go back',
	            'url' => site_url('entities/Content/assessment'),
	            'add_pk' => false,
	            'icon' => site_url('items/backend/img/menu_go_back.png'),
	        ),
	    ));
	    
	    $bc->custom_buttons(array(
	        array(
	            'name' => 'Edit item',
	            'icon' => site_url('items/backend/img/icon_editcontent.png'),
	            'add_pk' => true,
	            'url' => site_url('entities/content/edit_assessment_question')),
	    ));
	     
	    $bc->where(array('content_id' => $contentId));
	     
	    $bc->order_by_field('order');
	    $bc->order_by_direction('asc');
	     
	    $bc->list_columns(array('name', 'timelimit'));
	    $bc->ordering(array(
	        'ordering' => 'order',
	        'value' => 'name',
	    ));
	     
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	
	            'timelimit' => array
	            (
	                'db_name' => 'timelimit',
	                'type' => 'text',
	                'display_as' => 'Timelimit',
	                'col_info'=> 'in seconds',
	                'validation' => 'required|is_natural',
	            ),
	
	            'content_id' => array(
	                'db_name' => 'content_id',
	                'type' => 'hidden',
	                'value' => $contentId,
	            ),
	
	        ));
	     
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('backend/crud', $data);
	}
	
	
	public function assessment_stats()
	{
	    $bc = new besc_crud();
	    $bc->table('assessment_stats');
	    $bc->primary_key('id');
	    $bc->title('Assessment statistics');
	
	    $bc->unset_add();
	    $bc->unset_delete();
	    $bc->unset_edit();
	     
	    $bc->custom_actions(array(
	        array(
	            'name' => 'Export assessment statistics',
	            'url' => site_url('entities/Content/assessment_stats_export'),
	            'add_pk' => false,
	            'icon' => site_url('items/backend/img/export.png'),
	        ),
	    ));
	     
	    $bc->list_columns(array('assessment_name', 'created_date', 'role_result_name', 'firstname', 'lastname', 'username', 'client_id', 'lang_id'));
	
	    $bc->filter_columns(array('assessment_name', 'client_id', 'username', 'lang_id'));
	    
	    $langoptions = array();
	    foreach($this->em->getLanguage()->result() as $lang)
	    {
	        $langoptions[] = array(
                'key' => $lang->id,
	            'value' => $lang->short,
	        );
	    }
	    
	    $clientoptions = array();
	    foreach($this->em->getClients()->result() as $client)
	    {
	        $clientoptions[] = array(
	            'key' => $client->id,
	            'value' => $client->name,
	        );
	    }
	
	    $bc->columns(array
	        (
	            'assessment_name' => array
	            (
	                'db_name' => 'assessment_name',
	                'type' => 'text',
	                'display_as' => 'Assessment',
	            ),
	             
	            'created_date' => array
	            (
	                'db_name' => 'created_date',
	                'type' => 'text',
	                'display_as' => 'Timestamp',
	            ),
	             
	            'role_result_name' => array
	            (
	                'db_name' => 'role_result_name',
	                'type' => 'text',
	                'display_as' => 'Result',
	            ),
	             
	            'client_id' => array
	            (
	                'db_name' => 'client_id',
	                'type' => 'select',
	                'display_as' => 'Client',
	                'options' => $clientoptions,
	            ),
	            
	            'lang_id' => array
	            (
	                'db_name' => 'lang_id',
	                'type' => 'select',
	                'display_as' => 'Language',
	                'options' => $langoptions,
	            ),
	
	            'username' => array(
	                'db_name' => 'username',
	                'type' => 'text',
	                'display_as' => 'Username',
	            ),
	             
	            'lastname' => array
	            (
	                'db_name' => 'lastname',
	                'type' => 'text',
	                'display_as' => 'Lastname',
	            ),
	
	            'firstname' => array
	            (
	                'db_name' => 'firstname',
	                'type' => 'text',
	                'display_as' => 'Firstname',
	            ),
	
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $widget = array();
	    $widget['clients'] = $this->em->getClients();
	    $widget['techs'] =  $this->em->getTechnology();
	    $widget['language'] = $this->em->getLanguage();
	    $data['widget'] = $this->load->view('backend/assessment_widget', $widget, true);
	    
	    $this->render->__renderBackend('backend/crudplus', $data);
	}
	
	
	public function assessment_stats_export()
	{
	    $this->load->helper('file');
	    $this->load->dbutil();
	    $this->load->helper('download');
	
	    header('Content-type: text/csv');
	    header('Content-disposition: attachment;filename=FRINK_assessment_stats__' . date('Y-m-d') . '.csv');
	    echo "sep=,\n";
	    echo $this->dbutil->csv_from_result($this->em->getAssessmentStats());
	}
	
	
	
	
	public function edit_teasertext($contentId)
	{
	    $data['languages'] = array();
	    $data['content'] = $this->em->getContent($contentId)->row();
	    
	    foreach($this->em->getLanguagesByContentId($contentId)->result() as $lang)
	    {
            $data['languages'][$lang->id] = array(
                'teaser' => $this->em->getTeaserByContentId($contentId, $lang->id)->num_rows() == 1 ? $this->em->getTeaserByContentId($contentId, $lang->id)->row() : null,
                'name' => $lang->name,
            );
	    }
	        
	    $data['crud_data'] = $this->load->view('backend/edit_teasertext', $data, true);
	    $this->render->__renderBackend('backend/crud', $data);
	}
	
	public function edit_content($contentId, $language = NULL)
	{
	    $content = $this->em->getContent($contentId)->row();
	    $data['languages'] = $this->em->getLanguagesByContentId($contentId);
	    $data['id'] = $content->id;
	    $data['contentType'] = $content->type;
	    $data['name'] = $content->name; 
	    $data['quiz'] = 0;
	    $data['assessment'] = 0;
	    $data['correct'] = 0;
	    $this->__loadContentEditor($contentId, $language, $data);
	}
	
	public function edit_lesson($contentId, $language = NULL)
	{
	    $content = $this->em->getLesson($contentId)->row();
	    $data['languages'] = $this->em->getLanguagesByLessonId($contentId);
	    $data['id'] = $content->id;
	    $data['contentType'] = CONTENT_TYPE_LESSON;
	    $data['name'] = $content->name;
	    $data['quiz'] = 0;
	    $data['assessment'] = 0;
	    $data['correct'] = 0;
	    $this->__loadContentEditor($contentId, $language, $data);
	}
	
	public function edit_result_content($contentId, $language = NULL)
	{
	    $content = $this->em->getContent($contentId)->row();
	    $data['languages'] = $this->em->getLanguagesByContentId($contentId);
	    $data['id'] = $content->id;
	    $data['contentType'] = $content->type == CONTENT_TYPE_QUIZ ? CONTENT_TYPE_QUIZ_RESULT : CONTENT_TYPE_ASSESSMENT_RESULT;
	    $data['name'] = $content->name;
	    $data['quiz'] = 0;
	    $data['assessment'] = 0;
	    $data['correct'] = 0;
	    $this->__loadContentEditor($contentId, $language, $data);	    
	}
	
	public function edit_question($questionId, $language = NULL)
	{
	    $question = $this->em->getQuestionById($questionId)->row();
	    $quiz = $this->em->getContent($question->content_id)->row();
	    $data['languages'] = $this->em->getLanguagesByContentId($quiz->id);
	    $data['id'] = $question->id;
	    $data['contentType'] = CONTENT_TYPE_QUESTION;
	    $data['name'] = $quiz->name . ' - ' . $question->name;
	    $data['quiz'] = $quiz->id;
	    $data['assessment'] = 0;
	    $data['correct'] = $question->correct;
	    $this->__loadContentEditor($questionId, $language, $data);
	}
	
	public function edit_assessment_question($questionId, $language = NULL)
	{
	    $question = $this->em->getAssessmentQuestionById($questionId)->row();
	    $assessment = $this->em->getContent($question->content_id)->row();
	    $data['languages'] = $this->em->getLanguagesByContentId($assessment->id);
	    $data['roles'] = $this->em->getAssessmentRoles();
	    $data['id'] = $question->id;
	    $data['contentType'] = CONTENT_TYPE_ASSESSMENT_QUESTION;
	    $data['name'] = $assessment->name . ' - ' . $question->name;
	    $data['quiz'] = 0;
	    $data['correct'] = 0;
	    $data['assessment'] = $assessment->id;
	    $this->__loadContentEditor($questionId, $language, $data);
	}
	
	private function __loadContentEditor($id, $language, $data)
	{
	    $data['currentLang'] =  $language == NULL ? $data['languages']->row()->id : $language;
	    
	    $data['modules'] = array_merge(array(), $this->em->getModulesText($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesImage($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesMultipleChoice4($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesVideo($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesAccordionStart($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesAccordionEnd($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesSequence($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesAssessment($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesGallery($id, $data['currentLang'], $data['contentType'])->result_array());
	    $data['modules'] = array_merge($data['modules'], $this->em->getModulesVimeo($id, $data['currentLang'], $data['contentType'])->result_array());
	     
	    usort($data['modules'], 'module_cmp');
	     
	    $data['crud_data'] = $this->load->view('backend/edit_content', $data, true);
	    $this->render->__renderBackend('backend/crud', $data);
	}
	

	
	
	public function save_content()
	{

	    $contentId = $this->input->post('id');
	    $langId = $this->input->post('language_id');
	    $contentType = $this->input->post('contentType');
	    
	    $this->em->deleteModulesText($contentId, $langId, $contentType);
	    $this->em->deleteModulesVideo($contentId, $langId, $contentType);
	    $this->em->deleteModulesImage($contentId, $langId, $contentType);
	    $this->em->deleteModulesMultipleChoice4($contentId, $langId, $contentType);
	    $this->em->deleteModulesAccordionStart($contentId, $langId, $contentType);
	    $this->em->deleteModulesAccordionEnd($contentId, $langId, $contentType);
	    $this->em->deleteModulesSequence($contentId, $langId, $contentType);
	    $this->em->deleteModulesAssessment($contentId, $langId, $contentType);
	    $this->em->deleteModulesGallery($contentId, $langId, $contentType);
	    $this->em->deleteModulesVimeo($contentId, $langId, $contentType);
	     
	    if(isset($_POST['modules']))
	    {
	        foreach($_POST['modules'] as $module)
	        {
	            switch($module['type'])
	            {
	                case 'text':
	                    $this->em->insertModuleText(array(
                            'item_id' => $contentId,
                            'language_id' => $langId,
                            'content' => $module['content'],
                            'order' => $module['order'],
                            'pageType' => $contentType,
	                    ));
	                    break;
	                    
	                case 'image':
	                    $this->em->insertModuleImage(array(
	                        'item_id' => $contentId,
	                        'language_id' => $langId,
	                        'fname' => $module['fname'],
	                        'order' => $module['order'],
	                        'stretch' => $module['stretch'],
	                        'align' => $module['align'],
	                        'caption' => $module['caption'],
                            'pageType' => $contentType,
	                    ));
	                    break;
	                    
                    case 'multiplechoice4':
                        $this->em->insertModuleMultipleChoice4(array(
                            'item_id' => $contentId,
                            'language_id' => $langId,
                            'order' => $module['order'],
                            'question' => $module['question'],
                            'answer1' => $module['answer1'],
                            'answer2' => $module['answer2'],
                            'answer3' => $module['answer3'],
                            'answer4' => $module['answer4'],
                            'correct_answer' => 0,
                            'pageType' => $contentType,
                        ));
                        break;
                        
                    case 'video':
                        $this->em->insertModuleVideo(array(
                            'item_id' => $contentId,
                            'language_id' => $langId,
                            'code' => $module['code'],
                            'start' => $module['start'],
                            'pageType' => $contentType,
                            'order' => $module['order'],
                        ));
                        break;
                        
                    case 'accordion_start':
                        $this->em->insertModuleAccordionStart(array(
                            'item_id' => $contentId,
                            'language_id' => $langId,
                            'headline' => $module['headline'],
                            'pageType' => $contentType,
                            'order' => $module['order'],
                        ));
                        break;     
                        
                    case 'accordion_end':
                        $this->em->insertModuleAccordionEnd(array(
                            'item_id' => $contentId,    
                            'language_id' => $langId,
                            'pageType' => $contentType,
                            'order' => $module['order'],
                        ));
                        break;
                    
                    case 'sequence':
                        $insert_id = $this->em->insertModuleSequence(array(
                            'item_id' => $contentId,
                            'language_id' => $langId,
                            'order' => $module['order'],
                            'pageType' => $contentType,
                            'orientation' => $module['orientation'],
                        ));
                        $batch = array();
                        foreach($module['items'] as $item)
                        {
                            $batch[] = array(
                                'sequence_id' => $insert_id,
                                'fname' => $item['img'],
                                'description' => $item['desc'],
                                'ordering' => $item['ordering'],
                                'headline' => $item['headline'],    
                            );
                        }
                        $this->em->insertModuleSequenceItem($batch);
                        break;
                        
                    case 'assessment':
                        $this->em->insertModuleAssessment(array(
                        'item_id' => $contentId,
                        'language_id' => $langId,
                        'order' => $module['order'],
                        'question' => $module['question'],
                        'answer1' => $module['answer1'],
                        'answer1_role' => $module['answer1_role'],
                        'answer2' => $module['answer2'],
                        'answer2_role' => $module['answer2_role'],
                        'answer3' => $module['answer3'],
                        'answer3_role' => $module['answer3_role'],
                        'answer4' => $module['answer4'],
                        'answer4_role' => $module['answer4_role'] != '' ? $module['answer4_role'] : 0,
                        'pageType' => $contentType,
                        ));
                        break;
                        
                    case 'gallery':
                        $insert_id = $this->em->insertModuleGallery(array(
                            'item_id' => $contentId,
                            'language_id' => $langId,
                            'order' => $module['order'],
                            'pageType' => $contentType,
                        ));
                        $batch = array();
                        foreach($module['items'] as $item)
                        {
                            $batch[] = array(
                                'gallery_id' => $insert_id,
                                'fname' => $item['fname'],
                                'ordering' => $item['ordering'],
                            );
                        }
                        $this->em->insertModuleGalleryItem($batch);
                        break;
                        
                    case 'vimeo':
                        $this->em->insertModuleVimeo(array(
                        'item_id' => $contentId,
                        'language_id' => $langId,
                        'code' => $module['code'],
                        'pageType' => $contentType,
                        'order' => $module['order'],
                        ));
                        break;
	            }
	        }
	    }
	    
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );
	}
	
	
	public function clone_article($contentId)
	{
	    // clone content
	    $content = $this->em->getContent($contentId)->row_array();
	    unset($content['id']);
        $content['name'] .= ' (clone)';
        	    
	    $newContentId = $this->em->clone_content($content);
	    
	    
	    // content language
	    $content_language = array();
	    foreach($this->em->getLanguagesByContentId($contentId)->result() as $langs)
	    {
	        $content_language[] = array(
                'content_id' => $newContentId,
	            'language_id' => $langs->id   
	        );
	    }
	    if($content_language != array())
	        $this->em->clone_batch('content_language', $content_language);	
	    
	    
	    // content metatag
	    $content_metatag = array();
	    foreach($this->em->getMetatagByContentId($contentId)->result() as $metatag)
	    {
	        $content_metatag[] = array(
	            'content_id' => $newContentId,
	            'metatag_id' => $metatag->metatag_id,
	        );
	    }
	    if($content_metatag != array())
	        $this->em->clone_batch('content_metatag', $content_metatag);	    
	    
	    
	    // content technology
	    $content_technology = array();
	    foreach($this->em->getTechnologyByContentId($contentId)->result() as $tech)
	    {
	        $content_technology[] = array(
	            'content_id' => $newContentId,
	            'technology_id' => $tech->technology_id,
	        );
	    }
	    if($content_technology != array())
	        $this->em->clone_batch('content_technology', $content_technology);
        
	    
	    // content teaser
	    $content_teaser = array();
	    foreach($this->em->getTeaserByContentIdClone($contentId)->result() as $teaser)
	    {
	        $content_teaser[] = array(
	            'content_id' => $newContentId,
	            'lang_id' => $teaser->lang_id,
	            'teaser_text' => $teaser->teaser_text,
	            'teaser_subtext' => $teaser->teaser_subtext,
	        );
	    }
	    if($content_teaser != array())
	        $this->em->clone_batch('content_teaser', $content_teaser);
	    
	     
	    // client content
	    $client_content = array();
	    foreach($this->em->getClientsByContentId($contentId)->result() as $cl)
	    {
	        $client_content[] = array(
	            'content_id' => $newContentId,
	            'client_id' => $cl->client_id,
	        );
	    }
	    if($client_content != array())
	        $this->em->clone_batch('client_content', $client_content);	    
	    
	    
	    // module accordion start
	    $acc_start = array();
	    foreach($this->em->getModuleAccordionStart($contentId)->result() as $mod)
	    {
	        $acc_start[] = array(
	            'item_id' => $newContentId,
	            'language_id' => $mod->language_id,
	            'pageType' => $mod->pageType,
	            'order' => $mod->order,
	            'headline' => $mod->headline,
	        );
	    }
	    if($acc_start != array())
	        $this->em->clone_batch('module_accordion_start', $acc_start);	    
	    
	    // module accordion end
	    $acc_end = array();
	    foreach($this->em->getModuleAccordionEnd($contentId)->result() as $mod)
	    {
	        $acc_end[] = array(
	            'item_id' => $newContentId,
	            'language_id' => $mod->language_id,
	            'pageType' => $mod->pageType,
	            'order' => $mod->order,
	        );
	    }
	    if($acc_end != array())
	        $this->em->clone_batch('module_accordion_end', $acc_end);
	     
	    
	    // module image
	    $img = array();
	    foreach($this->em->getModuleImage($contentId)->result_array() as $mod)
	    {
	        unset($mod['id']);
	        $mod['item_id'] = $newContentId;
	        $img[] = $mod;
	    }
	    if($img != array())
	        $this->em->clone_batch('module_image', $img);
	    
	    // module text
	    $text = array();
	    foreach($this->em->getModuleText($contentId)->result_array() as $mod)
	    {
	        unset($mod['id']);
	        $mod['item_id'] = $newContentId;
	        $text[] = $mod;
	    }
	    if($text != array())
	        $this->em->clone_batch('module_text', $text);
	    
	    
	    // module video
	    $video = array();
	    foreach($this->em->getModuleVideo($contentId)->result_array() as $mod)
	    {
	        unset($mod['id']);
	        $mod['item_id'] = $newContentId;
	        $video[] = $mod;
	    }
	    if($video != array())
	        $this->em->clone_batch('module_video', $video);	   

	    // module vimeo
	    $vimeo = array();
	    foreach($this->em->getModuleVimeo($contentId)->result_array() as $mod)
	    {
	        unset($mod['id']);
	        $mod['item_id'] = $newContentId;
	        $vimeo[] = $mod;
	    }
	    if($vimeo != array())
	        $this->em->clone_batch('module_vimeo', $vimeo);	    
	    
	    
	    // module gallery
	    foreach($this->em->getModuleGallery($contentId)->result_array() as $mod)
	    {
	        $galleryId = $mod['id'];
	        unset($mod['id']);
	        $mod['item_id'] = $newContentId;
	        
	        $newGalleryId = $this->em->clone_gallery($mod);
	        
	        $galleryItems = array();
	        foreach($this->em->getModuleGalleryItem($galleryId)->result_array() as $gal)
	        {
	            unset($gal['id']);
	            $gal['gallery_id'] = $newGalleryId;
	            $galleryItems[] = $gal;
	        }
	        if($galleryItems != array())
	            $this->em->clone_batch('module_gallery_item', $galleryItems);	        
	    }

	     
	    // module sequence
	    foreach($this->em->getModuleSequence($contentId)->result_array() as $mod)
	    {
	        $sequenceId = $mod['id'];
	        unset($mod['id']);
	        $mod['item_id'] = $newContentId;
	         
	        $newSequenceId = $this->em->clone_sequence($mod);
	         
	        $sequenceItems = array();
	        foreach($this->em->getModuleSequenceItem($sequenceId)->result_array() as $seq)
	        {
	            unset($seq['id']);
	            $seq['sequence_id'] = $newSequenceId;
	            $sequenceItems[] = $seq;
	        }
	        if($sequenceItems != array())
	            $this->em->clone_batch('module_sequence_item', $sequenceItems);
	    }
	     
	    
	    
	    redirect('entities/Content/article', 'refresh');
	}
	
	public function save_teaser()
	{
	     
	    if($this->input->post('teasers') != null)
	    {
	        $batch = array();
	        foreach($this->input->post('teasers') as $item)
	        {
                $this->em->deleteTeaser($item['content_id'], $item['lang_id']);
                
	            $batch[] = array(
	                'content_id' => $item['content_id'],
	                'lang_id' => $item['lang_id'],
	                'teaser_subtext' => $item['teaser_subtext'],
	                'teaser_text' => $item['teaser_text'],
	            );
	        }
	         
	        $this->em->insertTeaser($batch);
	    }
	     
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );	    
	}
	
}

