<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Settings extends Backend 
{
    function __construct()
    {
        parent::__construct();
    }  

	public function language()
	{
	    $bc = new besc_crud();
		$bc->table('language');
		$bc->primary_key('id');
		$bc->title('Language');
		
		$bc->list_columns(array('name', 'short'));
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Language',
	            'validation' => 'required',
	        ),
	        
	        'short' => array
	        (
	            'db_name' => 'short',
	            'type' => 'text',
	            'display_as' => 'Abbreviation',
	            'validation' => 'required',
	        ),
	        
	        'search_defaulttext' => array
	        (
	            'db_name' => 'search_defaulttext',
	            'type' => 'text',
	            'display_as' => 'Defaulttext for searchbar',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Search ..."',
	        ),
	        
	        'sidebar_account' => array
	        (
	            'db_name' => 'sidebar_account',
	            'type' => 'text',
	            'display_as' => 'Sidebar "Account" headline',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Account"',
	        ),
	        
	        'sidebar_login' => array
	        (
	            'db_name' => 'sidebar_login',
	            'type' => 'text',
	            'display_as' => 'Sidebar "Login" button',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Login"',
	        ),
	        
	        'sidebar_logout' => array
	        (
	            'db_name' => 'sidebar_logout',
	            'type' => 'text',
	            'display_as' => 'Sidebar "Logout" button',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Logout"',
	        ),	   

	        'sidebar_profile' => array
	        (
	            'db_name' => 'sidebar_profile',
	            'type' => 'text',
	            'display_as' => 'Sidebar "Profile" button',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Profile"',
	        ),
	        
	        'quiz_start_btn' => array
	        (
	            'db_name' => 'quiz_start_btn',
	            'type' => 'text',
	            'display_as' => 'Quiz start button',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Start quiz"',
	        ),
	        
	        'assessment_start_btn' => array
	        (
	            'db_name' => 'assessment_start_btn',
	            'type' => 'text',
	            'display_as' => 'Assessment start button',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Start assessment"',
	        ),
	        
	        'lesson_register_btn' => array
	        (
	            'db_name' => 'lesson_register_btn',
	            'type' => 'text',
	            'display_as' => 'Register for lessons button',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Register for lesson"',
	        ),
	        
	        'lesson_register_success' => array
	        (
	            'db_name' => 'lesson_register_success',
	            'type' => 'multiline',
	            'display_as' => 'Lesson registration success',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Registration successful. You will receive an E-mail shortly."',
	        ),
	        
	        'lesson_register_already' => array
	        (
	            'db_name' => 'lesson_register_already',
	            'type' => 'multiline',
	            'display_as' => 'Lesson registration success',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "You have already registered for this lesson."',
	        ),
	        
	        'sequence_enlarge' => array
	        (
	            'db_name' => 'sequence_enlarge',
	            'type' => 'text',
	            'display_as' => 'Sequence module enlarge image text',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Click to enlarge"',
	        ),
	        
	        'menu_lesson' => array
	        (
	            'db_name' => 'menu_lesson',
	            'type' => 'text',
	            'display_as' => 'Sidebar "Lesson available" headline',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Training lessons"',
	        ),
	        
	        'menu_lesson_available' => array
	        (
	            'db_name' => 'menu_lesson_available',
	            'type' => 'text',
	            'display_as' => 'Sidebar "Available lessons" menu',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Available training lessons"',
	        ),
	        
	        'assessment_result_header' => array
	        (
	            'db_name' => 'assessment_result_header',
	            'type' => 'text',
	            'display_as' => 'Text for assessment result page (header)',
	            'validation' => 'required|max_length[500]',
	            'col_info' => 'EN: "We have determined the matching role for you:"',
	        ),
	        
	        'assessment_result_suggestions' => array
	        (
	            'db_name' => 'assessment_result_suggestions',
	            'type' => 'text',
	            'display_as' => 'Text for assessment result page (subtext)',
	            'validation' => 'required|max_length[500]',
	            'col_info' => 'EN: "These are some training lessons we suggest for you"',
	        ),
	        
	        'question_next' => array
	        (
	            'db_name' => 'question_next',
	            'type' => 'text',
	            'display_as' => 'Next button on top of questions',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Next"',
	        ),
	        
	        'question' => array
	        (
	            'db_name' => 'question',
	            'type' => 'text',
	            'display_as' => '"Question"',
	            'validation' => 'required|max_length[255]',
	            'col_info' => 'EN: "Question"',
	        ),
	        
	        'footer_text' => array
	        (
	            'db_name' => 'footer_text',
	            'type' => 'multiline',
	            'display_as' => 'License text in footer',
	            'validation' => 'required',
	            
	        ),
	        
	        'quiz_result_questions' => array
	        (
	            'db_name' => 'quiz_result_questions',
	            'type' => 'multiline',
	            'display_as' => 'Quiz results text',
	            'validation' => 'required',
	             
	        ),

	        'quiz_result_links' => array
	        (
	            'db_name' => 'quiz_result_links',
	            'type' => 'multiline',
	            'display_as' => 'Quiz result links',
	            'validation' => 'required',
	             
	        ),	 

	        'quiz_result_link_clicktext' => array
	        (
	            'db_name' => 'quiz_result_link_clicktext',
	            'type' => 'text',
	            'display_as' => '::restart:: for result link text',
	            'validation' => 'required|max_length[255]',
	        ),

	        'quiz_result_link_clicktext2' => array
	        (
	            'db_name' => 'quiz_result_link_clicktext2',
	            'type' => 'text',
	            'display_as' => '::mosaic:: for result link text',
	            'validation' => 'required|max_length[255]',
	        ),
	         
	        'search_term' => array
	        (
	            'db_name' => 'search_term',
	            'type' => 'text',
	            'display_as' => 'Search term',
	            'validation' => 'required|max_length[255]',
	        ),
	        
	        
	        'search_result' => array
	        (
	            'db_name' => 'search_result',
	            'type' => 'text',
	            'display_as' => 'Search result',
	            'validation' => 'required|max_length[255]',
	        ),

	        'calendar_headline' => array
	        (
	            'db_name' => 'calendar_headline',
	            'type' => 'text',
	            'display_as' => 'Calendar headline',
	            'validation' => 'required|max_length[255]',
	        ),
	         
	        
	        'calendar_subline' => array
	        (
	            'db_name' => 'calendar_subline',
	            'type' => 'text',
	            'display_as' => 'Calendar subline',
	            'validation' => 'required|max_length[255]',
	        ),
	         


		));
		
		$data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	
	public function search_keywords()
	{
	    $bc = new besc_crud();
	    $bc->table('search_keywords');
	    $bc->primary_key('id');
	    $bc->title('Keywords');
	
	    $bc->order_by_field('keyword');
	    $bc->order_by_direction('asc');
	     
	    $bc->list_columns(array('keyword'));
	    $bc->filter_columns(array('keyword'));
	
	    $bc->columns(array
        (
            'keyword' => array(
                'db_name' => 'keyword',
                'type' => 'text',
                'display_as' => 'Keyword',
                'validation' => 'required|is_unique[search_keywords.keyword]|max_length[255]|min_length[3]',
            ),
             
        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('backend/crud', $data);
	}	
	
}
