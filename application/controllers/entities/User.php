<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class User extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/User_model', 'em');
    }  

	public function client()
	{
		$bc = new besc_crud();
		$bc->table('client');
		$bc->primary_key('id');
		$bc->title('Client');
		
		$bc->order_by_field('name');
		$bc->order_by_direction('asc');
		
		$bc->list_columns(array('name', 'fname', 'client_technology_relation', 'client_language_relation'));
		$bc->filter_columns(array('name', 'client_technology_relation', 'client_language_relation'));
		
		$bc->custom_buttons(array(
		    array(
		        'name' => 'Edit menu',
		        'icon' => site_url('items/backend/img/icon_menu.png'),
		        'add_pk' => true,
		        'url' => 'edit_menu'),
		    array(
		        'name' => 'Edit menu',
		        'icon' => site_url('items/backend/img/icon_client_select.png'),
		        'add_pk' => true,
		        'url' => 'client_content'),		    
		));
		
		$languageOptions = array();
		foreach($this->em->getLanguages()->result() as $lang)
		{
		    $languageOptions[] = array(
                'key' => $lang->id,
		        'value' => $lang->name,		        
		    );
		}
		
		$helpOptions = array();
		foreach($this->em->getArticles()->result() as $help)
		{
		    $helpOptions[] = array(
                'key' => $help->id,
		        'value' => $help->name,  
		    );
		}
		
		$hasLessonsOptions = array(
		    array(
		        'key' => 0, 'value' => 'NO',
		    ),
		    array(
		        'key' => 1, 'value' => 'YES',
		    ),
		);
		
		
		$bc->columns(array
	    (
	        'name' => array(
                'db_name' => 'name',
	            'type' => 'text',
	            'display_as' => 'Name',
	            'validation' => 'required',
	        ),
	        
            'fname' => array(
                'db_name' => 'fname',
                'type' => 'image',
                'display_as' => 'Client logo',
                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>300x300 px',
                'accept' => '.png,.jpg,.jpeg',
                'uploadpath' => 'items/uploads/client',
                'crop' => array(
                    'ratio' => '300:300',
                    'minWidth' => 300,
                    'minHeight' => 300,
                    'maxWidth' => 600,
                    'maxHeight' => 600,
                ),
            ),
	        
	        'color1' => array(
	            'db_name' => 'color1',
	            'type' => 'colorpicker',
	            'display_as' => 'Main color',
	            'validation' => 'required',
	            'hexinput' => true,
	        ),
	        
	        'color2' => array(
	            'db_name' => 'color2',
	            'type' => 'colorpicker',
	            'display_as' => 'Sub color',
	            'validation' => 'required',
	            'hexinput' => true,
	        ),
	        
	        'client_language_relation' => array
	        (
	            'relation_id' => 'client_language_relation',
	            'type' => 'm_n_relation',
	            'table_mn' => 'client_language',
	            'table_mn_pk' => 'id',
	            'table_mn_col_m' => 'client_id',
	            'table_mn_col_n' => 'language_id',
	            'table_m' => 'client',
	            'table_n' => 'language',
	            'table_n_pk' => 'id',
	            'table_n_value' => 'name',
	            'display_as' => 'Languages',
	            'box_width' => 150,
	            'box_height' => 150,
	            'filter' => false,
	        ),
	        
	        'client_technology_relation' => array
	        (
	            'relation_id' => 'client_technology_relation',
	            'type' => 'm_n_relation',
	            'table_mn' => 'client_technology',
	            'table_mn_pk' => 'id',
	            'table_mn_col_m' => 'client_id',
	            'table_mn_col_n' => 'technology_id',
	            'table_m' => 'client',
	            'table_n' => 'technology',
	            'table_n_pk' => 'id',
	            'table_n_value' => 'name',
	            'display_as' => 'Technologies',
	            'box_width' => 150,
	            'box_height' => 150,
	            'filter' => false,
	        ),

	        'help_content_id' => array(
	            'db_name' => 'help_content_id',
	            'type' => 'combobox',
	            'display_as' => 'Help page',
	            'validation' => 'required',
	            'options' => $helpOptions,
	        ),	        
	        
	        'ga_code' => array(
	            'db_name' => 'ga_code',
	            'type' => 'text',
	            'display_as' => 'Google Analytics ID',
	            'validation' => 'required',
	        ),
	        
	        'default_language' => array(
	            'db_name' => 'default_language',
	            'type' => 'select',
	            'display_as' => 'Default language',
	            'validation' => 'required',
	            'options' => $languageOptions,
	        ),
	        
	        'has_lessons' => array(
	            'db_name' => 'has_lessons',
	            'type' => 'select',
	            'display_as' => 'Has training lessons',
	            'options' => $hasLessonsOptions,
	        ),
        
	         
		));
		
		$data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	public function user()
	{
	    $bc = new besc_crud();
	    $bc->table('user');
	    $bc->primary_key('id');
	    $bc->title('User');
	    
	    $bc->where('is_admin = 0');
	    
	    $bc->order_by_field('username');
	    $bc->order_by_direction('asc');
	    
	    $bc->list_columns(array('username', 'firstname', 'lastname', 'email', 'client_id'));
	    $bc->filter_columns(array('username', 'firstname', 'lastname', 'email', 'client_id'));
	    
	    $clients = array();
	    foreach($this->em->getClients()->result() as $client)
	    {
	        $clients[] = array(
                'key' => $client->id,
	            'value' => $client->name,
	        );
	    }
	    $languageOptions = array();
	    foreach($this->em->getLanguages()->result() as $lang)
	    {
	        $languageOptions[] = array(
	            'key' => $lang->id,
	            'value' => $lang->name,
	        );
	    }
	    
	    $bc->columns(array
	        (
	           
	            
	            'username' => array(
	                'db_name' => 'username',
	                'type' => 'text',
	                'display_as' => 'Username',
	                'validation' => 'required|is_unique[user.username]|max_length[255]',
	            ),
	             
	            'firstname' => array(
	                'db_name' => 'firstname',
	                'type' => 'text',
	                'display_as' => 'Firstname',
	                'validation' => 'max_length[255]',
	            ),
	             
	            'lastname' => array(
	                'db_name' => 'lastname',
	                'type' => 'text',
	                'display_as' => 'Lastname',
	                'validation' => 'max_length[255]',
	            ),
	             
	            'email' => array(
	                'db_name' => 'email',
	                'type' => 'text',
	                'display_as' => 'E-mail',
	                'validation' => 'required|is_unique[user.email]|valid_email|max_length[255]',
	            ),
	            
	            'client_id' => array(
	                'db_name' => 'client_id',
	                'type' => 'combobox',
	                'display_as' => 'Client',
	                'options' => $clients,
	                'validation' => 'required',
	            ),
	            
	            'is_admin' => array(
	                'db_name' => 'is_admin',
	                'type' => 'hidden',
	                'value' => 0,
	            ),
	             
	            'pword' => array(
	                'db_name' => 'pword',
	                'type' => 'hidden',
	                'value' => $this->auth->__createHash('start'),
	            ),
	            
	            'active_lang' => array(
	                'db_name' => 'active_lang',
	                'type' => 'select',
	                'options' => $languageOptions,
	            ),
	             
	        ));
	    
	    $data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	public function admin()
	{
	    $bc = new besc_crud();
	    $bc->table('user');
	    $bc->primary_key('id');
	    $bc->title('Administrator');
	     
	    $bc->where('is_admin = 1');
	     
	    $bc->order_by_field('username');
	    $bc->order_by_direction('asc');
	    
	    $bc->list_columns(array('username', 'firstname', 'lastname', 'email', 'client_id'));
	    $bc->filter_columns(array('username', 'firstname', 'lastname', 'email', 'client_id'));
	     
	    $clients = array();
	    foreach($this->em->getClients()->result() as $client)
	    {
	        $clients[] = array(
	            'key' => $client->id,
	            'value' => $client->name,
	        );
	    }
	     
	    $bc->columns(array
	        (
	            
	            
	            'username' => array(
	                'db_name' => 'username',
	                'type' => 'text',
	                'display_as' => 'Username',
	                'validation' => 'required|is_unique[user.username]|max_length[255]',
	            ),
	            
	            
	
	            'firstname' => array(
	                'db_name' => 'firstname',
	                'type' => 'text',
	                'display_as' => 'Firstname',
	                'validation' => 'max_length[255]',
	            ),
	
	            'lastname' => array(
	                'db_name' => 'lastname',
	                'type' => 'text',
	                'display_as' => 'Lastname',
	                'validation' => 'max_length[255]',
	            ),
	
	            'email' => array(
	                'db_name' => 'email',
	                'type' => 'text',
	                'display_as' => 'E-mail',
	                'validation' => 'required|is_unique[user.email]|valid_email|max_length[255]',
	            ),
	             
	            'client_id' => array(
	                'db_name' => 'client_id',
	                'type' => 'combobox',
	                'display_as' => 'Client',
	                'options' => $clients,
	                'validation' => 'required',
	            ),
	            
	            'is_admin' => array(
	                'db_name' => 'is_admin',
	                'type' => 'hidden',
	                'value' => 1,
	            ),
	            
	            'pword' => array(
	                'db_name' => 'pword',
	                'type' => 'hidden',
	                'value' => $this->auth->__createHash('start'),
	            ),
	             
	            'active_lang' => array(
	                'db_name' => 'active_lang',
	                'type' => 'hidden',
	                'value' => 0,
	            ),

	            
	
	        ));
	     
	    $data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	
	public function edit_menu($clientId, $language = NULL)
	{
	    $data['client'] =  $this->em->getClientById($clientId)->row();
	    $data['languages'] = $this->em->getLanguagesByClientId($clientId);
	    $data['currentLang'] = $language == NULL ? $data['languages']->row()->id : $language;
	    $data['metatags'] = $this->em->getMetatags();
	    $data['menuItems'] = $this->em->getMenuitems($clientId, $data['currentLang']);
	    $data['clients'] = $this->em->getClients();
	    
	    $data['crud_data'] = $this->load->view('backend/edit_menu', $data, true);
	    $this->render->__renderBackend('backend/crud', $data);
	}
	
	public function save_menu()
	{
	    $clientId = $this->input->post('client_id');
	    $languageId = $this->input->post('language_id');
	    
	    $this->em->deleteMenuitems($clientId, $languageId);
	    
	    if($this->input->post('menuitems') != null)
	    {
	        $batch = array();
	        foreach($this->input->post('menuitems') as $menuitem)
	        {
	            $batch[] = array(
                    'client_id' => $clientId,
	                'language_id' => $languageId,
	                'metatag_id' => $menuitem['metatag_id'] == 0 ? null : $menuitem['metatag_id'],
	                'name' => $menuitem['name'],
	                'ordering' => $menuitem['ordering'],
	            );
	        }
	        
	        $this->em->insertMenuitems($batch);
	    }
	    
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );
	}
	
	public function getLanguages()
	{
	    $clientId = $this->input->post('client_id');
	    $lang = array();
	    foreach($this->em->getLanguagesByClientId($clientId)->result() as $l)
	    {
	        $lang[] = array(
                'key' => $l->id,
	            'value' => $l->name,
	        );
	    }
	    echo json_encode(
	        array(
	            'success' => true,
	            'langs' => $lang,
	        )
	    );
	}
	
	public function copyMenu()
	{
	    $clientId = $this->input->post('client_id');
	    $languageId = $this->input->post('language_id');
	    $srcClientId = $this->input->post('src_client_id');
	    $srcLanguageId = $this->input->post('src_language_id');

	    $this->em->deleteMenuitems($clientId, $languageId);
	    
	    $batch = array();
	    foreach($this->em->getMenuitems($srcClientId, $srcLanguageId)->result() as $menuitem)
	    {
	        $batch[] = array(
	            'client_id' => $clientId,
	            'language_id' => $languageId,
	            'metatag_id' => $menuitem->metatag_id,
	            'name' => $menuitem->name,
	            'ordering' => $menuitem->ordering,
	        );
	    }
	    $this->em->insertMenuitems($batch);

	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );
	}
	
	
	public function client_content($clientId)
	{
	    $this->load->model('entities/Content_model', 'cm');
	    $data['content'] =  $this->cm->getContentClient()->result_array();
	    foreach($data['content'] as $key => $contentItem)
	    {
	        $data['content'][$key]['active'] = $this->cm->getContentClientByContent($clientId, $contentItem['id'])->num_rows() > 0;
	    }
	    
	    $data['client'] = $this->em->getClientById($clientId)->row();
	    
	    $data['crud_data'] = $this->load->view('backend/client_content', $data, true);
	    $this->render->__renderBackend('backend/crud', $data);
	}
	
	public function save_client_content()
	{
	    $clientId = $this->input->post('client_id');

	    $this->em->deleteClientContent($clientId);
	     
	    if($this->input->post('contentItems') != null)
	    {
	        $batch = array();
	        foreach($this->input->post('contentItems') as $contentItem)
	        {
	            $batch[] = array(
	                'client_id' => $clientId,
	                'content_id' => $contentItem,
	            );
	        }
	         
	        $this->em->insertClientContent($batch);
	    }
	     
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );	    
	}
	
}
