<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Menu extends Backend 
{
    function __construct()
    {
        parent::__construct();
    }  

	public function metatag()
	{
	    $bc = new besc_crud();
	    $bc->table('metatag');
	    $bc->primary_key('id');
	    $bc->title('Metatag');
	    
	    $bc->list_columns(array('name'));
	    $bc->filter_columns(array('name'));
	
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	             
	        ));
	
	    $data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	public function techs()
	{
	    $bc = new besc_crud();
	    $bc->table('technology');
	    $bc->primary_key('id');
	    $bc->title('Technology');
	    
	    $bc->list_columns(array('name', 'fname'));
	    $bc->filter_columns(array('name'));
	     
	    $bc->columns(array
        (
            'name' => array
            (
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
                'validation' => 'required',
            ),
            
            'fname' => array(
                'db_name' => 'fname',
                'type' => 'image',
                'display_as' => 'Technology logo',
                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>100x100 px',
                'accept' => '.png,.jpg,.jpeg',
                'uploadpath' => 'items/uploads/technology',
                'crop' => array(
                    'ratio' => '100:100',
                    'minWidth' => 100,
                    'minHeight' => 100,
                    'maxWidth' => 300,
                    'maxHeight' => 300,
                ),
            ),
             
        ));
	
	    $data['crud_data'] = $bc->execute();
		$this->render->__renderBackend('backend/crud', $data);
	}
	
	
	
	
}
