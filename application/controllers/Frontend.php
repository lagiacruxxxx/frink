<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller 
{
    public $user = null;
    
    function __construct()
    {
        parent::__construct();
        
        require_once (APPPATH . 'libraries/MyQuiz.php');
        require_once (APPPATH . 'libraries/MyAssessment.php');
        require_once (APPPATH . 'libraries/MyLesson.php');
        
        $this->auth->checkUserLogin();
        
        $this->load->model('Frontend_model', 'fm');
        $this->load->helper('besc_helper');
        
        $this->user = $this->auth->getUser();
        
    }    
    
	public function index()
	{
        $this->render->mosaic();
	}
	
    public function switchLanguage($langId)
    {
        $this->auth->switchLanguage($langId, true);
    }
    
    public function content($contentId)
    {
        $this->render->content($contentId, CONTENT_TYPE_ARTICLE);
    }
    
    public function gallery($contentId)
    {
        $this->render->gallery($contentId);
    }
    
    public function quiz($contentId)
    {
        $quiz = new MyQuiz($contentId, $this->auth->user());
        $quiz->startPage();
    }
    
    public function quiz_start($quizId)
    {
        $quiz = new MyQuiz($quizId, $this->auth->user());
        $quiz->start();
    }
    
    public function quiz_next()
    {
        $quiz = new MyQuiz($this->session->userdata('current_quiz'), $this->auth->user());
        $quiz->next();
    }
    
    public function quiz_answer($answer)
    {
        $quiz = new MyQuiz($this->session->userdata('current_quiz'), $this->auth->user());
        $quiz->answer($answer);
    }
    
    public function assessment($assessmentId)
    {
        $assessment = new MyAssessment($assessmentId, $this->auth->user());
        $assessment->startPage();        
    }
    
    public function assessment_start($assessmentId)
    {
        $assessment = new MyAssessment($assessmentId, $this->auth->user());
        $assessment->start();
    }
    
    public function assessment_next()
    {
        $quiz = new MyAssessment($this->session->userdata('current_assessment'), $this->auth->user());
        $quiz->next();
    }

    public function assessment_answer($answer)
    {
        $quiz = new MyAssessment($this->session->userdata('current_assessment'), $this->auth->user());
        $quiz->answer($answer);
    }    
    
    public function lesson($lessionId)
    {
        $lesson = new MyLesson($lessionId, $this->auth->user());
        $lesson->showPage();
    }
    
    public function lesson_register($lessonId)
    {
        $lesson = new MyLesson($lessonId, $this->auth->user());
        $lesson->register();
    }
    
    public function profile()
    {
        $this->render->profile();
    }
    
    public function lessons()
    {
        $year = $this->input->get('year');
        $month = $this->input->get('month');
        $this->render->lessons($year, $month);
    }
    
    public function search()
    {
        $this->render->search($this->input->get('query'));
    }

}

