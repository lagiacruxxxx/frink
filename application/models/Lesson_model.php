<?php

class Lesson_model extends CI_Model  
{

    
    function getLessonById($lessonId)
    {
        $this->db->where('id', $lessonId);
        return $this->db->get('lesson');
    }
    
    
    function insertLessonRegistration($data)
    {
        $this->db->insert('lesson_registration', $data);
        return $this->db->insert_id();
    }
    
    function getLessonRegistrationById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('lesson_registration');
    }
    
    function alreadyRegistered($lessonId, $userId)
    {
        $this->db->where('lesson_id', $lessonId);
        $this->db->where('user_id', $userId);
        return $this->db->get('lesson_registration');
    }
    
    function getLessonsByDate($date, $clientId)
    {
        $this->db->where('startdate', $date);
        $this->db->where('client_id', $clientId);
        return $this->db->get('lesson');
    }
    
    
    
    function getLessonSuggestions($roleId, $techs)
    {
        
        $this->db->select('lesson.*, 8 as "type"');
        $this->db->from('lesson');
        $this->db->where('visible', 1);
        $this->db->join('lesson_technology', 'lesson_technology.lesson_id = lesson.id AND lesson_technology.technology_id IN(' . $techs . ')');
        $this->db->join('lesson_assessment_role', 'lesson_assessment_role.lesson_id = lesson.id AND lesson_assessment_role.assessment_role_id=' . $roleId);
        return $this->db->get();
    }
    
    function getLessonSuggestions2($clientId, $roleId)
    {
        $this->db->from('lesson');
        $this->db->where('visible', 1);
        $this->db->where('startdate >= CURRENT_DATE()' );
        $this->db->where('client_id', $clientId);
        $this->db->join('lesson_assessment_role', 'lesson_assessment_role.lesson_id = lesson.id AND lesson_assessment_role.assessment_role_id=' . $roleId);
        $dummy = $this->db->get();
        var_dump($this->db->last_query());
        return $dummy;
    }
    
    function getAvailableTrainingLessons($clientId)
    {
        $this->db->where('client_id', $clientId);
        $this->db->get('lesson');
    }
    
    function getLessonsByClient($clientId)
    {
        $this->db->where('client_id', $clientId);
        $this->db->order_by('startdate', 'asc');
        $this->db->order_by('starttime', 'asc');
        return $this->db->get('lesson');
    }

}
