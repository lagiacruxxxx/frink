<?php

class Lang_model extends CI_Model  
{
    function getLangString($langString, $langId)
    {
        $this->db->select($langString);
        $this->db->where('id', $langId);
        return $this->db->get('language');
    }
    
}
