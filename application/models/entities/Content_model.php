<?php

class Content_model extends CI_Model
{
    
    function getContentClient()
    {
        $this->db->where_in('type', array(CONTENT_TYPE_ARTICLE, CONTENT_TYPE_ASSESSMENT, CONTENT_TYPE_CHEATSHEET, CONTENT_TYPE_LESSON, CONTENT_TYPE_QUIZ, CONTENT_TYPE_GALLERY));
        return $this->db->get('content');
    }

    function getContent($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('content');
    }
    
    function getContentClientByContent($clientId, $contentId)
    {
        $this->db->where('client_id', $clientId);
        $this->db->where('content_id', $contentId);
        return $this->db->get('client_content');
    }
    
    function getLesson($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('lesson');
    }
    
    function getLanguagesByContentId($id)
    {
        $this->db->select('language.id, language.name');
        $this->db->where('content_id', $id);
        $this->db->from('content_language');
        $this->db->join('language', 'language.id = content_language.language_id');
        return $this->db->get();
    }
    
    function getLanguagesByLessonId($id)
    {
        $this->db->select('language.id, language.name');
        $this->db->where('lesson_id', $id);
        $this->db->from('lesson_language');
        $this->db->join('language', 'language.id = lesson_language.language_id');
        return $this->db->get();
    }
    
    function getLanguage()
    {
        return $this->db->get('language');
    }
    
    function getUsers()
    {
        return $this->db->get('user');
    }
    
    function getClients()
    {
        return $this->db->get('client');
    }
    
    function getTechnology()
    {
        return $this->db->get('technology');    
    }
    
    
    
    function getTeaserByContentId($contentId, $langId)
    {
        $this->db->where('content_id', $contentId);
        $this->db->where('lang_id', $langId);
        return $this->db->get('content_teaser');
    }
    
    function deleteTeaser($contentId, $langId)
    {
        $this->db->where('content_id', $contentId);
        $this->db->where('lang_id', $langId);
        $this->db->delete('content_teaser');
    }
    
    function insertTeaser($batch)
    {
        $this->db->insert_batch('content_teaser', $batch);
    }
    
    
    function getMetatagById($metatagId)
    {
        $this->db->where('id', $metatagId);
        return $this->db->get('metatag');
    }
    
    
    function getQuestionById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('quiz_question');
    }
    
    function getAssessmentQuestionById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('assessment_question');
    }
    
    function getAssessmentRoles()
    {
        return $this->db->get('assessment_role');
    }
    
    function getAssessmentRoleById($id)
    {
        $this->db->where('id', $id);        
        return $this->db->get('assessment_role');
    }
    
    function getAssessmentRoleLangByRoleId($roleId, $langId)
    {
        $this->db->where('role_id', $roleId);
        $this->db->where('language_id', $langId);
        return $this->db->get('assessment_role_language');
    }
    
    function deleteAssessmentRoleLang($roleId, $langId)
    {
        $this->db->where('role_id', $roleId);
        $this->db->where('language_id', $langId);
        $this->db->delete('assessment_role_language');
    }
    
    function insertAssessmentRoleLang($batch)
    {
        $this->db->insert_batch('assessment_role_language', $batch);
    }
    
    function getLessonRegistrationStats($lessonId)
    {
        $this->db->select('lessonname as "Lessonname", clientname as "Client", firstname as "Firstname", lastname as "Lastname", username as "Username", email as "E-mail"');
        $this->db->where('lesson_id', $lessonId);
        return $this->db->get('lesson_registration_stats');
    }
    
    
    function getQuizStats()
    {
        $this->db->select('quizname as "Quizname", created_date as "Timestamp", percent as "Percent", points as "Points", clientname as "Client", username as "Username", lastname as "Lastname", firstname as "Firstname"');
        $this->db->order_by('created_date', 'desc');
        return $this->db->get('quiz_stats');
    }
    
    function getAssessmentStats()
    {
        $this->db->select('assessment_name as "Assessment", created_date as "Timestamp", role_result_name as "Result", clientname as "Client", username as "Username", lastname as "Lastname", firstname as "Firstname"');
        $this->db->order_by('created_date', 'desc');
        return $this->db->get('assessment_stats');
    }
    
    
    function deleteModulesText($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_text');
    }
    
    function insertModuleText($data)
    {
        $this->db->insert('module_text', $data);
    }
    
    function getModulesText($itemId, $langId, $pageType)
    {
        $this->db->select('module_text.*, "text" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_text');
    }
    
    function deleteModulesImage($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_image');
    }
    
    function insertModuleImage($data)
    {
        $this->db->insert('module_image', $data);
    }
    
    function getModulesImage($itemId, $langId, $pageType)
    {
        $this->db->select('module_image.*, "image" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_image');
    }
    
    function deleteModulesMultipleChoice4($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_multiplechoice4');
    }
    
    function insertModuleMultipleChoice4($data)
    {
        $this->db->insert('module_multiplechoice4', $data);
    }
    
    function getModulesMultipleChoice4($itemId, $langId, $pageType)
    {
        $this->db->select('module_multiplechoice4.*, "multiplechoice4" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_multiplechoice4');
    }
    
    function deleteModulesVideo($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_video');
    }
    
    function insertModuleVideo($data)
    {
        $this->db->insert('module_video', $data);
    }
    
    function getModulesVideo($itemId, $langId, $pageType)
    {
        $this->db->select('module_video.*, "video" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_video');
    }
    
    
    function deleteModulesAccordionStart($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_accordion_start');
    }
    
    function insertModuleAccordionStart($data)
    {
        $this->db->insert('module_accordion_start', $data);
    }
    
    function getModulesAccordionStart($itemId, $langId, $pageType)
    {
        $this->db->select('module_accordion_start.*, "accordion_start" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_accordion_start');
    }
    
    
    
    function deleteModulesAccordionEnd($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_accordion_end');
    }
    
    function insertModuleAccordionEnd($data)
    {
        $this->db->insert('module_accordion_end', $data);
    }
    
    function getModulesAccordionEnd($itemId, $langId, $pageType)
    {
        $this->db->select('module_accordion_end.*, "accordion_end" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_accordion_end');
    }
    
    
    function deleteModulesSequence($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_sequence');
    }
    
    function insertModuleSequence($data)
    {
        $this->db->insert('module_sequence', $data);
        return $this->db->insert_id();
    }
    
    function getModulesSequence($itemId, $langId, $pageType)
    {
        $this->db->select('module_sequence.*, "sequence" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_sequence');
    }
    
    function insertModuleSequenceItem($data)
    {
        $this->db->insert_batch('module_sequence_item', $data);
    }
    
    function getModulesSequenceItem($id)
    {
        $this->db->where('sequence_id', $id);
        $this->db->order_by('ordering', 'asc');
        return $this->db->get('module_sequence_item');
    }
    
    
    function deleteModulesAssessment($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_assessment');
    }
    
    function insertModuleAssessment($data)
    {
        $this->db->insert('module_assessment', $data);
    }
    
    function getModulesAssessment($itemId, $langId, $pageType)
    {
        $this->db->select('module_assessment.*, "assessment" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_assessment');
    }
    
    
    
    function deleteModulesGallery($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_gallery');
    }
    
    function insertModuleGallery($data)
    {
        $this->db->insert('module_gallery', $data);
        return $this->db->insert_id();
    }
    
    function getModulesGallery($itemId, $langId, $pageType)
    {
        $this->db->select('module_gallery.*, "gallery" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_gallery');
    }
    
    function insertModuleGalleryItem($data)
    {
        $this->db->insert_batch('module_gallery_item', $data);
    }
    
    function getModulesGalleryItem($id)
    {
        $this->db->where('gallery_id', $id);
        $this->db->order_by('ordering', 'asc');
        return $this->db->get('module_gallery_item');
    }
    
    
    function deleteModulesVimeo($itemId, $langId, $pageType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        $this->db->delete('module_vimeo');
    }
    
    function insertModuleVimeo($data)
    {
        $this->db->insert('module_vimeo', $data);
    }
    
    function getModulesVimeo($itemId, $langId, $pageType)
    {
        $this->db->select('module_vimeo.*, "vimeo" as "mod"');
        $this->db->where('item_id', $itemId);
        $this->db->where('language_id', $langId);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_vimeo');
    }
    

    
    function clone_content($data)
    {
        $this->db->insert('content', $data);
        return $this->db->insert_id();
    }
    
    function clone_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }
    
    function getMetatagByContentId($contentId)
    {
        $this->db->where('content_id', $contentId);
        return $this->db->get('content_metatag');
    }
    
    function getTechnologyByContentId($contentId)
    {
        $this->db->where('content_id', $contentId);
        return $this->db->get('content_technology');
    }
    
    function getTeaserByContentIdClone($contentId)
    {
        $this->db->where('content_id', $contentId);
        return $this->db->get('content_teaser');
    }
    
    function getClientsByContentId($contentId)
    {
        $this->db->where('content_id', $contentId);
        return $this->db->get('client_content');
    }
        
    function getModuleAccordionStart($contentId)
    {
        $this->db->where('item_id', $contentId);
        return $this->db->get('module_accordion_start');
    }
    
    function getModuleAccordionEnd($contentId)
    {
        $this->db->where('item_id', $contentId);
        return $this->db->get('module_accordion_end');
    }

    function getModuleText($contentId)
    {
        $this->db->where('item_id', $contentId);
        return $this->db->get('module_text');
    }

    function getModuleImage($contentId)
    {
        $this->db->where('item_id', $contentId);
        return $this->db->get('module_image');
    }    
    
    function getModuleVideo($contentId)
    {
        $this->db->where('item_id', $contentId);
        return $this->db->get('module_video');
    }

    function getModuleVimeo($contentId)
    {
        $this->db->where('item_id', $contentId);
        return $this->db->get('module_vimeo');
    }    
    
    function getModuleGallery($contentId)
    {
        $this->db->where('item_id', $contentId);
        return $this->db->get('module_gallery');
    }
    
    function getModuleGalleryItem($galleryId)
    {
        $this->db->where('gallery_id', $galleryId);
        return $this->db->get('module_gallery_item');
    }
    
    function clone_gallery($data)
    {
        $this->db->insert('module_gallery', $data);
        return $this->db->insert_id();
    }
    
    function getModuleSequence($contentId)
    {
        $this->db->where('item_id', $contentId);
        return $this->db->get('module_sequence');
    }
    
    function getModuleSequenceItem($galleryId)
    {
        $this->db->where('sequence_id', $galleryId);
        return $this->db->get('module_sequence_item');
    }
    
    function clone_sequence($data)
    {
        $this->db->insert('module_sequence', $data);
        return $this->db->insert_id();
    }    
}

?>