<?php

class Quiz_model extends CI_Model  
{

    function createQuizResult($data)
    {
        $this->db->insert('quiz_result', $data);
        return $this->db->insert_id();
    }
    
    function insertQuizResultQuestion($data)
    {
        $this->db->insert('quiz_result_question', $data);
    }
    
    function insertQuizResultQuestions($data)
    {
        $this->db->insert_batch('quiz_result_question', $data);
    }
    
    function getMultipleChoice4Module($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('module_multiplechoice4');
    }
    
    function getQuizResultById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('quiz_result');
    }
    
    function getQuizResultQuestions($resultId)
    {
        $this->db->where('quiz_result_id', $resultId);
        return $this->db->get('quiz_result_question');
    }
    
    
    
    
    function getQuestionsByQuizId($id)
    {
        $this->db->where('content_id', $id);
        $this->db->order_by('order', 'asc');
        $this->db->order_by('id', 'asc');
        return $this->db->get('quiz_question');
    }
    
    function getQuestionById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('quiz_question');
    }
    
    
    function getQuizResultsByUserId($userId)
    {
        $this->db->select('quiz_result.*, content.name as quizname, content.teaser_fname');
        $this->db->where('user_id', $userId);
        $this->db->join('content', 'content.id = quiz_result.content_id');
        $this->db->order_by('quiz_result.created_date', 'desc');
        return $this->db->get('quiz_result');
    }
    
    
    function getQuizQueryResults($clients, $lang, $startdate, $enddate)
    {
        if($clients != '')
            $this->db->where_in('client_id', $clients);
        if($lang != '')
            $this->db->where_in('lang_id', $lang);
        
        $this->db->where("created_date BETWEEN '" . date('Y-m-d 00:00:00', strtotime($startdate)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($enddate)) . "'");
        return $this->db->get('quiz_stats');
    }
    

}
