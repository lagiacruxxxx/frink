<?php

class Frontend_model extends CI_Model  
{

    function getClientById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('client');
    }
    
    function getUserById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('user');
    }
    
    function getGalleryItemsByContentId($contentId)
    {
        $this->db->where('content_id', $contentId);
        return $this->db->get('gallery_item');
    }
    
    function getLanguagesByClientId($id)
    {
        $this->db->select('language.id, language.name, language.short');
        $this->db->where('client_id', $id);
        $this->db->from('client_language');
        $this->db->join('language', 'language.id = client_language.language_id');
        return $this->db->get();
    }    
    
    function getLanguageById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('language');
    }
    
    function updateUserById($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }
    
    function getTechsByClientId($id)
    {
        $this->db->select('technology.*');
        $this->db->where('client_id', $id);
        $this->db->from('client_technology');
        $this->db->join('technology', 'technology.id = client_technology.technology_id');
        return $this->db->get();
    }
    
    function isContentAvailableForClient($clientId, $contentId)
    {
        $this->db->where('client_id', $clientId);
        $this->db->where('content_id', $contentId);
        return $this->db->get('client_content');
    }
    
    function getMenuitemsByClientId($clientId, $langId)
    {
        $this->db->where('client_id', $clientId);
        $this->db->where('language_id', $langId);
        $this->db->order_by('ordering', 'asc');
        return $this->db->get('menuitem');
    }
    
    function getLessonsByTech($techs)
    {
        $this->db->select('lesson.*, 8 as "type"');
        $this->db->from('lesson');
        $this->db->where('visible', 1);
        $this->db->join('lesson_technology', 'lesson_technology.lesson_id = lesson.id AND lesson_technology.technology_id IN(' . $techs . ')');
        return $this->db->get();
    }
    
    function getContentByTech($techs)
    {
        $this->db->select('content.*');
        $this->db->from('content');
        $this->db->where('visible', 1);
        $this->db->join('content_technology', 'content_technology.content_id = content.id AND content_technology.technology_id IN(' . $techs . ')');
        $this->db->order_by('content.priority', 'desc');
        return $this->db->get();
    }    
    
    function getMetatagsByContent($contentId)
    {
        $this->db->where('content_id', $contentId);
        return $this->db->get('content_metatag');
    }
    
    function getTechsByContent($contentId)
    {
        $this->db->where('content_id', $contentId);
        return $this->db->get('content_technology');
    }
    
    function getTeaserByContent($contentId, $languageId)
    {
        $this->db->where('content_id', $contentId);
        $this->db->where('lang_id', $languageId);
        return $this->db->get('content_teaser');
    }
    
    function getMetatagsByLesson($contentId)
    {
        $this->db->where('lesson_id', $contentId);
        return $this->db->get('lesson_metatag');
    }
    
    function getTechsByLesson($contentId)
    {
        $this->db->where('lesson_id', $contentId);
        return $this->db->get('lesson_technology');
    }    
    
    function getContentById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('content');
    }
    
    
    
    function createQuizResult($data)
    {
        $this->db->insert('quiz_result', $data);
        return $this->db->insert_id();
    }
    
    
    function getSearchKeywords()
    {
        return $this->db->get('search_keywords');
    }
    
    function getTechs()
    {
        return $this->db->get('technology');
    }
    
}
