<?php

class Assessment_model extends CI_Model  
{

    function createAssessmentResult($data)
    {
        $this->db->insert('assessment_result', $data);
        return $this->db->insert_id();
    }
    
    function getQuestionsByAssessmentId($id)
    {
        $this->db->where('content_id', $id);
        $this->db->order_by('order', 'asc');
        $this->db->order_by('id', 'asc');
        return $this->db->get('assessment_question');
    }

    function getAssessmentModule($questionId, $language, $pageType)
    {
        $this->db->where('item_id', $questionId);
        $this->db->where('language_id', $language);
        $this->db->where('pageType', $pageType);
        return $this->db->get('module_assessment');
    }
    
    function getAssessmentRoles()
    {
        return $this->db->get('assessment_role');
    }
    
    function getAssessmentRoleById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('assessment_role');
    }
    
    function saveAssessmentResult($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('assessment_result', $data);
    }
    
    
    function getAssessmentResultById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('assessment_result');
    }
    
    function getAssessmentResultByUser($userId)
    {
        $this->db->where('user_id', $userId);
        $this->db->where('role_result !=', 0);
        return $this->db->get('assessment_result');
    }
    
    function getAssessmentById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('content');    
    }
    
    
    function getAssessmentQueryResults($clients, $lang, $startdate, $enddate)
    {
        if($clients != '')
            $this->db->where_in('client_id', $clients);
        if($lang != '')
            $this->db->where_in('lang_id', $lang);
        
        $this->db->where("created_date BETWEEN '" . date('Y-m-d 00:00:00', strtotime($startdate)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($enddate)) . "'");
        return $this->db->get('assessment_stats');
    }    
    
    function getAssessmentRoleLangByRoleId($roleId, $langId)
    {
        $this->db->where('role_id', $roleId);
        $this->db->where('language_id', $langId);
        return $this->db->get('assessment_role_language');
    }
    
}
