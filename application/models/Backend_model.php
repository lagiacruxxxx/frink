<?php

class Backend_model extends CI_Model
{
    function getMetatagCategories()
    {
        $this->db->where('active', 1);
        return $this->db->get('metatag_category');
    }
    
    function getMetatagByCategory($categoryId)
    {
        $this->db->where('metatag_category_id', $categoryId);
        $this->db->order_by('name');
        return $this->db->get('metatag');
        
    }
    
    function getItemsByMetatag($metatagId)
    {
        $this->db->select('item.id, item.name, item.detail_img');
        $this->db->from('metatag_item');
        $this->db->where('metatag_id', $metatagId);
        $this->db->join('item', 'item.id = metatag_item.item_id');
        $this->db->order_by('item.name', 'asc');
        return $this->db->get();
    }
}

?>