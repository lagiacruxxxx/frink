var progress_status = 0;
var progress_interval = null;

var a = 0;
var delay = (1000 / 30);
var now, before = new Date();var now, before = new Date();

$(document).ready(function()
{
	//igniteProgressbar();
	igniteAnswerListeners();
});


function igniteProgressbar()
{
	$( "#progressbar" ).progressbar(
	{
		value: 100,
		max: 100,
	});
	
	progress_interval = setInterval(function()
	{
		now = new Date();
	    var elapsedTime = (now.getTime() - before.getTime());
	    
	    progress_status += elapsedTime;
    	
		$( "#progressbar" ).progressbar({value: 100 - Math.round((progress_status / ($( "#progressbar" ).attr('limit') * 1000)) * 100)});
		
		if(progress_status >= $( "#progressbar" ).attr('limit') * 1000)
		{
			clearInterval(progress_interval);
			answerQuestion(null, 'quiz');
		}
			
		before = new Date();  
		
	}, 250);
	
}


function igniteAnswerListeners()
{
	$('.module_multiplechoice4 .module_answer').on('click', function()
	{
		answerQuestion($(this), 'quiz');
	});
	
	$('.module_assessment .module_answer').on('click', function()
	{
		answerQuestion($(this), 'assessment');
	});
	
	/*$('.question_block_bottom.right').on('click', function()
	{
		answerQuestion(null, $(this).attr('type'));
	});*/
}


function answerQuestion(mod, type)
{
	if(mod != null)
		var answer = mod.attr('answer');
	else
		var answer = 5;
	
	$('.module_multiplechoice4 .module_answer').off('click');
	$('.module_assessment .module_answer').off('click');
	
	$.ajax(
	{
		url: rootUrl + 'Frontend/' + type + '_answer/' + answer,
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				
				if(type == 'quiz')
				{
					if(ret.correct)
						mod.addClass('module_answer_correct');
					else
					{
						mod.addClass('module_answer_false');
						mod.parent().find('.module_answer[answer="' + ret.correctAnswer + '"]').addClass('module_answer_correct')
					}
					
					mod.removeClass('animate');
					$('.module_answer').off('mouseup');
					
					setTimeout(function()
					{
						window.location.reload();
					}, 2000);
				}					
				else
					window.location.reload();
			}
			else
				alert('error with answer');
		},
	});
}

