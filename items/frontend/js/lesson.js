
$(document).ready(function()
{
	igniteLessonListeners();
});


function igniteLessonListeners()
{
	$('.lesson_register').on('click', function()
	{
		registerLesson($(this));
	});
	
	$('.lesson_header_toggle').on('click', function()
	{
		toggleLesson($(this).parent().parent());
	});
	
	$('.lessongroup_toggle span').on('click', function()
	{
		toggleAllLessons($(this).parent().parent());
	});
}


function toggleLesson(lesson)
{
	if(lesson.hasClass('lesson_open'))
	{
		lesson.removeClass('lesson_open');
		lesson.find('.lesson_content').hide();
		//lesson.find('.lesson_header_toggle').css({'background-image': 'url("../items/frontend/img/acc_arrow_right.png")'});
	}
	else
	{
		lesson.addClass('lesson_open');
		lesson.find('.lesson_content').show();
		//lesson.find('.lesson_header_toggle').css({'background-image': 'url("../items/frontend/img/acc_arrow_down.png")'});		
	}
}


function registerLesson(btn)
{
	btn.hide();
	btn.parent().find('.lesson_register_loading').show();
	$.ajax(
	{
		url: rootUrl + 'Frontend/lesson_register/' + btn.attr('lesson'),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			btn.parent().find('.lesson_register_loading').hide();
			
			if(ret.success)
			{
				btn.parent().find('.lesson_register_success').show();
			}
			else
				alert('error with registration');
		},
	});
}


function toggleAllLessons(lessongroup)
{
	if(lessongroup.find('.lesson.lesson_open').length == lessongroup.find('.lesson').length)
	{
		lessongroup.find('.lesson').each(function()
		{
			toggleLesson($(this));
		});
	}
	else
	{
		lessongroup.find('.lesson').each(function()
		{
			$(this).removeClass('lesson_open');
			toggleLesson($(this));
		});
	}
}
