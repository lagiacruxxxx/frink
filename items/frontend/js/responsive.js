var wWidth;
var wHeight;
var content_container_width;

$(document).ready(function()
{	
	resize();
	
	$(window).resize(function()
	{
		resize();
	});
});

function resize()
{
	wWidth = $(document).width();
	wHeight = $(document).height();
	
	$('#header').css({'width': wWidth, 'left': 0});
	$('#sidemenu').css({'height': wHeight - $('#header').outerHeight(), 'top': $('#header').outerHeight()});
	$('#content').css({'width': wWidth - $('#sidemenu').outerWidth(), 'height': wHeight - $('#header').outerHeight() - $('#techheader').outerHeight() - $('#footer').outerHeight(), 'left': $('#sidemenu').outerWidth(), 'top': $('#header').outerHeight() + $('#techheader').outerHeight()});
	$('#techheader').css({'width': wWidth - $('#sidemenu').outerWidth() - 30, 'left': $('#sidemenu').outerWidth(), 'top': $('#header').outerHeight()});
	$('#footer').css({'width': wWidth, 'left': 0});
	
	fixMosaic();
}



function fixMosaic()
{
	var content = $('#content');
	var perline = Math.floor(content.width() / 214);
	var margin = Math.floor((content.width() % 214) / 2);
	$('#mosaic').css({'width': perline * 214, 'margin-left': 0, 'margin-right': margin});
}