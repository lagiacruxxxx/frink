var elasticsearch = null;
var es_client = null;

$(document).ready(function()
{	
	igniteES();
	es_search();
});

function igniteES()
{
	es_client = new $.es.Client(
	{
		host: [{
		       host: '75f7785602d7d1f584ec9016ec2452d5.us-east-1.aws.found.io',
		       port: '9243',
		       protocol: 'https',
		       auth: 'academy:3lastic4academy&',
		}],
		log: 'trace',
	});	
}


function es_search()
{
	es_client.search(
	{
		index: 'academy-article',
		size: 100,
		body:
		{
			"query": 
			{
				"multi_match": 
			    {
					"query": es_query,
					"fields": 
					[
					 	"title",
					 	"content"
					]
			    }
			},
			"filter": 
			{
				"bool": 
				{
					"must": 
					[
					 	{
					 		"match": 
					 		{
				 				"language.languageCode": es_activeLang
					 		}
					 	},
					 	{
					 		"nested": 
					 		{
					 			"path": "clients",
					 			"filter": 
					 			{
					 				"match": 
					 				{
					 					"clients.id": es_client_id
					 				}
					 			}
					 		}
					 	}
				 	]
			    }
			},
			"highlight": 
			{
			    "pre_tags": ["<em class=\"highlight\" >"],
			    "post_tags": ["</em class=\"highlight\" >"],
			    "fields": 
			    {
			    	"title": {},
			    	"content": {}
			    }
			}
		}
	}, 
	function (error, response) 
	{
		console.log(response);
		$('#searchheader_searchtotal').empty().html(response.hits.total);
		
		for(var i = 0 ; i < response.hits.hits.length ; i++)
		{
			var resultclone = $('.searchresult.is_dummy').clone();
			resultclone.removeClass('is_dummy');
			resultclone.find('.searchresult_image img').attr('src', 'items/uploads/content/' + response.hits.hits[i]._source.teaserImage);
			resultclone.find('.searchresult_title span').text(response.hits.hits[i]._source.title);
			resultclone.find('.searchresult_title a').attr('href', rootUrl + 'content/' + response.hits.hits[i]._source.contentId);
			var highlight = '';
			if(response.hits.hits[i].highlight.title !== undefined)
			{
				for(var j = 0 ; j < response.hits.hits[i].highlight.title.length ; j++)
				{
					highlight += response.hits.hits[i].highlight.title[j];
				}
			}
			if(response.hits.hits[i].highlight.content !== undefined)
			{
				for(var j = 0 ; j < response.hits.hits[i].highlight.content.length ; j++)
				{
					highlight += response.hits.hits[i].highlight.content[j];
				}
			}
			resultclone.find('.searchresult_highlight').html(strip_tags(highlight, '<em> </em>'));
			
			var techs = '';
			for(var j = 0 ; j < response.hits.hits[i]._source.technologies.length ; j++)
			{
				techs += '<div class="searchresult_techs_item">';
				techs += '<div class="searchresult_techs_item_image"><img src="items/uploads/technology/' + es_techs[response.hits.hits[i]._source.technologies[j].id][1] +'" /></div>';
				techs += '<div class="searchresult_techs_item_name">' + es_techs[response.hits.hits[i]._source.technologies[j].id][0] + '</div>';
				techs += '</div>';
			}
			resultclone.find('.searchresult_techs').html(techs);
			
			$('#searchresults').append(resultclone);
		}
	});
}


function strip_tags (input, allowed) {
    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}