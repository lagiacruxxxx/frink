var sidemenu_active = null;
var techmenu_active = null;


/***********************************
 * DEVICE SPECIFIC
 ************************************/



$(document).ready(function()
{	
	toggleHeaderListeners(true);
	toggleMenuFilterListeners(true);
	igniteMosaic();
	toggleModuleListeners(true);
	lightBoxOptions();
	fixModuleStyle();
	igniteSearchbar();
});


function igniteSearchbar()
{
	var source = [];
	$('.searchbar_keywords').each(function()
	{
		source.push($(this).html());
	});
	$('#searchbar input, #searchheader_input input').autocomplete(
	{
		source: source,
	});
}


function fixModuleStyle()
{
	$('.module_accordion_start, .module_accordion_content').each(function()
	{
		var sub = 2;
		if($(this).hasClass('module_accordion_content'))
			sub += 10;
			
		$(this).width(parseInt($(this).parent().width())-sub);
		
	});
	
	/*$('.module_accordion_content').each(function()
	{
		$(this).width(parseInt($(this).parent().width()) -2 - 10);
	});*/
			
	setTimeout(function()
	{
		$('.module_sequence').each(function()
		{
			if($(this).hasClass('leftright'))
			{
				var height = 0;
				$(this).find('.module_sequence_item').each(function()
				{
					if(height <= $(this).find('.module_sequence_desc').outerHeight())
						height = $(this).find('.module_sequence_desc').outerHeight();
				});
				
				$(this).find('.module_sequence_item').height(height);
			}
			else
			{
				var height = 0;
				$(this).find('.module_sequence_item').each(function()
				{
					if(height <= $(this).find('.module_sequence_desc').outerHeight())
						height = $(this).find('.module_sequence_desc').outerHeight();
				});
				
				$(this).find('.module_sequence_item').height(height + 148);
			}
		});
	}, 500);
}

function lightBoxOptions()
{
	$('.module_sequence_img, .module_sequence_enlarge').on('click', function()
	{
		var item = $(this).parent().hasClass('module_sequence_item') ? $(this).parent() : $(this).parent().parent();
		var sequence = item.parent();
		
		var images = [];
		var i = 0;
		var j = 0;
		sequence.find('.module_sequence_item').each(function()
		{
			if(item.find('.module_sequence_ordering').html() == $(this).find('.module_sequence_ordering').html())
				j = i;
			var label = '<p class="module_sequence_ordering" style="margin-right: 5px; font-size: 20px;margin-bottom: 4px; font-weight: bold;">' + $(this).find('.module_sequence_ordering').text() + '</p><p style="font-size: 14px; color: #5a5952; text-transform: uppercase; margin-bottom: 4px;">' + $(this).find('.module_sequence_headline').text() + '</p>' + $(this).find('.module_sequence_text').html();
			var img = $(this).find('.module_sequence_img img').attr('src').replace(/ /g, '%20');
			images[i] = [img, label];
			i++;
		});
		
		$.slimbox
		(
			images,
			j,
			{
				'captionAnimationDuration': 1,
				'counterText': '',
				'imageFadeDuration': 1,
				'overlayFadeDuration': 1,
				'resizeDuration': 200,
			}
			
		);
	});
}


function toggleHeaderListeners(toggle)
{
	if(toggle)
	{
		$('#langswitch span').on('click', function()
		{
			switchLanguage($(this).attr('lang_id'));
		});
		
		$('#searchbar input, #searchheader_input input').on('keyup', function(e)
		{
		    if(e.keyCode == 13)
		    {
				search($(this).val());
		    }
		});
		
	}
	else
	{
		$('#langswitch span').off('click');
		$('#searchbar input').off('keyup');
	}
}

function igniteMosaic()
{
	sidemenu_active = $('.sidemenu_item_active').attr('metatag_id') !== undefined ? $('.sidemenu_item_active').attr('metatag_id') : null;
	techmenu_active = $('.techmenu_item_active').attr('tech_id') !== undefined ? $('.techmenu_item_active').attr('tech_id') : null;
	
	var maxheight = 0;
	$('.content').find('.content_text').each(function()
	{
		maxheight = $(this).height() > maxheight ? $(this).height() : maxheight; 
	});
	//$('.content').find('.content_text').height(maxheight);
	
	if($('#mosaic').length > 0)
	{
		$('#mosaic').isotope(
		{
			'itemSelector': '.content',
			'masonry':
			{
				'columnWidth': 216,
				'gutter': 15,
			}
		});
		
		filterMosaic();
	}
}

function toggleMenuFilterListeners(toggle)
{
	if(toggle)
	{
		$('.sidemenu_item, .sidemenu_headline').on('click', function()
		{
			if($(this).attr('metatag_id') !== undefined)
			{
				sidemenu_active = parseInt($(this).attr('metatag_id')) == 0 ? null : $(this).attr('metatag_id'); 
				
				$('.sidemenu_item').removeClass('sidemenu_item_active');
				$('.sidemenu_headline').removeClass('sidemenu_item_active');
				docCookies.removeItem('frink_academy_tagfilter', null, null);
				if(sidemenu_active != null)
				{
					$(this).addClass('sidemenu_item_active');
					docCookies.setItem('frink_academy_tagfilter', sidemenu_active, Infinity, '/', null , false);
				}
				else
				{
					docCookies.removeItem('frink_academy_tagfilter', '/', null);
				}
				
				if($('#mosaic').length <= 0)
				{
					window.location.href = rootUrl;
				}
				else
					filterMosaic();
			}
		});
		
		$('.tech').on('click', function()
		{
			techmenu_active = $(this).attr('tech_id') == 0 ? null : $(this).attr('tech_id');
			$('.tech').removeClass('techmenu_item_active');
			docCookies.removeItem('frink_academy_techfilter', null, null);
			if(techmenu_active != null)
			{
				$(this).addClass('techmenu_item_active');
				docCookies.setItem('frink_academy_techfilter', techmenu_active, Infinity, '/', null , false);
			}
			else
			{
				docCookies.removeItem('frink_academy_techfilter', '/', null);
			}
			
			if($('#mosaic').length <= 0)
				window.location.href = rootUrl;
			else
				filterMosaic();
		});
	}
	else
	{
		$('.sidemenu_item, .sidemenu_headline').off('click');
		$('.tech').off('click');
	}
}


function filterMosaic()
{
	$('#mosaic').isotope(
	{
		filter: function()
		{
			var sidebartest = false;
			if(sidemenu_active == null)
				sidebartest = true;
			else
				{
					var metatags = $(this).attr('metatags').split(',');
					sidebartest = $.inArray(sidemenu_active, metatags) >= 0;
				}
			
			var techtest = false;
			if(techmenu_active == null)
				techtest = true;
			else
				{
					var techs = $(this).attr('techs').split(',');
					techtest = $.inArray(techmenu_active, techs) >= 0;
				}
			
			return sidebartest && techtest;
		},
	});
}


function toggleModuleListeners(toggle)
{
	if(toggle)
	{
		$('.module_accordion_arrow').on('click', function()
		{
			toggleAccordion($(this).parent());
		});
	}
	else
	{
		$('.module_accordion_arrow').off('click');
	}
}


function toggleAccordion(accordion)
{
	var acc_content = $('.module_accordion_content[module_id="' + accordion.attr('module_id') + '"]');
	if(acc_content.height() == 0)
	{
		//accordion.find('.module_accordion_arrow').css({'width': '19px', 'height': '12px', 'margin-top': '19px', 'margin-right': '8px'});
		accordion.find('.module_accordion_headline').css({'border-bottom': 'solid 1px #b4b4b4'});
		acc_content.css({'height': 'auto', 'padding': '5px', 'border': '1px solid #b4b4b4', 'padding-top': '15px'});
		accordion.addClass('module_Accordion_start_open');
	}
	else
	{
		//accordion.find('.module_accordion_arrow').css({'width': '12px', 'height': '19px', 'margin-top': '15px', 'margin-right': '15px'});
		accordion.find('.module_accordion_headline').css({'border-bottom': '0px'});
		acc_content.css({'height': 0, 'padding': '0px 5px', 'border': '0px'});
		accordion.removeClass('module_Accordion_start_open');
	}
}


function switchLanguage(langId)
{
	$.ajax(
	{
		url: rootUrl + 'Frontend/switchLanguage/' + langId,
		method: 'POST',
		success: function(data)
		{
			location.reload();
		},
	});
}


function triggerGATracker(category, value)
{
	ga('send', 'event', category, 'click', value);
}


function search(query)
{
	window.location.href = rootUrl + 'search?query=' + query;
}