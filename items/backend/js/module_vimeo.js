

function new_module_vimeo(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'vimeo',
		code: '11712103',
		start: 0,
		id: id,
		parent: parent,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_vimeo"><div class="module_vimeo_overlay"></div>';
			html += "<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/11712103' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>"
			html += '</div>';
			return html;
		},
		
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);
				
				checkMoveButtons(active_module);
				
				$('#popup_module_vimeo').find('.popup_cancel_button').unbind('click');
				$('#popup_module_vimeo').find('.popup_save_button').unbind('click');
				$('#popup_module_vimeo').find('.popup_delete_button').unbind('click');
				$('#popup_module_vimeo').find('.popup_moveup_button').unbind('click');
				$('#popup_module_vimeo').find('.popup_movedown_button').unbind('click');

				$('#popup_module_vimeo').find('#vimeo_code_input').val(modules[active_module.attr('module_id')].code);
				$('#popup_module_vimeo').show();
				
				$('#popup_module_vimeo').find('.popup_save_button').click(function()
				{
					modules[active_module.attr('module_id')].code =  $('#popup_module_vimeo').find('#vimeo_code_input').val();
					active_module.find('iframe').attr('src', 'https://player.vimeo.com/video/' + modules[active_module.attr('module_id')].code);
					$('#popup_module_vimeo').hide();
				});
				
				$('#popup_module_vimeo').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_vimeo').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
				
				$('#popup_module_vimeo').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_vimeo').hide();
				});
				
				$('#popup_module_vimeo').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_vimeo').hide();
				});
				
			});
		},
		
		unbindListeners: function()
		{
			this.module.unbind('dblclick');
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'code': this.code,
				'order': this.id,
				'type': 'vimeo',
			};
			
			return ret;
		},
		
	}
	
	return new_elem;
}