

function new_module_assessment(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'assessment',
		id: id,
		parent: parent,
		question: '',
		answer1: '',
		answer1_role: 0,
		answer2: '',
		answer2_role: 0,
		answer3: '',
		answer3_role: 0,
		answer4: '',
		answer4_role: 0,
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_multiplechoice4">';
			html += '<div class="module_question">Question ...</div>';
			html += '<div class="module_answer" answer=1 role>Answer 1 ...</div>';
			html += '<div class="module_answer" answer=2 >Answer 2 ...</div>';
			html += '<div class="module_answer" answer=3 >Answer 3 ...</div>';
			html += '<div class="module_answer" answer=4 >Answer 4 ...</div>';
			html += '</div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);

				checkMoveButtons(active_module);

				$('#popup_module_assessment').find('.popup_cancel_button').unbind('click');
				$('#popup_module_assessment').find('.popup_save_button').unbind('click');
				$('#popup_module_assessment').find('.popup_delete_button').unbind('click');
				$('#popup_module_assessment').find('.popup_moveup_button').unbind('click');
				$('#popup_module_assessment').find('.popup_movedown_button').unbind('click');

				$('#popup_module_assessment').find('#module_assessment_question').val(modules[active_module.attr('module_id')].question);
				$('#popup_module_assessment').find('#module_assessment_answer1').val(modules[active_module.attr('module_id')].answer1);
				$('#popup_module_assessment').find('#module_assessment_answer2').val(modules[active_module.attr('module_id')].answer2);
				$('#popup_module_assessment').find('#module_assessment_answer3').val(modules[active_module.attr('module_id')].answer3);
				$('#popup_module_assessment').find('#module_assessment_answer4').val(modules[active_module.attr('module_id')].answer4);
				$('#popup_module_assessment').find('#module_assessment_answer1_role').val(modules[active_module.attr('module_id')].answer1_role);
				$('#popup_module_assessment').find('#module_assessment_answer2_role').val(modules[active_module.attr('module_id')].answer2_role);
				$('#popup_module_assessment').find('#module_assessment_answer3_role').val(modules[active_module.attr('module_id')].answer3_role);
				$('#popup_module_assessment').find('#module_assessment_answer4_role').val(modules[active_module.attr('module_id')].answer4_role);
				
				
				
				$('#popup_module_assessment').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_assessment').hide();
				});
				
				$('#popup_module_assessment').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_assessment').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
				
				$('#popup_module_assessment').find('.popup_save_button').click(function()
				{
					modules[active_module.attr('module_id')].question = $('#popup_module_assessment').find('#module_assessment_question').val();
					modules[active_module.attr('module_id')].answer1 = $('#popup_module_assessment').find('#module_assessment_answer1').val();
					modules[active_module.attr('module_id')].answer2 = $('#popup_module_assessment').find('#module_assessment_answer2').val();
					modules[active_module.attr('module_id')].answer3 = $('#popup_module_assessment').find('#module_assessment_answer3').val();
					modules[active_module.attr('module_id')].answer4 = $('#popup_module_assessment').find('#module_assessment_answer4').val();
					modules[active_module.attr('module_id')].answer1_role = $('#popup_module_assessment').find('#module_assessment_answer1_role').val();
					modules[active_module.attr('module_id')].answer2_role = $('#popup_module_assessment').find('#module_assessment_answer2_role').val();
					modules[active_module.attr('module_id')].answer3_role = $('#popup_module_assessment').find('#module_assessment_answer3_role').val();
					modules[active_module.attr('module_id')].answer4_role = $('#popup_module_assessment').find('#module_assessment_answer4_role').val();
						
					active_module.find('.module_question').text(modules[active_module.attr('module_id')].question);
					active_module.find('.module_answer[answer=1]').text(modules[active_module.attr('module_id')].answer1);
					active_module.find('.module_answer[answer=2]').text(modules[active_module.attr('module_id')].answer2);
					active_module.find('.module_answer[answer=3]').text(modules[active_module.attr('module_id')].answer3);
					active_module.find('.module_answer[answer=4]').text(modules[active_module.attr('module_id')].answer4);
					
					$('#popup_module_assessment').hide();
				});
				
				$('#popup_module_assessment').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_assessment').hide();
				});
				
				$('#popup_module_assessment').show();
			});
		},

		unbindListeners: function()
		{

		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'type': this.type,
				'order': this.id,
				'question': this.question,
				'answer1': this.answer1,
				'answer1_role': this.answer1_role,
				'answer2': this.answer2,
				'answer2_role': this.answer2_role,
				'answer3': this.answer3,
				'answer3_role': this.answer3_role,
				'answer4': this.answer4,
				'answer4_role': this.answer4_role,
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			
		},		
		
	}
	
	return new_elem;
}