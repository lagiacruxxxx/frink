

function new_module_image(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'image',
		filename: '',
		description: '',
		id: id,
		parent: parent,
		uploadpath: '/items/uploads/fileman/',
		align: 'center',
		stretch: true,
		caption: '',
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_image"><img src="' + '/items/backend/img/image_upload_placeholder.png" /><div class="module_image_caption"></div></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.find('img').dblclick(function()
			{
				active_module = $(this).parent();

				checkMoveButtons(active_module);

				$('#popup_module_image').find('.popup_cancel_button').unbind('click');
				$('#popup_module_image').find('.popup_save_button').unbind('click');
				$('#popup_module_image').find('.popup_delete_button').unbind('click');
				$('#popup_module_image').find('.popup_moveup_button').unbind('click');
				$('#popup_module_image').find('.popup_movedown_button').unbind('click');
				
				$('#module_image_upload_button').off('click');
				$('#module_image_upload_input').off('change');
				$('#module_image_filemanager').off('click');

				if(modules[active_module.attr('module_id')].filename != '')
				{
					$('#module_image_upload_preview').attr('src', modules[active_module.attr('module_id')].filename);
					$('#module_image_upload_preview').show();
				}
				else
				{
					$('#module_image_upload_preview').attr('src', '');
					$('#module_image_upload_preview').hide();
				}
				
				$('#module_image_upload_caption').val(active_module.find('.module_image_caption').text());
				
				$('#module_image_upload_stretch').attr('checked', modules[active_module.attr('module_id')].stretch == 1);
				$('#module_image_upload_align').val(modules[active_module.attr('module_id')].align);
				
				
				$('#popup_module_image').show();
				
				$('#popup_module_image').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_image').hide();
				});
				
				$('#popup_module_image').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_image').hide();
				});
				
				$('#popup_module_image').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_image').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
				
				$('#popup_module_image').find('.popup_save_button').click(function()
				{
					modules[active_module.attr('module_id')].filename = $('#module_image_upload_preview').attr('src');
					modules[active_module.attr('module_id')].stretch = $('#module_image_upload_stretch').is(":checked") ? 1 : 0;
					modules[active_module.attr('module_id')].align = $('#module_image_upload_align').val();
					modules[active_module.attr('module_id')].caption = $('#module_image_upload_caption').val();
					
					active_module.find('img').attr('src', modules[active_module.attr('module_id')].filename);
					if(modules[active_module.attr('module_id')].stretch == 1)
						active_module.find('img').css({'width': '100%'});
					else
						active_module.find('img').css({'width': 'auto'});
					
					active_module.css({'text-align': modules[active_module.attr('module_id')].align});
					
					active_module.find('.module_image_caption').empty().text($('#module_image_upload_caption').val());
					
					$('#popup_module_image').hide();
				});
				
				$('#module_image_filemanager').on('click', function()
				{
					openCustomRoxy('image');
				});
				
				
				$('#module_image_upload_button').click(function()
				{
					$('#module_image_upload_input').click();
				});
				
				$('#module_image_upload_input').change(function()
				{
					var element_name = $(this).attr('id');
					var elem = $('#' + element_name);
					var uploadpath = modules[active_module.attr('module_id')].uploadpath;
					var xhr = new XMLHttpRequest();		
					var fd = new FormData;
					var files = this.files;
					
					fd.append('data', files[0]);
					fd.append('filename', files[0].name);
					fd.append('element', element_name);
					fd.append('uploadpath', uploadpath);
					
					xhr.addEventListener('load', function(e) 
					{
						var ret = $.parseJSON(this.responseText);
						
						if(ret.success)
						{
							var html = '<div id="bc_upload_crop"><img id="bc_upload_crop_img" src="' + modules[active_module.attr('module_id')].uploadpath + ret.filename + '" /><div id="bc_upload_crop_btn">CROP</div><div id="bc_upload_skip_btn">SKIP CROP</div></div><div id="bc_upload_crop_fade"></div>';
							$('body').append(html);
							
							var wWidth = $(document).width();
							var wHeight = $(document).height();
							var padding = 30;
							
							imagesLoaded($('#bc_upload_crop'), function()
							{
								var iWidth = $('#bc_upload_crop_img').get(0).naturalWidth;
								var iHeight = $('#bc_upload_crop_img').get(0).naturalHeight;
								var ratio = iWidth / iHeight;
								var cWidth = iWidth + padding;
								var cHeight = iHeight + padding + $('bc_#upload_crop_btn').height();
								
								if(cWidth > wWidth * 0.9)
								{
									iWidth = wWidth * 0.9 - padding;
									iHeight = iWidth / ratio;
									cWidth = iWidth + padding;
									cHeight = iHeight + padding;
								}
								
								if(cHeight > wHeight * 0.9)
								{
									iHeight = wHeight * 0.8 - padding - $('#bc_upload_crop_btn').height();
									iWidth = iHeight * ratio;
									cWidth = iWidth + padding;
									cHeight = iHeight + padding;			
								}
								
								$('#bc_upload_crop').css({'left': (wWidth - cWidth)/2, 'top': (wHeight - cHeight)/2});
								$('#bc_upload_crop_img').css({'width': iWidth, 'height': iHeight});
								
								
								areaselect = $('#bc_upload_crop_img').imgAreaSelect(
								{ 
									handles: true,
									parent: '#bc_upload_crop',
									instance: true,
								});

							});	
							
							$('#bc_upload_crop_btn').off('click');

							$('#bc_upload_crop_btn').on('click', function()
							{
								$.ajax(
								{
									url: rootUrl + 'Backend/crop_image',
									data: { 
										filename: ret.filename, 
										uploadpath: modules[active_module.attr('module_id')].uploadpath, 
										x1: areaselect.getSelection().x1, 
										y1: areaselect.getSelection().y1, 
										x2: areaselect.getSelection().x2, 
										y2: areaselect.getSelection().y2, 
										ratio: $('#bc_upload_crop_img').get(0).naturalWidth / parseInt($('#bc_upload_crop_img').css('width')),
									},
									method: 'POST',
									cache: false,
									dataType: 'json',
									success: function(data)
									{
										var ret = data;
										
										if(ret.success)
										{
											$('#bc_upload_crop_fade').remove();
											$('#bc_upload_crop').remove();
											
											$('#module_image_upload_preview').attr('src', modules[active_module.attr('module_id')].uploadpath + ret.filename + "?" + new Date().getTime());
											$('#module_image_upload_preview').attr('fname', modules[active_module.attr('module_id')].uploadpath + ret.filename);
											$('#module_image_upload_preview').show();
										}
										else
										{
											alert('Error while cropping');
										}
									}
								});
							});
							
							$('#bc_upload_skip_btn').off('click');
							
							$('#bc_upload_skip_btn').on('click', function()
							{
								$('#bc_upload_crop_fade').remove();
								$('#bc_upload_crop').remove();
								$('#module_image_upload_preview').attr('src', modules[active_module.attr('module_id')].uploadpath + ret.filename + "?" + new Date().getTime());
								$('#module_image_upload_preview').attr('fname', modules[active_module.attr('module_id')].uploadpath + ret.filename);
								$('#module_image_upload_preview').show();
							});
							
							$('#bc_upload_crop_fade').off('click');
					
							$('#bc_upload_crop_fade').on('click', function()
							{
								$('#bc_upload_crop_fade').remove();
								$('#bc_upload_crop').remove();
							});
						}
						else
						{
							show_message('error', 'Error while uploading!');
						}
				    });
					
					xhr.open('post', rootUrl + 'Backend/upload_image');
					xhr.send(fd);
				});
			});
		},

		unbindListeners: function()
		{

		},
		
		getSaveData: function()
		{
			if(this.filename != '')
			{
				var ret = 
				{
					'order': this.id,
					'fname': this.filename,
					'type': 'image',
					'align': this.align,
					'stretch': this.stretch,
					'caption': this.caption,
				};
			}
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.content);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setFontColor(properties.font_color);
			this.setFontSize(properties.font_size);
			this.setTextAlign(properties.align);
			this.setSidebarImage(properties.right_side_img);
			this.setSidebarText(properties.right_side_img_text);
		},		
		
	}
	
	return new_elem;
}