$(document).ready(function()
{	
	resize();
});

function addGlobalListeners()
{
	$(window).resize(function()
	{
		resize();
	});
}

function resize()
{
	var wWidth = $(window).width();
	var wHeight = $(window).height();
	
	$('#menu').css({'width' : wWidth});
	$('#sidebar').css({'height': wHeight - $('#menu').height()});
	$('#content').css({'height': wHeight - $('#menu').height() -40, 'width': wWidth - $('#sidebar').width() -40});
	$('.crud_widget').css({'width': wWidth - $('#sidebar').width() -60});
}


