

function new_module_multiplechoice4(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'multiplechoice4',
		id: id,
		parent: parent,
		question: '',
		answer1: '',
		answer2: '',
		answer3: '',
		answer4: '',
		correct_answer: 1,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_multiplechoice4">';
			html += '<div class="module_question">Question ...</div>';
			html += '<div class="module_answer" answer=1>Answer 1 ...</div>';
			html += '<div class="module_answer" answer=2>Answer 2 ...</div>';
			html += '<div class="module_answer" answer=3>Answer 3 ...</div>';
			html += '<div class="module_answer" answer=4>Answer 4 ...</div>';
			html += '</div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);

				checkMoveButtons(active_module);

				$('#popup_module_multiplechoice4').find('.popup_cancel_button').unbind('click');
				$('#popup_module_multiplechoice4').find('.popup_save_button').unbind('click');
				$('#popup_module_multiplechoice4').find('.popup_delete_button').unbind('click');
				$('#popup_module_multiplechoice4').find('.popup_moveup_button').unbind('click');
				$('#popup_module_multiplechoice4').find('.popup_movedown_button').unbind('click');

				$('#popup_module_multiplechoice4').find('#module_multiplechoice4_question').val(modules[active_module.attr('module_id')].question);
				$('#popup_module_multiplechoice4').find('#module_multiplechoice4_answer1').val(modules[active_module.attr('module_id')].answer1);
				$('#popup_module_multiplechoice4').find('#module_multiplechoice4_answer2').val(modules[active_module.attr('module_id')].answer2);
				$('#popup_module_multiplechoice4').find('#module_multiplechoice4_answer3').val(modules[active_module.attr('module_id')].answer3);
				$('#popup_module_multiplechoice4').find('#module_multiplechoice4_answer4').val(modules[active_module.attr('module_id')].answer4);
				
				$('#popup_module_multiplechoice4').find('#module_multiplechoice4_correct_answer').val(modules[active_module.attr('module_id')].correct_answer);
				
				
				$('#popup_module_multiplechoice4').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_multiplechoice4').hide();
				});
				
				$('#popup_module_multiplechoice4').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_multiplechoice4').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
				
				$('#popup_module_multiplechoice4').find('.popup_save_button').click(function()
				{
					modules[active_module.attr('module_id')].question = $('#popup_module_multiplechoice4').find('#module_multiplechoice4_question').val();
					modules[active_module.attr('module_id')].answer1 = $('#popup_module_multiplechoice4').find('#module_multiplechoice4_answer1').val();
					modules[active_module.attr('module_id')].answer2 = $('#popup_module_multiplechoice4').find('#module_multiplechoice4_answer2').val();
					modules[active_module.attr('module_id')].answer3 = $('#popup_module_multiplechoice4').find('#module_multiplechoice4_answer3').val();
					modules[active_module.attr('module_id')].answer4 = $('#popup_module_multiplechoice4').find('#module_multiplechoice4_answer4').val();
					modules[active_module.attr('module_id')].correct_answer = $('#popup_module_multiplechoice4').find('#module_multiplechoice4_correct_answer').val();
						
					active_module.find('.module_question').text(modules[active_module.attr('module_id')].question);
					active_module.find('.module_answer[answer=1]').text(modules[active_module.attr('module_id')].answer1);
					active_module.find('.module_answer[answer=2]').text(modules[active_module.attr('module_id')].answer2);
					active_module.find('.module_answer[answer=3]').text(modules[active_module.attr('module_id')].answer3);
					active_module.find('.module_answer[answer=4]').text(modules[active_module.attr('module_id')].answer4);
					
					$('#popup_module_multiplechoice4').hide();
				});
				
				$('#popup_module_multiplechoice4').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_multiplechoice4').hide();
				});
				
				$('#popup_module_multiplechoice4').show();
			});
		},

		unbindListeners: function()
		{

		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'type': 'multiplechoice4',
				'order': this.id,
				'question': this.question,
				'answer1': this.answer1,
				'answer2': this.answer2,
				'answer3': this.answer3,
				'answer4': this.answer4,
				'correct_answer': this.correct_answer,
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.content);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setFontColor(properties.font_color);
			this.setFontSize(properties.font_size);
			this.setTextAlign(properties.align);
			this.setSidebarImage(properties.right_side_img);
			this.setSidebarText(properties.right_side_img_text);
		},		
		
	}
	
	return new_elem;
}