

function new_module_accordion_start(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'accordion_start',
		id: id,
		parent: parent,
		headline: 'Headline',
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_accordion_start">Headline</div>';
			//html += '<div class="module_accordion_content">';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);

				checkMoveButtons(active_module);

				$('#popup_module_accordionstart').find('.popup_cancel_button').unbind('click');
				$('#popup_module_accordionstart').find('.popup_save_button').unbind('click');
				$('#popup_module_accordionstart').find('.popup_delete_button').unbind('click');
				$('#popup_module_accordionstart').find('.popup_moveup_button').unbind('click');
				$('#popup_module_accordionstart').find('.popup_movedown_button').unbind('click');

				$('#popup_module_accordionstart').find('#module_accordion_headline').val(modules[active_module.attr('module_id')].headline);
				
				$('#popup_module_accordionstart').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_accordionstart').hide();
				});
				
				$('#popup_module_accordionstart').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_accordionstart').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
				
				$('#popup_module_accordionstart').find('.popup_save_button').click(function()
				{
					modules[active_module.attr('module_id')].headline = $('#popup_module_accordionstart').find('#module_accordion_headline').val();
						
					active_module.text(modules[active_module.attr('module_id')].headline);
					
					$('#popup_module_accordionstart').hide();
				});
				
				$('#popup_module_accordionstart').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_accordionstart').hide();
				});
				
				$('#popup_module_accordionstart').show();
			});
		},

		unbindListeners: function()
		{

		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'type': 'accordion_start',
				'order': this.id,
				'headline': this.headline,
			};
			
			return ret;
		},
	}
	
	return new_elem;
}







function new_module_accordion_end(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'accordion_start',
		id: id,
		parent: parent,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_accordion_end"></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);

				checkMoveButtons(active_module);

				$('#popup_module_accordionend').find('.popup_cancel_button').unbind('click');
				$('#popup_module_accordionend').find('.popup_delete_button').unbind('click');
				$('#popup_module_accordionend').find('.popup_moveup_button').unbind('click');
				$('#popup_module_accordionend').find('.popup_movedown_button').unbind('click');

				$('#popup_module_accordionend').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_accordionstart').hide();
				});
				
				$('#popup_module_accordionend').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_accordionend').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
				
				$('#popup_module_accordionend').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_accordionend').hide();
				});
				
				$('#popup_module_accordionend').show();
			});
		},

		unbindListeners: function()
		{

		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'type': 'accordion_end',
				'order': this.id,
			};
			
			return ret;
		},
	}
	
	return new_elem;
}