

function new_module_gallery(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'gallery',
		id: id,
		parent: parent,
		uploadpath: '/items/uploads/fileman/',
		items: [],
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_gallery has_placeholder" data-text="Doubleclick to edit"></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);

				checkMoveButtons(active_module);

				$('#popup_module_gallery').find('.popup_cancel_button').unbind('click');
				$('#popup_module_gallery').find('.popup_save_button').unbind('click');
				$('#popup_module_gallery').find('.popup_delete_button').unbind('click');
				$('#popup_module_gallery').find('.popup_moveup_button').unbind('click');
				$('#popup_module_gallery').find('.popup_movedown_button').unbind('click');
				
				$('#module_gallery_upload_button').off('click');
				$('#module_gallery_upload_input').off('change');
				$('#module_image_filemanager').off('click');
				
				$('#module_gallery_items').empty();
				var i;
				for( i = 0; i < modules[active_module.attr('module_id')].items.length ; i++)
				{
					$('#module_gallery_items').append('<div class="gallery_item" fname="' + modules[active_module.attr('module_id')].items[i].fname + '"><img src="' + modules[active_module.attr('module_id')].items[i].fname + '"></div>');
				}
				
				$('#module_gallery_items').find('.gallery_item').off('dblclick');
				$('#module_gallery_items').find('.gallery_item').on('dblclick', function()
				{
					$(this).remove();
				});
				
				
				$('#module_gallery_items').sortable();
			    $( "#module_gallery_items" ).disableSelection();

			    
			    $('#module_gallery_filemanager').on('click', function()
				{
					openCustomRoxy('gallery');
				});

				$('#popup_module_gallery').show();
				
				$('#popup_module_gallery').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_gallery').hide();
				});
				
				$('#popup_module_gallery').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_gallery').hide();
				});
				
				$('#popup_module_gallery').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_gallery').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
				
				$('#popup_module_gallery').find('.popup_save_button').click(function()
				{
					var items = [];
					var order = 0;
					var html = '';
					$('#module_gallery_items').find('.gallery_item').each(function()
					{
						html += '<div class="module_gallery_item"><img src="' + $(this).attr('fname') + '"></div>';
						items.push
						( 
							{
								'fname': $(this).attr('fname'),
								'ordering': order++,
							}
						);
					});
					
					modules[active_module.attr('module_id')].items = items;
					active_module.empty().append(html);
					
					$('#popup_module_gallery').hide();
				});
				
				
				$('#module_gallery_upload_button').click(function()
				{
					$('#module_gallery_upload_input').click();
				});
				
				$('#module_gallery_upload_input').change(function()
				{
					var element_name = $(this).attr('id');
					var elem = $('#' + element_name);
					var uploadpath = modules[active_module.attr('module_id')].uploadpath;
					var xhr = new XMLHttpRequest();		
					var fd = new FormData;
					var files = this.files;
					
					fd.append('data', files[0]);
					fd.append('filename', files[0].name);
					fd.append('element', element_name);
					fd.append('uploadpath', uploadpath);
					
					xhr.addEventListener('load', function(e) 
					{
						var ret = $.parseJSON(this.responseText);
						
						if(ret.success)
						{
							var html = '<div id="bc_upload_crop"><img id="bc_upload_crop_img" src="' + modules[active_module.attr('module_id')].uploadpath + ret.filename + '" /><div id="bc_upload_crop_btn">CROP</div><div id="bc_upload_skip_btn">SKIP CROP</div></div><div id="bc_upload_crop_fade"></div>';
							$('body').append(html);
							
							var wWidth = $(document).width();
							var wHeight = $(document).height();
							var padding = 30;
							
							imagesLoaded($('#bc_upload_crop'), function()
							{
								var iWidth = $('#bc_upload_crop_img').get(0).naturalWidth;
								var iHeight = $('#bc_upload_crop_img').get(0).naturalHeight;
								var ratio = iWidth / iHeight;
								var cWidth = iWidth + padding;
								var cHeight = iHeight + padding + $('bc_#upload_crop_btn').height();
								
								if(cWidth > wWidth * 0.9)
								{
									iWidth = wWidth * 0.9 - padding;
									iHeight = iWidth / ratio;
									cWidth = iWidth + padding;
									cHeight = iHeight + padding;
								}
								
								if(cHeight > wHeight * 0.9)
								{
									iHeight = wHeight * 0.8 - padding - $('#bc_upload_crop_btn').height();
									iWidth = iHeight * ratio;
									cWidth = iWidth + padding;
									cHeight = iHeight + padding;			
								}
								
								$('#bc_upload_crop').css({'left': (wWidth - cWidth)/2, 'top': (wHeight - cHeight)/2});
								$('#bc_upload_crop_img').css({'width': iWidth, 'height': iHeight});
								
								
								areaselect = $('#bc_upload_crop_img').imgAreaSelect(
								{ 
									handles: true,
									parent: '#bc_upload_crop',
									instance: true,
								});

							});	
							
							$('#bc_upload_crop_btn').off('click');

							$('#bc_upload_crop_btn').on('click', function()
							{
								$.ajax(
								{
									url: rootUrl + 'Backend/crop_image',
									data: { 
										filename: ret.filename, 
										uploadpath: modules[active_module.attr('module_id')].uploadpath, 
										x1: areaselect.getSelection().x1, 
										y1: areaselect.getSelection().y1, 
										x2: areaselect.getSelection().x2, 
										y2: areaselect.getSelection().y2, 
										ratio: $('#bc_upload_crop_img').get(0).naturalWidth / parseInt($('#bc_upload_crop_img').css('width')),
									},
									method: 'POST',
									cache: false,
									dataType: 'json',
									success: function(data)
									{
										var ret = data;
										
										if(ret.success)
										{
											$('#bc_upload_crop_fade').remove();
											$('#bc_upload_crop').remove();
											$('#module_gallery_items').append('<div class="gallery_item" fname="' + modules[active_module.attr('module_id')].uploadpath + ret.filename + '"><img src="' + modules[active_module.attr('module_id')].uploadpath + ret.filename + "?" + new Date().getTime() + '"/></div>');
											$('#module_gallery_items').find('.gallery_item').off('dblclick');
											$('#module_gallery_items').find('.gallery_item').on('dblclick', function()
											{
												$(this).remove();
											});
										}
										else
										{
											alert('Error while cropping');
										}
									}
								});
							});
							
							$('#bc_upload_skip_btn').off('click');
							
							$('#bc_upload_skip_btn').on('click', function()
							{
								$('#bc_upload_crop_fade').remove();
								$('#bc_upload_crop').remove();
								$('#module_gallery_items').append('<div class="gallery_item" fname="' + modules[active_module.attr('module_id')].uploadpath + ret.filename + '"><img src="' + modules[active_module.attr('module_id')].uploadpath + ret.filename + "?" + new Date().getTime() + '"/></div>');
								$('#module_gallery_items').find('.gallery_item').off('dblclick');
								$('#module_gallery_items').find('.gallery_item').on('dblclick', function()
								{
									$(this).remove();
								});
							});
							
							$('#bc_upload_crop_fade').off('click');
					
							$('#bc_upload_crop_fade').on('click', function()
							{
								$('#bc_upload_crop_fade').remove();
								$('#bc_upload_crop').remove();
							});
						}
						else
						{
							show_message('error', 'Error while uploading!');
						}
				    });
					
					xhr.open('post', rootUrl + 'Backend/upload_image');
					xhr.send(fd);
				});
			});
		},

		unbindListeners: function()
		{

		},
		
		getSaveData: function()
		{
			var gallery_items = [];
			var ret = 
			{
				'order': this.id,
				'items': this.items,
				'type': 'gallery',
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.content);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setFontColor(properties.font_color);
			this.setFontSize(properties.font_size);
			this.setTextAlign(properties.align);
			this.setSidebarImage(properties.right_side_img);
			this.setSidebarText(properties.right_side_img_text);
		},		
		
	}
	
	return new_elem;
}