$(document).ready(function()
{
	igniteWidget();
});


function igniteWidget()
{
	$(".widgetmenu_item.client select").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		placeholder_text_multiple: 'Select client',
		width: "300px",
	});
	
	$(".widgetmenu_item.language select").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		placeholder_text_multiple: 'Select language',
		width: "300px",
	});
		
	$(".widgetmenu_item.startdate input[type='text']").datepicker({
		dateFormat: 'dd.mm.yy',
		changeMonth: true,
        changeYear: true,
	});

	$(".widgetmenu_item.enddate input[type='text']").datepicker({
		dateFormat: 'dd.mm.yy',
		changeMonth: true,
        changeYear: true,
	});
	
	
	$('#widget_query').on('click', function()
	{
		if($(this).hasClass('quiz'))
		{
			$.ajax(
			{
				url: rootUrl + 'Backend/quiz_stats_query',
				data: {
					'clients': $(".widgetmenu_item.client select").val(),
					'lang': $(".widgetmenu_item.language select").val(),
					'startdate': $(".widgetmenu_item.startdate input[type='text']").val(),
					'enddate': $(".widgetmenu_item.enddate input[type='text']").val(),
				},
				method: 'POST',
				success: function(data)
				{
					var ret = $.parseJSON(data);
					
					$('#widgetresult').empty().html(ret.results);
				}
			});
		}
		else
		{
			$.ajax(
			{
				url: rootUrl + 'Backend/assessment_stats_query',
				data: {
					'clients': $(".widgetmenu_item.client select").val(),
					'lang': $(".widgetmenu_item.language select").val(),
					'startdate': $(".widgetmenu_item.startdate input[type='text']").val(),
					'enddate': $(".widgetmenu_item.enddate input[type='text']").val(),
				},
				method: 'POST',
				success: function(data)
				{
					var ret = $.parseJSON(data);
					
					$('#widgetresult').empty().html(ret.results);
				}
			});			
		}
	});
	
	$('#show_results').on('click', function()
	{
		
	});
}
