var id_increment = 0;
var modules = [];
var active_module = null;

$(document).ready(function()
{	
	toggleMenuListeners(true);
});




function toggleMenuListeners(toggle)
{
	if(toggle)
	{
		
		$('#menu_save').on('click', function()
		{
			saveClientContent();
		});
		
		$('#menu_discard').on('click', function()
		{
			window.location.href = rootUrl + 'entities/User/client';
		});
		
		$('#select_all').on('click', function()
		{
			$('#client_content_table input[type="checkbox"]').prop('checked', true);
		});

		$('#deselect_all').on('click', function()
		{
			$('#client_content_table input[type="checkbox"]').prop('checked', false);
		});		
	}
	else
	{
		$('#menu_save').off('click');
		$('#menu_discard').off('click');
	}
}


function saveClientContent()
{
	var i = 0;
	var content = [];
	$('#client_content_table input[type="checkbox"]:checked').each(function()
	{
		content.push($(this).attr('content_id'));
	});
	
	$.ajax(
	{
		url: rootUrl + 'entities/User/save_client_content',
		data: {
			'client_id': clientId,
			'contentItems': content,
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				alert('Save successful!');
				window.location.href = rootUrl + 'entities/User/client';
			}
			else
			{
				alert('Error while saving');
			}
		}
	});	
}




