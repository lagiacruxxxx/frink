

function new_module_text(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'text',
		text: '',
		id: id,
		parent: parent,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_text has_placeholder" data-text="Enter text here"></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);
				
				checkMoveButtons(active_module);

				$('#popup_module_text').find('.popup_cancel_button').unbind('click');
				$('#popup_module_text').find('.popup_save_button').unbind('click');
				$('#popup_module_text').find('.popup_delete_button').unbind('click');
				$('#popup_module_text').find('.popup_moveup_button').unbind('click');
				$('#popup_module_text').find('.popup_movedown_button').unbind('click');

				CKEDITOR.instances.module_text_editor.setData(active_module.html());
				$('#popup_module_text').show();
				
				$('#popup_module_text').find('.popup_save_button').click(function()
				{
					active_module.html(CKEDITOR.instances.module_text_editor.getData());
					$('#popup_module_text').hide();
				});
				
				$('#popup_module_text').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_text').hide();
				});
				
				$('#popup_module_text').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_text').hide();
				});
				
				$('#popup_module_text').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_text').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
						
			});
		},
		
		unbindListeners: function()
		{
			this.module.unbind('dblclick');
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'order': this.id,
				'content': this.module.html(),
				'type': 'text',
			};
			
			return ret;
		},
	
		load: function(properties)
		{
	
		},		
		
	}
	
	return new_elem;
}