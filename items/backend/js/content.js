var id_increment = 0;
var modules = [];
var active_module = null;
var fileman_custom_url = rootUrl + 'fileman_custom/index.html?integration=custom';

$(document).ready(function()
{	
	toggleContentMenuListeners(true);
	toggleScrollListeners(true);
	igniteCKEditor();
	initModules();
	initModulePopups();
	
});


function toggleContentMenuListeners(toggle)
{
	if(toggle)
	{
		$('#content_lang_switch').on('click', function()
		{
			if(parseInt($('#content_lang_select').val()) != contentLang)
			{
				switch(contentType)
				{
				case 0:
				case 1:
				case 4:
					window.location.href = rootUrl + 'entities/Content/edit_content/' + contentId + '/' + $('#content_lang_select').val();
					break;
					
				case 6:
				case 7:
					window.location.href = rootUrl + 'entities/Content/edit_result_content/' + contentId + '/' + $('#content_lang_select').val();
					break;

				case 2:
					window.location.href = rootUrl + 'entities/Content/edit_question/' + contentId + '/' + $('#content_lang_select').val();
					break;
				case 5:
					window.location.href = rootUrl + 'entities/Content/edit_assessment_question/' + contentId + '/' + $('#content_lang_select').val();
					break;
				}
			}	
		});
		
		$('#content_module_add').on('click', function()
		{
			new_module($('#content_module_select').val(), $('#content_container'));
		});
		
		$('#content_save').on('click', function()
		{
			saveContent();
		});
		
		$('#content_discard').on('click', function()
		{
			switch(contentType)
			{
			case 0:
				window.location.href = rootUrl + 'entities/Content/article';
				break;
			case 1:
			case 7:
				window.location.href = rootUrl + 'entities/Content/quiz';
				break;
			case 2:
				window.location.href = rootUrl + 'entities/Content/quiz_question/' + quizId;
				break;
			case 4:
			case 6:
				window.location.href = rootUrl + 'entities/Content/assessment';
				break;
			case 5:
				window.location.href = rootUrl + 'entities/Content/assessment_question/' + assessmentId;
				break;
			case 8:
				window.location.href = rootUrl + 'entities/Content/lesson/';
				break;
					
			}
		});
	}
	else
	{
		$('#content_lang_switch').off('click');
		$('#content_module_add').off('click');
		$('#content_save').off('click');
		$('#content_discard').off('click');
	}
}



function initModules()
{
	$('.module').each(function()
	{
		if($(this).hasClass('module_text'))
		{
			modules[$(this).attr('module_id')] = new_module_text($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].text = $(this).html();
		}
		
		if($(this).hasClass('module_image'))
		{
			modules[$(this).attr('module_id')] = new_module_image($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].filename = $(this).children('img').attr('src');
			modules[$(this).attr('module_id')].align = $(this).css('text-align');
			modules[$(this).attr('module_id')].caption = $(this).find('.module_image_caption').text();
			modules[$(this).attr('module_id')].stretch =  $(this).children('img').attr('style').search('100%') != -1 ? 1 : 0;
		}
		
		if($(this).hasClass('module_multiplechoice4'))
		{
			modules[$(this).attr('module_id')] = new_module_multiplechoice4($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].question = $(this).find('.module_question').text();
			modules[$(this).attr('module_id')].answer1 = $(this).find('.module_answer[answer=1]').text();
			modules[$(this).attr('module_id')].answer2 = $(this).find('.module_answer[answer=2]').text();
			modules[$(this).attr('module_id')].answer3 = $(this).find('.module_answer[answer=3]').text();
			modules[$(this).attr('module_id')].answer4 = $(this).find('.module_answer[answer=4]').text();
			modules[$(this).attr('module_id')].correct_answer = $(this).attr('correct_answer');
		}
		
		if($(this).hasClass('module_assessment'))
		{
			modules[$(this).attr('module_id')] = new_module_assessment($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].question = $(this).find('.module_question').text();
			modules[$(this).attr('module_id')].answer1 = $(this).find('.module_answer[answer=1]').text();
			modules[$(this).attr('module_id')].answer2 = $(this).find('.module_answer[answer=2]').text();
			modules[$(this).attr('module_id')].answer3 = $(this).find('.module_answer[answer=3]').text();
			modules[$(this).attr('module_id')].answer4 = $(this).find('.module_answer[answer=4]').text();
			modules[$(this).attr('module_id')].answer1_role = parseInt($(this).find('.module_answer[answer=1]').attr('role'));
			modules[$(this).attr('module_id')].answer2_role = parseInt($(this).find('.module_answer[answer=2]').attr('role'));
			modules[$(this).attr('module_id')].answer3_role = parseInt($(this).find('.module_answer[answer=3]').attr('role'));
			modules[$(this).attr('module_id')].answer4_role = parseInt($(this).find('.module_answer[answer=4]').attr('role'));
		}
		
		if($(this).hasClass('module_video'))
		{
			modules[$(this).attr('module_id')] = new_module_video($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].code = $(this).attr('code');
			modules[$(this).attr('module_id')].start = $(this).attr('start');
		}	
		
		if($(this).hasClass('module_accordion_start'))
		{
			modules[$(this).attr('module_id')] = new_module_accordion_start($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].headline = $(this).text();
		}
		
		if($(this).hasClass('module_accordion_end'))
		{
			modules[$(this).attr('module_id')] = new_module_accordion_end($(this).attr('module_id'), $(this).parent());
		}
		
		if($(this).hasClass('module_sequence'))
		{
			modules[$(this).attr('module_id')] = new_module_sequence($(this).attr('module_id'), $(this).parent());
			var items = [];
			var order = 0;
			$(this).find('.module_sequence_item').each(function()
			{
				items.push
				( 
					{
						'img': $(this).find('.module_sequence_img img').attr('src'),
						'headline': $(this).find('.module_sequence_headline').text(),
						'desc': $(this).find('.module_sequence_text').html(),
						'ordering': order++,
					}
				);
			});
			modules[$(this).attr('module_id')].items = items;
		}
		
		if($(this).hasClass('module_gallery'))
		{
			modules[$(this).attr('module_id')] = new_module_gallery($(this).attr('module_id'), $(this).parent());
			var items = [];
			var order = 0;
			$(this).find('.module_gallery_item').each(function()
			{
				items.push
				( 
					{
						'fname': $(this).attr('fname'),
						'ordering': order++,
					}
				);
			});
			modules[$(this).attr('module_id')].items = items;
		}
		
		if($(this).hasClass('module_vimeo'))
		{
			modules[$(this).attr('module_id')] = new_module_vimeo($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].code = $(this).attr('code');
		}	
		
		if(id_increment <= parseInt($(this).attr('module_id')) +1)
			id_increment = parseInt($(this).attr('module_id')) +1;
		
		modules[$(this).attr('module_id')].init();
	});
	
	fixSequenceItemHeight();	
}

function toggleButtonListeners(toggle)
{
	if(toggle)
	{
		$('.item_save').click(function()
		{
			saveItem();
		});
		
		$('.item_cancel').click(function()
		{
			window.location.href = rootUrl + 'entities/item/items';
		});
		
		$('.item_detail_type_switch').on('click', function()
		{
			$('.item_detail_type_switch').toggleClass('item_detail_type_active');
			toggleDetailType($('.item_detail_type_switch.item_detail_type_active').attr('detail_type'));
		});
	}
	else
	{
		$('.item_save').unbind('click');
		$('.item_cancel').unbind('click');
	}
}



function saveContent()
{
	var mods = [];
	for(var i = 0 ; i < id_increment ; i++)
	{
		if(modules[i] !== undefined)
		{
			if(modules[i].getSaveData() != null)
				mods.push(modules[i].getSaveData());
		}
	}
	
	$.ajax(
	{
		url: rootUrl + 'entities/Content/save_content',
		data: {
			'id': contentId,
			'language_id': contentLang,
			'contentType': contentType,
			'modules': mods,
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				alert('Save successful!');
			}
			else
			{
				alert('Error while saving');
			}
		}
	});	
}


function igniteCKEditor()
{
	var roxyFileman = rootUrl + 'fileman_ckeditor/index.html'; 
	//$('#module_text_editor').ckeditor();	
	CKEDITOR.replace( 'module_text_editor', 
	{
		filebrowserBrowseUrl:roxyFileman,
        filebrowserImageBrowseUrl:roxyFileman+'?type=image',
		customConfig: rootUrl + '/items/backend/ckeditor/config_text.js'
	});
	//$('#module_sequence_text').ckeditor();
	CKEDITOR.replace( 'module_sequence_text', 
	{
	    customConfig: rootUrl + '/items/backend/ckeditor/config_sequence.js'
	});
}



function new_module(module_type, parent)
{
	var new_module = null;
	
	switch(module_type)
	{
		case 'text':
			modules[id_increment] = new_module_text(id_increment, parent);
			break;
		case 'image':
			modules[id_increment] = new_module_image(id_increment, parent);
			break;
		case 'multiplechoice4':
			modules[id_increment] = new_module_multiplechoice4(id_increment, parent);
			break;	
		case 'video':
			modules[id_increment] = new_module_video(id_increment, parent);
			break;	
		case 'accordion_start':
			modules[id_increment] = new_module_accordion_start(id_increment, parent);
			break;
		case 'accordion_end':
			modules[id_increment] = new_module_accordion_end(id_increment, parent);
			break;	
		case 'sequence':
			modules[id_increment] = new_module_sequence(id_increment, parent);
			break;				
		case 'assessment':
			modules[id_increment] = new_module_assessment(id_increment, parent);
			break;
		case 'gallery':
			modules[id_increment] = new_module_gallery(id_increment, parent);
			break;
		case 'vimeo':
			modules[id_increment] = new_module_vimeo(id_increment, parent);
			break;
	}
	
	parent.append(modules[id_increment].getPrototypeHTML());
	modules[id_increment].init();
	
	id_increment++;
}


function toggleScrollListeners(toggle)
{
	if(toggle)
	{
		$('#content').scroll(function() 
		{
			$('#content_menu_container').css({'top': $('#content').scrollTop()});
		});
	}
	else
	{
		$('#content').unbind('scroll');
	}
}


function moveUp(module)
{
	var $div = module;
	modules[module.attr('module_id')].id--;
	modules[$div.prev('.module').attr('module_id')].id++;
	$div.prev('.module').before($div);	
	checkMoveButtons(module);
}

function moveDown(module)
{
	var $div = module;
	modules[module.attr('module_id')].id++;
	modules[$div.next('.module').attr('module_id')].id--;
	$div.next('.module').after($div);
	checkMoveButtons(module);
}

function checkMoveButtons(module)
{
	if(modules[module.attr('module_id')].id <= 0)
		$('.popup_moveup_button').hide();
	else
		$('.popup_moveup_button').show();
	
	if(modules[module.attr('module_id')].id >= modules.length - 1)
		$('.popup_movedown_button').hide();
	else
		$('.popup_movedown_button').show();
}


function nl2br(str, is_xhtml) 
{   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function br2nl(str)
{
    return str.replace(/<br>/g, "\r");
};


function getFnameFromImageSource(src)
{
	return src.substring(src.lastIndexOf('/')+1);
}



function initModulePopups()
{
	$('#popup_module_text').css({'left': ($(document).width() -990) /2, 'top': ($(document).height() - 600)/2});
}



function openCustomRoxy(moduleType)
{
	//$('#roxyCustomPanel').height(parseInt($(window).width() * 0.8));
	//$('#roxyCustomPanel').width(parseInt($(window).width() * 0.8));
	$('#roxyCustomPanel iframe').attr('src', fileman_custom_url + '&module_type=' + moduleType);
	$('#roxyCustomPanel').dialog({
		width: parseInt($(window).width() * 0.8),
		height: parseInt($(window).height() * 0.8),
		modal:true, 
	});
}

function closeCustomRoxy()
{
	$('#roxyCustomPanel').dialog('close');
}

