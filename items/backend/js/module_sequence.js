var editing_sequence_item = null;

function new_module_sequence(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'sequence',
		text: '',
		id: id,
		parent: parent,
		items: [],
		orientation: 0,
		uploadpath: '/items/uploads/fileman/',
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.bindListeners();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_sequence has_placeholder" data-text="Enter sequence items here">';
			html += '</div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);
				
				checkMoveButtons(active_module);

				$('#popup_module_sequence').find('.popup_cancel_button').unbind('click');
				$('#popup_module_sequence').find('.popup_save_button').unbind('click');
				$('#popup_module_sequence').find('.popup_delete_button').unbind('click');
				$('#popup_module_sequence').find('.popup_moveup_button').unbind('click');
				$('#popup_module_sequence').find('.popup_movedown_button').unbind('click');
				$('#module_sequence_upload_button').off('click');
				$('#module_sequence_upload_input').off('change');
				$('#module_sequence_add_button').off('click');
				$('#module_sequence_filemanager').off('click');
				$('#popup_module_sequence').find('#module_sequence_orientation').off('change');
				toggleSequenceItemControllListeners(false);	
				
				$('#popup_module_sequence').find('#module_sequence_orientation').val(active_module.hasClass('leftright') ? 1 : 0);
				
				$('#popup_module_sequence').find('#sequence_list').empty();
				$('#popup_module_sequence').find('#sequence_list').append(active_module.html());
				$('#popup_module_sequence').find('#sequence_list').find('.module_sequence_item').each(function()
				{
					$(this).append('<div class="module_sequence_item_cont"><div class="module_sequence_item_cont_edit">EDIT</div><div class="module_sequence_item_cont_delete">DELETE</div></div>');
				});
				
				if(active_module.hasClass('leftright'))
					$('#popup_module_sequence').find('#sequence_list').addClass('leftright');
				else
					$('#popup_module_sequence').find('#sequence_list').removeClass('leftright');
					
				$('#popup_module_sequence').find('#module_sequence_orientation').on('change', function()
				{
					if($(this).val() == 0)
						$('#popup_module_sequence').find('#sequence_list').removeClass('leftright');
					else
						$('#popup_module_sequence').find('#sequence_list').addClass('leftright');
				});
				
				$('#popup_module_sequence').find('#sequence_list').sortable();
				
				$('#popup_module_sequence').find('.popup_save_button').click(function()
				{
					var items = [];
					var order = 0;
					$('#sequence_list').find('.module_sequence_item').each(function()
					{
						items.push
						( 
							{
								'img': $(this).find('.module_sequence_img img').attr('src'),
								'desc': br2nl($(this).find('.module_sequence_text').html()),
								'headline': $(this).find('.module_sequence_headline').text(),
								'ordering': order++,
							}
						);
					});
					
					
					active_module.empty();
					$('#popup_module_sequence').find('.module_sequence_item_cont').remove();
					active_module.append($('#sequence_list').html());
					fixSequenceItemHeight();
					
					modules[active_module.attr('module_id')].items = items;
					modules[active_module.attr('module_id')].orientation = $('#popup_module_sequence').find('#module_sequence_orientation').val();
					
					if(modules[active_module.attr('module_id')].orientation == 0)
						active_module.removeClass('leftright');
					else
						active_module.addClass('leftright');
					
					$('#popup_module_sequence').hide();
				});
				
				$('#popup_module_sequence').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_sequence').hide();
				});
				
				$('#popup_module_sequence').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					$('#popup_module_sequence').hide();
				});
				
				$('#popup_module_sequence').find('.popup_moveup_button').click(function()
				{
					moveUp(active_module);
				});
				
				$('#popup_module_sequence').find('.popup_movedown_button').click(function()
				{
					moveDown(active_module);
				});
				
				$('#module_sequence_filemanager').on('click', function()
				{
					openCustomRoxy('sequence');
				});
				
				$('#module_sequence_add_button').on('click', function()
				{
					if($(this).attr('mode') == 'add')
					{
						var html = '<div class="module_sequence_item">';
						html += '<div class="module_sequence_img"><img src="' + $('#module_sequence_upload_preview').attr('src') +  '" /></div>';
						html += '<div class="module_sequence_desc"><div class="module_sequence_enlarge">Click to enlarge</div><div class="module_sequence_ordering">?</div>';
						html += '<div class="module_sequence_headline">' + $('#module_sequence_headline').val() + '</div><div class="module_sequence_text">'+ CKEDITOR.instances.module_sequence_text.getData() + '</div></div>';
						html += '<div class="module_sequence_item_cont"><div class="module_sequence_item_cont_edit">EDIT</div><div class="module_sequence_item_cont_delete">DELETE</div></div>';
						html += '</div>';
						$('#sequence_list').append(html);	
					}
					else
					{
						editing_sequence_item.find('.module_sequence_img img').attr('src', $('#module_sequence_upload_preview').attr('src'));
						editing_sequence_item.find('.module_sequence_text').html(CKEDITOR.instances.module_sequence_text.getData());
						editing_sequence_item.find('.module_sequence_headline').text($('#module_sequence_headline').val());
					}
					
					toggleSequenceItemControllListeners(false);
					toggleSequenceItemControllListeners(true);
							
					fixSequenceItemHeight();

					editing_sequence_item = null;
					$('#module_sequence_add_button').attr('mode', 'add');
					CKEDITOR.instances.module_sequence_text.setData( '' );
					$('#module_sequence_headline').val('');
					$('#module_sequence_upload_preview').hide().attr('src', '');
				});
				
				$('#module_sequence_upload_button').on('click', function()
				{
					$('#module_sequence_upload_input').click();
				});
				
				$('#module_sequence_upload_input').on('change', function()
				{
					var element_name = $(this).attr('id');
					var elem = $('#' + element_name);
					var xhr = new XMLHttpRequest();		
					var fd = new FormData;
					var files = this.files;
					
					fd.append('data', files[0]);
					fd.append('filename', files[0].name);
					fd.append('element', element_name);
					fd.append('uploadpath', modules[active_module.attr('module_id')].uploadpath);
					
					xhr.addEventListener('load', function(e) 
					{
						var ret = $.parseJSON(this.responseText);
						
						if(ret.success)
						{
							
							$('#module_sequence_upload_preview').attr('src', modules[active_module.attr('module_id')].uploadpath + ret.filename);
							$('#module_sequence_upload_preview').show();
						}
						else
						{
							show_message('error', 'Error while uploading!');
						}
				    });
					
					xhr.open('post', rootUrl + 'Backend/upload_image');
					xhr.send(fd);
				});
				
				toggleSequenceItemControllListeners(true);
				
				$('#popup_module_sequence').show();
			});
		},
		
		updateItems: function()
		{
			
		},
		
		unbindListeners: function()
		{
			this.module.unbind('dblclick');
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'order': this.id,
				'items': this.items,
				'type': 'sequence',
				'orientation': this.orientation,
			};
			
			if(this.items.length > 0)
				return ret;
			else
				return null;
		},
	
	}
	
	return new_elem;
}


function toggleSequenceItemControllListeners(toggle)
{
	if(toggle)
	{
		$('#popup_module_sequence').find('.module_sequence_item_cont_edit').on('click', function()
		{
			CKEDITOR.instances.module_sequence_text.setData($(this).parent().parent().find('.module_sequence_text').html());
			$('#module_sequence_upload_preview').attr('src', $(this).parent().parent().find('.module_sequence_img img').attr('src')).show();
			$('#module_sequence_headline').val($(this).parent().parent().find('.module_sequence_headline').text());
			editing_sequence_item = $(this).parent().parent();
			$('#module_sequence_add_button').attr('mode', 'edit');
			
		});
		
		$('#popup_module_sequence').find('.module_sequence_item_cont_delete').on('click', function()
		{
			$(this).parent().parent().remove();
			fixSequenceItemHeight();
		});
	}
	else
	{
		$('#popup_module_sequence').find('.module_sequence_item_cont_edit').off('click');
		$('#popup_module_sequence').find('.module_sequence_item_cont_delete').off('click');
	}
}

function fixSequenceItemHeight()
{
	/*$('.module_sequence, #sequence_list').each(function()
	{
		$(this).find('.module_sequence_item').css('height', 'auto');
		var height = 0;
		$(this).find('.module_sequence_item').each(function()
		{
			if(height < $(this).height())
				height = $(this).height();
		})
		
		$(this).find('.module_sequence_item').height(height);
	});*/	
	
	$('.module_sequence, #sequence_list').each(function()
	{
		if($(this).hasClass('leftright'))
		{
			var height = 0;
			$(this).find('.module_sequence_item').each(function()
			{
				if(height <= $(this).find('.module_sequence_desc').outerHeight())
					height = $(this).find('.module_sequence_desc').outerHeight();
			});
			
			$(this).find('.module_sequence_item').height(height);
		}
		else
		{
			var height = 0;
			$(this).find('.module_sequence_item').each(function()
			{
				if(height <= $(this).find('.module_sequence_desc').outerHeight())
					height = $(this).find('.module_sequence_desc').outerHeight();
			});
			
			$(this).find('.module_sequence_item').height(height + 148);
		}
		
	});
}
