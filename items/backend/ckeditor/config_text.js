/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) 
{
	config.removePlugins = 'uploadimage';
	config.uploadUrl = '';
	
	//config.extraPlugins = 'imageuploader';
	
	config.enterMode = CKEDITOR.ENTER_BR;
	
	config.font_names = 'Oswald regular/oswald_regularregular;' + 
						'Trade Gothic/TradeGothic;' + 
						'Trade Gothic Bold/TradeGothic-Bold;' +
						'Trade Gothic BoldCondTwenty/TradeGothic-BoldCondTwenty;' +
						'Trade Gothic BoldCondTwenty Oblique/TradeGothic-BoldCondTwentyObl;' +
						'Trade Gothic Bold Oblique/TradeGothic-BoldOblique;' +
						'Trade Gothic BoldTwo/TradeGothic-BoldTwo;' +
						'Trade Gothic BoldTwo Oblique/TradeGothic-BoldTwoOblique;' +
						'Trade Gothic CondEighteen/TradeGothic-CondEighteen;' +
						'Trade Gothic CondEighteen Oblique/TradeGothic-CondEighteenObl;' +
						'Trade Gothic Light/TradeGothic-Light;' +
						'Trade Gothic Light Oblique/TradeGothic-LightOblique;' +
						'Trade Gothic Oblique/TradeGothic-Oblique;' +
						config.font_names;

	config.height = parseInt($('#popup_module_text').height() - 200) + 'px';
	config.width = '790px';
	config.resize_enabled=false;
	
	config.removeButtons = 'About,Source,Save,NewPage,DocProps,Preview,Print,Templates,document,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Anchor,CreatePlaceholder,Flash,PageBreak,Iframe,InsertPre,Blockquote,CreateDiv,BidiLtr,BidiRtl,ShowBlocks,Maximize,UIColor';
};